import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './controllers/users/users.controller';
import { UsersService } from './services/users/users.service';
import { AuthsService } from './services/auths/auths.service';
import { AuthsController } from './controllers/auths/auths.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { DevicesService } from './services/devices/devices.service';
import { DevicesController } from './controllers/devices/devices.controller';
import { FollowerController } from './controllers/follower/follower.controller';
import { FollowerService } from './services/follower/follower.service';
import { UserLanguagesController } from './controllers/user-languages/user-languages.controller';
import { UserLanguagesService } from './services/user-languages/user-languages.service';
import { UserPostsController } from './controllers/user-posts/user-posts.controller';
import { UserPostsService } from './services/user-posts/user-posts.service';
import { UserPostsViewsService } from './services/user-posts-views/user-posts-views.service';
import { UserPostsViewsController } from './controllers/user-posts-views/user-posts-views.controller';
import { UserPostsAttachmentsController } from './controllers/user-posts-attachments/user-posts-attachments.controller';
import { UserPostsAttachmentsService } from './services/user-posts-attachments/user-posts-attachments.service';
import { UserProfileViewsController } from './controllers/user-profile-views/user-profile-views.controller';
import { UserProfileViewsService } from './services/user-profile-views/user-profile-views.service';
import { UserPaymentMethodsService } from './services/user-payment-methods/user-payment-methods.service';
import { UserPaymentMethodsController } from './controllers/user-payment-methods/user-payment-methods.controller';
import { BiosController } from './controllers/bios/bios.controller';
import { BiosService } from './services/bios/bios.service';
import { users } from '../../db/entitys/users';
import { auths } from '../../db/entitys/auths';
import { devices } from '../../db/entitys/devices';
import { followers } from '../../db/entitys/followers';
import { user_languages } from '../../db/entitys/user_languages';
import { user_posts } from '../../db/entitys/user_posts';
import { user_post_views } from '../../db/entitys/user_post_views';
import { user_post_attachments } from '../../db/entitys/user_post_attachments';
import { user_payment_methods } from '../../db/entitys/user_payment_methods';
import { bios } from '../../db/entitys/bios';
import { user_profile_views } from '../../db/entitys/user_profile_views';
import { SharedModule } from '../shared/shared.module';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [
        users,
        auths,
        devices,
        followers,
        user_languages,
        user_posts,
        user_post_views,
        user_post_attachments,
        user_payment_methods,
        bios,
        user_profile_views
      ]
    ),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'secret',
      signOptions: {
        expiresIn: '180d',
      },
    }),
    SharedModule
  ],
  controllers: [UsersController, AuthsController, DevicesController, FollowerController, UserLanguagesController, UserPostsController, UserPostsViewsController, UserPostsAttachmentsController, UserProfileViewsController, UserPaymentMethodsController, BiosController],
  providers: [UsersService, AuthsService, DevicesService, FollowerService, UserLanguagesService, UserPostsService, UserPostsViewsService, UserPostsAttachmentsService, UserProfileViewsService, UserPaymentMethodsService, BiosService],
  exports: [PassportModule, AuthsService]
})
export class UsersModule {

}
