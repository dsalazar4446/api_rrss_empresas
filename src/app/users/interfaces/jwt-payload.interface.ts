import { IsString, IsNotEmpty, IsNumber, IsBoolean, IsOptional, IsEnum } from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { OS } from "../../shared/utils/enums";
import { CreateDevicesDto } from "../dto/devices.dto";

export class JwtPayload extends CreateDevicesDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    email: string;
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    password: string;
}
