import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserPostView, UpdateUserPostView } from '../../dto/user-post-view.dto';
import { user_post_views } from '../../../../db/entitys/user_post_views';
@Injectable()
export class UserPostsViewsService {
    constructor(@InjectRepository(user_post_views) private readonly repository: Repository<user_post_views>) {}
    async create(body: CreateUserPostView) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }
    async listByUser(userId: string, languageType: string){
        return await this.repository.findOne({
            where:{
                userId
            }
        });
    }
    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }

    async update(id: string, body: Partial<UpdateUserPostView>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}