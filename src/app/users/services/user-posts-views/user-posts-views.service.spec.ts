import { Test, TestingModule } from '@nestjs/testing';
import { UserPostsViewsService } from './user-posts-views.service';

describe('UserPostsViewsService', () => {
  let service: UserPostsViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserPostsViewsService],
    }).compile();

    service = module.get<UserPostsViewsService>(UserPostsViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
