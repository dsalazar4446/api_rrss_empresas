import { Injectable, HttpException, HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAuthsDto, UpdateAuthsDto, CreateLoginDeviceDto } from '../../dto/auths.dto';
import { JwtPayload } from '../../interfaces/jwt-payload.interface';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { auths } from '../../../../db/entitys/auths';
import { DevicesService } from '../devices/devices.service';
import { CreateDevicesDto } from '../../dto/devices.dto';
import { devices } from '../../../../db/entitys/devices';
import { SendgridService } from '../../../shared/services/sendgrid/sendgrid.service';
import * as ShortUniqueId from 'short-unique-id';

@Injectable()
export class AuthsService {
    constructor(
        @InjectRepository(auths) private readonly repository: Repository<auths>,
        @InjectRepository(devices) private readonly deviceRpo: Repository<devices>,
        @Inject(forwardRef(() => UsersService)) private readonly userssService: UsersService,
        private readonly jwtService: JwtService,
        private readonly deviceService: DevicesService,
        private readonly sendgrid: SendgridService
    ) { }
    async create(body: CreateAuthsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string) {
        return await this.repository.find({
            where: {
                language_type: languageType,
            }
        });
    }

    async detail(id: string, languageType: string) {
        return await this.repository.findOne({
            where: {
                id,
                language_type: languageType,
            }
        });
    }

    async findByUserId(user: string) {
        return await this.repository.findOne({
            where: {
                user,
            }
        });
    }

    async update(id: string, body: Partial<UpdateAuthsDto>) {
        const result = await this.repository.findOne(id);
        await this.repository.merge(result, body);
        return await this.repository.save(result);
    }

    async delete(id: string) {
        const result = await this.repository.findOne({ where: { id } })
        if (!result) {
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if (remove) {
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }
    async validateUser(payload: CreateLoginDeviceDto) {
        console.log(payload.password)
        const error = {
            status: 'error',
            code: HttpStatus.NOT_FOUND,
            message: 'email or password incorrect'
        }
        if (!payload) {
            return new HttpException('data  wasn\'t send', HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const result = await this.userssService.findByEmail(payload.email);
        if (!result) {
            return error
        }
        const auth = await this.findByUserId(result.id);
        console.log(auth)
        if (auth.pwd !== payload.password) {
            return error
        }
        return {
            status: 'ok',
            code: HttpStatus.OK,
            data: result
        }
    }

    async forgotPassword(userId: string, languageType: string) {
        const uid = new ShortUniqueId()
        const code = uid.randomUUID(4);
        const user = await this.userssService.detail(userId, languageType);
        user.verification_code = code;
        this.userssService.update(user.id, user);
        this.sendgrid.sendMail(process.env.EMAIL, [user.email], 'verification code', 'Your verification code', `
        <p> Your verification code is: ${code}</p>`)
        .then(
            resp => {console.log(resp)}
        ).
        catch(
            err => {console.log(err)}
        )
    }

    async updatePass(userId:string, pass:string){
        const auth = await this.repository.findOne({
            where:{user: userId}
        })
        auth.pwd= pass
        const update = this.update(auth.id,auth);
        if(!update){
            throw new HttpException('could not update password, try again', HttpStatus.NOT_FOUND)
        }
        return new HttpException('change password success', HttpStatus.OK)
    }

    async createToken(payload: CreateLoginDeviceDto) {
        const data: any = await this.validateUser(payload);
        console.log(data)
        if (data.status === 'error') {
            return data
        }
        const email = { email: payload.email }
        const createDevice: CreateDevicesDto = payload;
        const result = await this.userssService.findByEmail(payload.email);
        createDevice.user = result;
        this.deviceRpo.save(createDevice).then(resp => { console.log('ok') }).catch(err => console.log('Error', err))
        const accessToken = this.jwtService.sign(email);
        return {
            accessToken,
            user: data.data
        };
    }
}