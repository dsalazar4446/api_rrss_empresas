import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFollower, UpdateFollower } from '../../dto/follower.dto';
import { followers } from '../../../../db/entitys/followers';
@Injectable()
export class FollowerService {
    constructor(@InjectRepository(followers) private readonly repository: Repository<followers>) {}
    async create(body: CreateFollower) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({relations:['user', 'followed']});
    }
    async listByUserId(userId: string){
        return await this.repository.find({
            where:{
                userId
            },
            relations:['user', 'followed']  
        });
    }
    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:['user', 'followed']
        });
    }

    async update(id: string, body: Partial<UpdateFollower>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}