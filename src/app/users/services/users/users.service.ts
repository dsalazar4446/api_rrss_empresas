import { Injectable, HttpException, HttpStatus, forwardRef, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto, UpdateUserDto, UserPayload } from '../../dto/user.dto';
import { AuthsService } from '../auths/auths.service';
import { users } from '../../../../db/entitys/users';
import { CreateAuthsDto } from '../../dto/auths.dto';
import * as ShortUniqueId from 'short-unique-id';
import { SendgridService } from '../../../shared/services/sendgrid/sendgrid.service';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(users) private readonly repository: Repository<users>,
        @Inject(forwardRef(() => AuthsService)) private readonly authsService: AuthsService,
        private sengrid: SendgridService
    ) {}
    async create(body: UserPayload) {
        console.log(body)
        const uid = new  ShortUniqueId()
        body.verification_code = uid.randomUUID(4)
        body.assistant_step = 1;
        body.role_user = 1
        body.user_status = 'pending'
        const user: CreateUserDto = {
            city:body.city ,
            email:!body.email?null:body.email.trim() ,
            cellphone:!body.cellphone?null:body.cellphone.trim() ,
            avatar:!body.avatar?null:body.avatar.trim() ,
            first_name:!body.first_name?null:body.first_name.trim() ,
            last_name:!body.last_name?null:body.last_name.trim() ,
            gender:!body.gender?null:body.gender.trim() ,
            birth_date:!body.birth_date?null:body.birth_date ,
            latitude:!body.latitude?null:body.latitude.trim() ,
            longitude:!body.longitude?null:body.longitude.trim() ,   
            address_1:!body.address_1?null:body.address_1.trim() ,
            adddres_2:!body.adddres_2?null:body.adddres_2.trim() ,
            assistant_step:!body.assistant_step?null:body.assistant_step ,
            verification_code: !body.verification_code?null:body.verification_code.trim() ,
            user_status:body.user_status ,
            role_admin:body.role_admin,
            role_user:body.role_user,
            role_advisor:body.role_advisor,
            role_company:body.role_company,
            language_type: body.language_type
        }
  

        const existEmail = await this.repository.findOne({where:{email:body.email}})
        const existPhone = await this.repository.findOne({where:{cellphone: body.cellphone}})
        if(existEmail){
            return new HttpException('this email is registered', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        if (existPhone) {
            return new HttpException('this celphone is registered', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        const result = await this.repository.save(user)
        await this.authsService.create({user: result, pwd: body.pwd })
        this.sengrid.sendMail(process.env.EMAIL, [body.email], 'verification code','Your verification code     ', `
            <p> Your verification code is: ${body.verification_code}</p>
        `).then(resp => {
            // console.log('Resp',resp)
        }).catch(
            err => {
                // console.log('Err',err)        
            }
        )
        return result;
    }

    async list(languageType: string){
        return await this.repository.find({relations:['devicess', 'city','bioss', 'followerss', 'followerss2', 'userLanguagess']});
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations:['devicess', 'city','bioss', 'followerss', 'followerss2','userLanguagess  ']
        });
    }
    async verificationCode(id,code){
        const user = await this.repository.findOne(id,{where: {verification_code:code}})
        if(!user){
            return {
                status: false,
                message: 'incorrect verification code'
            }
        }
        user.assistant_step = 2
        await this.repository.save(user)
        return {
            status: true,
            message: 'verification code confirm'
        }
    }

    async verificationCodeForgot(id,code){
        const user = await this.repository.findOne(id,{where: {verification_code:code}})
        if(!user){
            return {
                status: false,
                message: 'incorrect verification code'
            }
        }
        await this.repository.save(user)
        return {
            status: true,
            message: 'verification code confirm'
        }
    }

    async update(id: string, body: Partial<UpdateUserDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.delete(result)
        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

    async findByEmail(email:string){
        return await this.repository.findOne({where:{email}})
    }

}