import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserPosts, UpdateUserPosts } from '../../dto/user-post.dto';
import { user_posts } from '../../../../db/entitys/user_posts';
@Injectable()
export class UserPostsService {
    constructor(@InjectRepository(user_posts) private readonly repository: Repository<user_posts>) {}
    async create(body: CreateUserPosts) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where: {
                language_type: languageType
            }
        });
    }

    async listByUser(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType
            }
        });
    }
    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType
            }
        });
    }

    async update(id: string, body: Partial<UpdateUserPosts>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}