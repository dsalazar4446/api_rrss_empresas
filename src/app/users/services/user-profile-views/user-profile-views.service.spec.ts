import { Test, TestingModule } from '@nestjs/testing';
import { UserProfileViewsService } from './user-profile-views.service';

describe('UserProfileViewsService', () => {
  let service: UserProfileViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserProfileViewsService],
    }).compile();

    service = module.get<UserProfileViewsService>(UserProfileViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
