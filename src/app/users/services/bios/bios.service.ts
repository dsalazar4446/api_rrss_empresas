import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBiosDto, UpdateBiosDto } from '../../dto/bios.dto';
import { bios } from '../../../../db/entitys/bios';
@Injectable()
export class BiosService {
    constructor(@InjectRepository(bios) private readonly repository: Repository<bios>) {}
    async create(body: CreateBiosDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                languageType,
            }
        });
    }

    async listByUserId(userId:string, languageType: string){
        return await this.repository.find({
            where:{
                userId,
                languageType,
            }
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            }
        });
    }

    async update(id: string, body: Partial<UpdateBiosDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}