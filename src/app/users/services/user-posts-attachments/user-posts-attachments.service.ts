import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserPostAttachment, UpdateUserPostAttachment } from '../../dto/user-post-attachment.dto';
import { user_post_attachments } from '../../../../db/entitys/user_post_attachments';
@Injectable()
export class UserPostsAttachmentsService {
    constructor(@InjectRepository(user_post_attachments) private readonly repository: Repository<user_post_attachments>) {}
    async create(body: CreateUserPostAttachment) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }
    async listByUser(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                user_post_id: id,     
            }
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,     
            }
        });
    }

    async update(id: string, body: Partial<UpdateUserPostAttachment>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}