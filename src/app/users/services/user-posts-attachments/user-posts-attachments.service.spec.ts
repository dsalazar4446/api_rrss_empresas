import { Test, TestingModule } from '@nestjs/testing';
import { UserPostsAttachmentsService } from './user-posts-attachments.service';

describe('UserPostsAttachmentsService', () => {
  let service: UserPostsAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserPostsAttachmentsService],
    }).compile();

    service = module.get<UserPostsAttachmentsService>(UserPostsAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
