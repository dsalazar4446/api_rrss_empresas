import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { CreateUserDto, UpdateUserDto, UserPayload } from '../../dto/user.dto';
import { UsersService } from '../../services/users/users.service';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';
import { arrayFiles, localOptions } from './multer.option';

@Controller('users')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class UsersController {
    constructor(private readonly usersService: UsersService){}

    @Post()
    // @UseInterceptors(LangInterceptor)
    // @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'avatar', required: false})
    async create(@Body() body: UserPayload){
        return await this.usersService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.usersService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.usersService.detail(id, language);
    }
    @Get(':id/:code')
    async verificationCode(@Param('id') id: string,@Param('code') code: string){
        return await this.usersService.verificationCode(id, code);
    }

    @Get('verification-code-forgot/:id/:code')
    async verificationCodeForgot(@Param('id') id: string,@Param('code') code: string){
        return await this.usersService.verificationCodeForgot(id, code);
    }

    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    // @UseInterceptors(LangInterceptor)
    // @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'avatar', required: false})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserDto>){
        return await this.usersService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.usersService.delete(id);
    }

}