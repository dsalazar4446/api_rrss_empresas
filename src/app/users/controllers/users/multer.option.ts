
import { diskStorage } from 'multer';
import { extname } from "path";

export const arrayFiles = [
    { name: 'avatar', maxCount: 1 },
];

export const localOptions = {
    storage: diskStorage({
        destination: './uploads/users'
        , filename: (req, file, cb) => {
            let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];
            if (extValidas.indexOf(extname(file.originalname)) < 0) {
                cb('valid extensions: ' + extValidas.join(', '));
                return;
            }   
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'avatar') {
                req.body.avatar = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
    })
}