import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { DevicesService } from '../../services/devices/devices.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateDevicesDto, UpdateDevicesDto } from '../../dto/devices.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('devices')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class DevicesController {
    constructor(private readonly devicesService: DevicesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateDevicesDto){
        return await this.devicesService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.devicesService.list(language);
    }
    @Get('user/:userId')
    async listByUser(@Param('userId') id: string){
        return await this.devicesService.listByUser(id);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.devicesService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateDevicesDto>){
        return await this.devicesService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.devicesService.delete(id);
    }

}