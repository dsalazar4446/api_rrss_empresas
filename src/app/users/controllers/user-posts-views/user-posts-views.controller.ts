import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { UserPostsViewsService } from '../../services/user-posts-views/user-posts-views.service';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateUserPostView, UpdateUserPostView } from '../../dto/user-post-view.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('user-posts-views')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Users')
export class UserPostsViewsController {
    constructor(private readonly userPostsViewsService: UserPostsViewsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserPostView){
        return await this.userPostsViewsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userPostsViewsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPostsViewsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  Partial<UpdateUserPostView>){
        return await this.userPostsViewsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userPostsViewsService.delete(id);
    }

}
