import { Test, TestingModule } from '@nestjs/testing';
import { UserPostsViewsController } from './user-posts-views.controller';

describe('UserPostsViews Controller', () => {
  let controller: UserPostsViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserPostsViewsController],
    }).compile();

    controller = module.get<UserPostsViewsController>(UserPostsViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
