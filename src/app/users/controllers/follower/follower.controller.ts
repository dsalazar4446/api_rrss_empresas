import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { FollowerService } from '../../services/follower/follower.service';
import { CreateFollower, UpdateFollower } from '../../dto/follower.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('followers')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class FollowerController {
    constructor(private readonly followerService: FollowerService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateFollower){
        return await this.followerService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.followerService.list(language);
    }
    @Get('user/:userId')
    async listByUserId(@Param('userId') id: string){
        return await this.followerService.listByUserId(id);
    }
    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.followerService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  UpdateFollower){
        return await this.followerService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.followerService.delete(id);
    }

}