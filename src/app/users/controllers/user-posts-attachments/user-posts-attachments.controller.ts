import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { UserPostsAttachmentsService } from '../../services/user-posts-attachments/user-posts-attachments.service';
import { CreateUserPostAttachment, UpdateUserPostAttachment } from '../../dto/user-post-attachment.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';
import { arrayFiles, localOptions } from './multer.option';

@Controller('user-posts-attachments')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Users')
export class UserPostsAttachmentsController {
    constructor(private readonly userPostsAttachmentsService: UserPostsAttachmentsService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'post_attachement', required: false})
    async create(@Body() body: CreateUserPostAttachment){
        return await this.userPostsAttachmentsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userPostsAttachmentsService.list(language);
    }

    @Get('user-post/:id')
    async listByUser(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPostsAttachmentsService.listByUser(id, language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPostsAttachmentsService.detail(id, language);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'post_attachement', required: false})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserPostAttachment>){
        return await this.userPostsAttachmentsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userPostsAttachmentsService.delete(id);
    }

}