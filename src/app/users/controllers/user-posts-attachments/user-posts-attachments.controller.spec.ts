import { Test, TestingModule } from '@nestjs/testing';
import { UserPostsAttachmentsController } from './user-posts-attachments.controller';

describe('UserPostsAttachments Controller', () => {
  let controller: UserPostsAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserPostsAttachmentsController],
    }).compile();

    controller = module.get<UserPostsAttachmentsController>(UserPostsAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
