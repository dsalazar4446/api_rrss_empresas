import { Test, TestingModule } from '@nestjs/testing';
import { UserLanguagesController } from './user-languages.controller';

describe('UserLanguages Controller', () => {
  let controller: UserLanguagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserLanguagesController],
    }).compile();

    controller = module.get<UserLanguagesController>(UserLanguagesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
