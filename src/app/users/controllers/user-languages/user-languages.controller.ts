import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { UserLanguagesService } from '../../services/user-languages/user-languages.service';
import { CreateUserLenguajesDto, UpdateUserLenguajesDto } from '../../dto/user-languages.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('user-languages')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class UserLanguagesController {
    constructor(private readonly userLanguagesService: UserLanguagesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserLenguajesDto){
        return await this.userLanguagesService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userLanguagesService.list(language);
    }

    @Get('user/:id')
    async listByUser(@Param('id') id: string, @Headers('language') language: string){
        return await this.userLanguagesService.listByUser(id, language);
    }
    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userLanguagesService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserLenguajesDto>){
        return await this.userLanguagesService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userLanguagesService.delete(id);
    }

}