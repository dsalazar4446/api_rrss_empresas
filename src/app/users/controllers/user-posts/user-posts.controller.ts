import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { UserPostsService } from '../../services/user-posts/user-posts.service';
import { CreateUserPosts, UpdateUserPosts } from '../../dto/user-post.dto';
import { arrayFiles, localOptions } from './multer.option';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('user-posts')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class UserPostsController {
    constructor(private readonly userPostsService: UserPostsService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'user_post_picture', required: false})
    async create(@Body() body: CreateUserPosts){
        return await this.userPostsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userPostsService.list(language);
    }
    @Get('user/:id')
    async listByUser(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPostsService.listByUser(id, language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPostsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'user_post_picture', required: false})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserPosts>){
        return await this.userPostsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userPostsService.delete(id);
    }

}