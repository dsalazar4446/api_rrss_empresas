
import { diskStorage } from 'multer';
import { extname } from "path";
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';

export const arrayFiles = [
    { name: 'user_post_picture', maxCount: 1 },
];

export const localOptions: MulterOptions = {
    storage: diskStorage({
        destination: './uploads/user-posts'
        , filename: (req, file, cb) => {
            let extValidas = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG'];


            if (extValidas.indexOf(extname(file.originalname)) < 0) {
                cb('valid extensions: ' + extValidas.join(', '));
                return;
            }
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'user_post_picture') {
                req.body.user_post_picture = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
    })
}