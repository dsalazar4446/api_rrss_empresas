
import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { AuthsService } from '../../services/auths/auths.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateAuthsDto, UpdateAuthsDto, CreateLoginDeviceDto, ForgotPass } from '../../dto/auths.dto';
import { JwtPayload } from '../../interfaces/jwt-payload.interface';
import { ApiImplicitHeader, ApiUseTags } from '@nestjs/swagger';

@Controller('auths')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)

export class AuthsController {
    constructor(private readonly authsService: AuthsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAuthsDto){
        return await this.authsService.create(body);
    }

    @Post('login')
    login(@Body() payload: CreateLoginDeviceDto){   
        return this.authsService.createToken(payload)
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.authsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.authsService.detail(id, language);
    }

    @Get('forgot-password/:userId')
    async forgotPassword(@Param('userId') userId,@Headers('language') language: string){
       return await this.authsService.forgotPassword(userId,language); 
    }


    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAuthsDto>){
        return await this.authsService.update(id, body);
    }

    @Put('restore-password/:userId')
    updatePass(@Param('userId') userId: string, @Body() body: ForgotPass){
        this.authsService.updatePass(userId, body.pwd)
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.authsService.delete(id);
    }

    
}