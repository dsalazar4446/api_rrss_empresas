import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { UserPaymentMethodsService } from '../../services/user-payment-methods/user-payment-methods.service';
import { CreateUserPaymentMethodsDto, UpdateUserPaymentMethodsDto } from '../../dto/user-payment-methods.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('user-payment-methods')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Users')
export class UserPaymentMethodsController {
    constructor(private readonly userPaymentMethodsService: UserPaymentMethodsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserPaymentMethodsDto){
        return await this.userPaymentMethodsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userPaymentMethodsService.list(language);
    }

    @Get('user/:id')
    async listByUser(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPaymentMethodsService.listByUser(id, language);
    }


    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userPaymentMethodsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserPaymentMethodsDto>){
        return await this.userPaymentMethodsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userPaymentMethodsService.delete(id);
    }

}