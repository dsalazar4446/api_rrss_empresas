import { Test, TestingModule } from '@nestjs/testing';
import { UserPaymentMethodsController } from './user-payment-methods.controller';

describe('UserPaymentMethods Controller', () => {
  let controller: UserPaymentMethodsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserPaymentMethodsController],
    }).compile();

    controller = module.get<UserPaymentMethodsController>(UserPaymentMethodsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
