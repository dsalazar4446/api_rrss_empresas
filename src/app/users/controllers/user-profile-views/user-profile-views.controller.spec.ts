import { Test, TestingModule } from '@nestjs/testing';
import { UserProfileViewsController } from './user-profile-views.controller';

describe('UserProfileViews Controller', () => {
  let controller: UserProfileViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserProfileViewsController],
    }).compile();

    controller = module.get<UserProfileViewsController>(UserProfileViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
