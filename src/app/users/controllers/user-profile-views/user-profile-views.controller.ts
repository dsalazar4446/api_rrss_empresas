import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { UserProfileViewsService } from '../../services/user-profile-views/user-profile-views.service';
import { CreateUserProfileViews, UpdateUserProfileViews } from '../../dto/user-profile-view.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('user-profile-views')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Users')
export class UserProfileViewsController {
    constructor(private readonly userProfileViewsService: UserProfileViewsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserProfileViews){
        return await this.userProfileViewsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.userProfileViewsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.userProfileViewsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserProfileViews>,){
        return await this.userProfileViewsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.userProfileViewsService.delete(id);
    }

}