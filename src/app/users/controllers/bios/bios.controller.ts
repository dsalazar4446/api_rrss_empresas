import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { BiosService } from '../../services/bios/bios.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateBiosDto, UpdateBiosDto } from '../../dto/bios.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('bios')
@ApiUseTags('Users')
@UseInterceptors(ResponseInterceptor)
export class BiosController {
    constructor(private readonly biosService: BiosService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateBiosDto){
        return await this.biosService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.biosService.list(language);
    }
    @Get('user/:userId')
    async listByUserId(@Param('userId') id: string,@Headers('language') language: string){
        return await this.biosService.listByUserId(id,language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.biosService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  Partial<UpdateBiosDto>){
        return await this.biosService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.biosService.delete(id);
    }

}