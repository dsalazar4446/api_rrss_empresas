import { PostAttachementType } from "../../shared/utils/enums";
import { IsUUID, IsString, IsOptional } from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";



export class CreateUserPostAttachment {
    @ApiModelProperty()
    @IsUUID()
    user_post_id?: string
    @ApiModelProperty()
    @IsUUID()
    post_attachement?: string
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    post_attachement_type?: PostAttachementType
}

export class UpdateUserPostAttachment extends CreateUserPostAttachment{
    @ApiModelProperty()
    @IsUUID()
    id?: string
}