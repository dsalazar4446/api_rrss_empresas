import { Gender, UserStatus } from "../../shared/utils/enums";
import {IsString, IsUUID, IsEmail, IsOptional, IsNumber } from 'class-validator'
import { cities } from "../../../db/entitys/cities";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class CreateUserDto {
    @ApiModelPropertyOptional()
    @IsOptional()
    @IsUUID()
    public city?:cities
    @ApiModelProperty()
    @IsEmail()
    public email?: string;
    @ApiModelProperty()
    @IsString()
    public cellphone?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public avatar?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public first_name?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public last_name?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public gender?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public birth_date?: Date;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public latitude?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public longitude?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public address_1?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public adddres_2?: string;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    public assistant_step?: number;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    public verification_code?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    public user_status?: string;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    public role_admin?: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    public role_user?: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    public role_advisor?: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    public role_company?: number;
    @ApiModelProperty()
    @IsString()
    language_type?:string;
}

export class UserPayload extends CreateUserDto {
    @ApiModelProperty()
    @IsString()
    pwd?: string;
}

export class UpdateUserDto extends CreateUserDto{
    @ApiModelProperty()
    @IsUUID()
    public id?: string;
}