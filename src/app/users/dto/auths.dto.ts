import { IsUUID, IsString, IsNumber, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { OS } from "../../shared/utils/enums";
import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class CreateAuthsDto {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsString()
    pwd?: string;
}
export class UpdateAuthsDto extends CreateAuthsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}

export class CreateLoginDeviceDto extends JwtPayload{
    @ApiModelPropertyOptional()
    @IsUUID()
    @IsOptional()
    user?: users;
}

export class ForgotPass {
    pwd: string
}