import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersController } from '../controllers/users/users.controller';
import { AuthsController } from '../controllers/auths/auths.controller';
import { UsersService } from '../services/users/users.service';
import { AuthsService } from '../services/auths/auths.service';
import { devices } from '../../../db/entitys/devices';
import { users } from '../../../db/entitys/users';
import { auths } from '../../../db/entitys/auths';
import { bios } from '../../../db/entitys/bios';
import { followers } from '../../../db/entitys/followers';
import { user_languages } from '../../../db/entitys/user_languages';
import { user_payment_methods } from '../../../db/entitys/user_payment_methods';
import { user_post_attachments } from '../../../db/entitys/user_post_attachments';
import { user_post_views } from '../../../db/entitys/user_post_views';
import { user_posts } from '../../../db/entitys/user_posts';
import { user_profile_views } from '../../../db/entitys/user_profile_views';


@Module({
    imports:[
        TypeOrmModule.forFeature([
          devices,
          users,
          auths,
          bios,
          followers,
          user_languages,
          user_payment_methods,
          user_post_attachments,
          user_post_views,
          user_posts,
          user_profile_views
        ]),
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
          secret: 'secret',
          signOptions: {
            expiresIn: '180d',
          },
        }),

    ],
    controllers: [UsersController, AuthsController],
    providers: [UsersService, AuthsService],
    exports: [PassportModule, AuthsService]
})
export class UsersModule {

}
