import { LanguageType } from "../../shared/utils/enums";
import { IsEnum, IsUUID, IsString, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateBiosDto {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsString()
    bio?: string;
    @ApiModelProperty()
    @IsEnum({
        ES : 'es',
        EN : 'en',
        IT : 'it', 
        PR : 'pr'
    })
    @IsOptional()
    language_type?: LanguageType

}

export class UpdateBiosDto extends CreateBiosDto {
    @ApiModelProperty()
    @IsString()
    id?: string;
}
