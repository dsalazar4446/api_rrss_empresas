import { PaymentRecievePay } from "../../shared/utils/enums";
import { IsUUID, IsString } from "class-validator";
import { payment_methods } from "../../../db/entitys/payment_methods";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";




export class CreateUserPaymentMethodsDto {
    @ApiModelProperty()
    @IsUUID()
    payment_method_id?: payment_methods;
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsString()
    last_numbers?: string;
    @ApiModelProperty()
    @IsString()
    payment_token?: string;
    @ApiModelProperty()
    @IsString()
    payment_recieve_pay?: PaymentRecievePay;
    @ApiModelProperty()
    @IsString()
    payment_main?: string;
}

export class UpdateUserPaymentMethodsDto extends CreateUserPaymentMethodsDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
