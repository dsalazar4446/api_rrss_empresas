import { OS } from "../../shared/utils/enums";
import { IsUUID, IsString, IsNumber, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateDevicesDto {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty({enum:[
        'chrome',
        'firefox',
        'opera',
        'explorer',
        'android',
        'ios',
        'win'
    ]})

    @IsEnum({
        CHROME:'chrome',
        FIREFOX:'firefox',
        OPERA:'opera',
        EXPLORER:'explorer',
        ANDROID:'android',
        IOS:'ios',
        WIN: 'win'
    })
    @ApiModelProperty()
    @IsOptional()
    os?: OS
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    ip_address?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    uuid_device?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    longitude?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    latitude?: string;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    player_id?: string;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    device_status?: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    mobile_notification?: number;
    @ApiModelProperty()
    @IsNumber()
    @IsOptional()
    mobile_vibration?: number;

}

export class UpdateDevicesDto extends CreateDevicesDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}