import { IsUUID } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";


export class CreateUserProfileViews {
    @ApiModelProperty()
    @IsUUID()
    viewedByUser?:users;
    @ApiModelProperty()
    @IsUUID()
    profileUser?:users;
}

export class UpdateUserProfileViews extends CreateUserProfileViews{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
