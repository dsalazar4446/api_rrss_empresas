import { LanguageType } from "../../shared/utils/enums";
import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";


export class CreateUserPosts {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsString()
    user_post_picture?: string;
    @ApiModelProperty()
    @IsString()
    user_post_title?: string;
    @ApiModelProperty()
    @IsString()
    user_post_description?: string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr" 
    })
    @IsOptional()
    language_type?: LanguageType
}

export class UpdateUserPosts extends CreateUserPosts {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
