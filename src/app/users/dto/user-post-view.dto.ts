import { IsUUID, IsNumber } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";


export class CreateUserPostView {
    @ApiModelProperty()
    @IsUUID()
    user?:users
    @ApiModelProperty()
    @IsUUID()
    userPostId?: string
    @ApiModelProperty()
    @IsNumber()
    userPostShare?: number;
    @ApiModelProperty()
    @IsNumber()
    userPostLike?: number;
}

export class UpdateUserPostView extends CreateUserPostView {
    @ApiModelProperty()
    @IsUUID()
    id?: string
}