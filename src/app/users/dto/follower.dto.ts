import { IsUUID } from "class-validator";
import { users } from "../../../db/entitys/users";
import { followers } from "../../../db/entitys/followers";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateFollower {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsUUID()
    followed?: followers 
}

export class UpdateFollower extends CreateFollower {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}