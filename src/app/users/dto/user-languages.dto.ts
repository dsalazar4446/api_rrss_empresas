import { Entity, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne } from "typeorm";
import { LanguageType } from "../../shared/utils/enums";
import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class CreateUserLenguajesDto {
    @ApiModelProperty()
    @IsUUID()
    users?: users;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr"
    })
    @IsOptional()
    language_type?: LanguageType;
}

export class UpdateUserLenguajesDto extends CreateUserLenguajesDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string
}