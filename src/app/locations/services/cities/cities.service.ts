import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCitiesDto } from '../../dto/cities.dto';
import { cities } from '../../../../db/entitys/cities';
@Injectable()
export class CitiesService {
    constructor(@InjectRepository(cities) private readonly repository: Repository<cities>) {}
    async create(body: CreateCitiesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:['country']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            }
        });
    }

    async update(id: string, body: Partial<CreateCitiesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}