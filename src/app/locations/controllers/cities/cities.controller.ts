import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CitiesService } from '../../services/cities/cities.service';
import { CreateCitiesDto, UpdateCitiesDto } from '../../dto/cities.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('cities')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Locations')
export class CitiesController {
    constructor(private readonly citiesService: CitiesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateCitiesDto){
        return await this.citiesService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.citiesService.list(language); 
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.citiesService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  Partial<UpdateCitiesDto>){
        return await this.citiesService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.citiesService.delete(id);
    }

}