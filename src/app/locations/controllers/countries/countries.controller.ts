import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { CountriesService } from '../../services/countries/countries.service';
import { CreateCountriesDto, UpdateCountriesDto } from '../../dto/countries.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('countries')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Locations')
export class CountriesController {
    constructor(private readonly countriesService: CountriesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateCountriesDto){
        return await this.countriesService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.countriesService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.countriesService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  Partial<UpdateCountriesDto>){
        return await this.countriesService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.countriesService.delete(id);
    }

}