import { Module } from '@nestjs/common';
import { CitiesService } from './services/cities/cities.service';
import { CountriesService } from './services/countries/countries.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitiesController } from './controllers/cities/cities.controller';
import { CountriesController } from './controllers/countries/countries.controller';
import { cities } from '../../db/entitys/cities';
import { countries } from '../../db/entitys/countries';

@Module({
  imports: [
    TypeOrmModule.forFeature([cities,countries])
  ],
  providers: [CitiesService, CountriesService],
  controllers: [CitiesController, CountriesController]
})
export class LocationsModule {}
