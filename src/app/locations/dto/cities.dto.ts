import { IsUUID, IsString } from "class-validator";
import { countries } from "../../../db/entitys/countries";
import { ApiModelProperty } from "@nestjs/swagger";


export class CreateCitiesDto {
    @ApiModelProperty()
    @IsUUID()
    country?: countries;
    @ApiModelProperty()
    @IsString()
    city_name?: string;
}

export class UpdateCitiesDto extends CreateCitiesDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}