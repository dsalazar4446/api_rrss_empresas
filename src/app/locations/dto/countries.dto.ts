import { IsUUID, IsString, MaxLength } from "class-validator";
import { IsNull } from "typeorm";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateCountriesDto {
    @ApiModelProperty()
    @IsString()
    @MaxLength(255)
    country_name?: string;
    @ApiModelProperty()
    @IsString()
    @MaxLength(255)
    country_flag?: string;
    @ApiModelProperty()
    @IsString()
    @MaxLength(6)
    country_code?: string;

}

export class UpdateCountriesDto extends CreateCountriesDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}