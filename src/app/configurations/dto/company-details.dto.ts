import { IsUUID, IsString, IsOptional } from "class-validator";
import { companies } from "../../../db/entitys/companies";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateCompanyDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    company?:companies | null;
    @ApiModelProperty()
    @IsString()
    company_detail?:string;
    @ApiModelProperty()
    @IsString() 
    company_terms?:string;
    @ApiModelProperty()
    @IsString()
    company_privacy?:string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?:string;  
}

export class UpdateCompanyDetailsDto extends CreateCompanyDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}