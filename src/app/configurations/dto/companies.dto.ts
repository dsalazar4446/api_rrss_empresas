import { IsUUID, IsString, IsEnum } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateCompaniesDto {
    @ApiModelProperty()
    @IsString()
    company_name?:string;
    @ApiModelProperty()
    @IsString()
    company_identification?:string;
    @ApiModelProperty()
    @IsString()
    company_email?:string;
    @ApiModelProperty()
    @IsString()
    company_phone?:string;
    @ApiModelProperty()
    @IsString()
    company_logo?:string;
    @ApiModelProperty()
    @IsString()
    company_address?:string | null;
    @ApiModelProperty()
    @IsString()
    company_latitude?:string | null;
    @ApiModelProperty()
    @IsString()
    company_longitude?:string | null;
    @ApiModelProperty()
    @IsString()
    company_postal_code?:string | null;
    @ApiModelProperty()
    @IsString()
    company_website?:string;
    @ApiModelProperty()
    @IsEnum({
        light:'light',
        gray:'gray',
        dark:'dark',
    })
    user_company_color_txt?: string
}

export class UpdateCompaniesDto extends CreateCompaniesDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}
