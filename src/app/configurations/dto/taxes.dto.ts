import { IsUUID, IsDate, IsNumber } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateTaxesDto {
    @ApiModelProperty()
    @IsDate()
    tax_date?:Date;
    @ApiModelProperty()
    @IsNumber()
    tax_percentage?:number;
    @ApiModelProperty()
    @IsNumber()
    retefete_percentage?:number;
}

export class UpdateTaxesDto extends CreateTaxesDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}
