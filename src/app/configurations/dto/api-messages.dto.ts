import { IsUUID, IsString, IsOptional } from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";


export class CreateApiMessagesDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
    @ApiModelProperty()
    @IsString()
    message_code?:string;
    @ApiModelProperty()
    @IsString()
    message_txt?:string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?:string;
}


export class UpdateApiMessagesDto extends CreateApiMessagesDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}