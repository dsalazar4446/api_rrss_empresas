import { Module } from '@nestjs/common';
import { TaxesController } from './controlles/taxes/taxes.controller';
import { ApiMessagesController } from './controlles/api-messages/api-messages.controller';
import { CompaniesController } from './controlles/companies/companies.controller';
import { CompanyDetailsController } from './controlles/company-details/company-details.controller';
import { CompanyDetailsService } from './services/company-details/company-details.service';
import { CompaniesService } from './services/companies/companies.service';
import { TaxesService } from './services/taxes/taxes.service';
import { ApiMessagesService } from './services/api-messages/api-messages.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { api_messages } from '../../db/entitys/api_messages';
import { taxes } from '../../db/entitys/taxes';
import { companies } from '../../db/entitys/companies';
import { company_details } from '../../db/entitys/company_details';


@Module({
  imports:[
    TypeOrmModule.forFeature([
      api_messages,
      taxes,
      companies,
      company_details
    ])
  ],
  controllers: [TaxesController, ApiMessagesController, CompaniesController, CompanyDetailsController],
  providers: [CompanyDetailsService, CompaniesService, TaxesService, ApiMessagesService]
})
export class ConfigurationsModule {}
