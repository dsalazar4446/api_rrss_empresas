import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { companies } from '../../../../db/entitys/companies';
import { CreateCompaniesDto } from '../../dto/companies.dto';
@Injectable()
export class CompaniesService {
    constructor(@InjectRepository(companies) private readonly repository: Repository<companies>) {}
    async create(body: CreateCompaniesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:['companyDetailss'],
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:['companyDetailss'],
        });
    }

    async update(id: string, body: Partial<CreateCompaniesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}