import { Test, TestingModule } from '@nestjs/testing';
import { ApiMessagesController } from './api-messages.controller';

describe('ApiMessages Controller', () => {
  let controller: ApiMessagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiMessagesController],
    }).compile();

    controller = module.get<ApiMessagesController>(ApiMessagesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
