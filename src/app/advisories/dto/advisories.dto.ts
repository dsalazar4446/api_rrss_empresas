import { IsUUID, IsDate, IsNumber, IsString } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateAdvisoriesDto {
    @ApiModelProperty()
    @IsUUID()
    requestedByUser?:users | null;
    @ApiModelProperty()
    @IsUUID()
    advisorUser?:users | null;
    @ApiModelProperty()
    @IsDate()
    advisory_sheduled?:Date;
    @ApiModelProperty()
    @IsDate()
    advisory_start?:Date;
    @ApiModelProperty()
    @IsDate()
    advisory_end?:Date;
    @ApiModelProperty()
    @IsNumber()
    advisory_rate?:number;
    @ApiModelProperty()
    @IsString()
    advisory_observation?:string | null;

}

export class UpdateAdvisoriesDto extends CreateAdvisoriesDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}