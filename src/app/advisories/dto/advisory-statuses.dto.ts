import { IsString, IsUUID, IsEnum } from "class-validator";
import { advisories } from "../../../db/entitys/advisories";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateAdvisoryStatusesDto {
    @ApiModelProperty()
    @IsUUID()
    advisories?:advisories;
    @ApiModelProperty({enum:['requested', 'confirmed', 'rejected', 'canceled']})
    @IsEnum({
        requested:'requested', confirmed:'confirmed', rejected:'rejected', canceled:'canceled'
    })
    advisory_status?:string;

}

export class UpdateAdvisoryStatusesDto extends CreateAdvisoryStatusesDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}