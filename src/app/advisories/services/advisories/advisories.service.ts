import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { advisories } from '../../../../db/entitys/advisories';
import { CreateAdvisoriesDto, UpdateAdvisoriesDto } from '../../dto/advisories.dto';
@Injectable()
export class AdvisoriesService {
    constructor(@InjectRepository(advisories) private readonly repository: Repository<advisories>) {}
    async create(body: CreateAdvisoriesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:[
                'requestedByUser',
                'advisorUser'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:[
                'requestedByUser',
                'advisorUser'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateAdvisoriesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}