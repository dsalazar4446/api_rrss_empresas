import { Test, TestingModule } from '@nestjs/testing';
import { AdvisoryStatusesService } from './advisory-statuses.service';

describe('AdvisoryStatusesService', () => {
  let service: AdvisoryStatusesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdvisoryStatusesService],
    }).compile();

    service = module.get<AdvisoryStatusesService>(AdvisoryStatusesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
