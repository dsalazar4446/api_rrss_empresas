import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { advisory_statuses } from '../../../../db/entitys/advisory_statuses';
import { CreateAdvisoryStatusesDto } from '../../dto/advisory-statuses.dto';
import { UpdateAdvisoriesDto } from '../../dto/advisories.dto';
@Injectable()
export class AdvisoryStatusesService {
    constructor(@InjectRepository(advisory_statuses) private readonly repository: Repository<advisory_statuses>) {}
    async create(body: CreateAdvisoryStatusesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
           relations: ['advisories'],
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['advisories'],
        });
    }

    async update(id: string, body: Partial<UpdateAdvisoriesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}