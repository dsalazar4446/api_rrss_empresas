import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { AdvisoryStatusesService } from '../../services/advisory-statuses/advisory-statuses.service';
import { CreateAdvisoryStatusesDto } from '../../dto/advisory-statuses.dto';
import { UpdateAdvisoriesDto } from '../../dto/advisories.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('advisory-statuses')
@ApiUseTags('Advisories')
@UseInterceptors(ResponseInterceptor)
export class AdvisoryStatusesController {
    constructor(private readonly nameService: AdvisoryStatusesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAdvisoryStatusesDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAdvisoriesDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}