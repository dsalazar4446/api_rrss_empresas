import { Test, TestingModule } from '@nestjs/testing';
import { AdvisoryStatusesController } from './advisory-statuses.controller';

describe('AdvisoryStatuses Controller', () => {
  let controller: AdvisoryStatusesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdvisoryStatusesController],
    }).compile();

    controller = module.get<AdvisoryStatusesController>(AdvisoryStatusesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
