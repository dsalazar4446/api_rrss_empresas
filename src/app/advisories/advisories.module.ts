import { Module } from '@nestjs/common';
import { AdvisoriesController } from './controllers/advisories/advisories.controller';
import { AdvisoryStatusesController } from './controllers/advisory-statuses/advisory-statuses.controller';
import { AdvisoryStatusesService } from './services/advisory-statuses/advisory-statuses.service';
import { AdvisoriesService } from './services/advisories/advisories.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { advisories } from '../../db/entitys/advisories';
import { advisory_statuses } from '../../db/entitys/advisory_statuses';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      advisories,
      advisory_statuses
    ])
  ],
  controllers: [AdvisoriesController, AdvisoryStatusesController],
  providers: [AdvisoryStatusesService, AdvisoriesService]
})
export class AdvisoriesModule {}
