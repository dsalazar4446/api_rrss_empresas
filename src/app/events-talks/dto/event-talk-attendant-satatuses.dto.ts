import { IsString, IsUUID, IsEnum } from "class-validator";
import { event_talk_attendants } from "../../../db/entitys/event_talk_attendants";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateEventTalkAttendantSatatuses {
    @ApiModelProperty()
    @IsString()
    eventTalkAttendants?: event_talk_attendants | null;
    @ApiModelProperty({enum:[ "reserved",  "confirmed",  "canceled",  "missed",  "attended"]})
    @IsEnum({
        reserved: "reserved", confirmed: "confirmed", canceled: "canceled", missed: "missed", attended: "attended"
    })
    event_talk_attendant_satatus?: string;

}
export class UpdateEventTalkAttendantSatatuses extends CreateEventTalkAttendantSatatuses {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}