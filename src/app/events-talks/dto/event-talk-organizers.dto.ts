import { IsUUID } from "class-validator";
import { users } from "../../../db/entitys/users";
import { events_talks } from "../../../db/entitys/events_talks";
import { ApiModelProperty } from "@nestjs/swagger";


export class CreateEventTalkOrganizersDto {
    @ApiModelProperty()
    @IsUUID()
    users?: users | null;
    @ApiModelProperty()
    @IsUUID()
    eventTalk?: events_talks | null;

}

export class UpdateEventTalkOrganizersDto extends CreateEventTalkOrganizersDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
