import { IsUUID, IsString, IsDate, IsNumber, IsEnum, IsOptional } from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class CreateEventsTalksDto {
    @ApiModelProperty()
    @IsString()
    event_talk_title?: string;
    @ApiModelProperty()
    @IsString()
    event_talk_description?: string;
    @ApiModelProperty()
    @IsDate()
    event_talk_schedule?: Date;
    @ApiModelProperty()
    @IsDate()
    event_talk_start?: Date;
    @ApiModelProperty()
    @IsString()
    event_talk_end?: string;
    @ApiModelProperty()
    @IsNumber()
    event_talk_taxes?: number;
    @ApiModelProperty()
    @IsNumber()
    event_talk_retefte?: number;

    @ApiModelProperty()
    @IsNumber()
    event_talk_ammount?: number;
    @ApiModelProperty()
    @IsNumber()
    event_talk_total?: number;
    @ApiModelProperty({enum:[ "event",  "talk" ]})
    @IsEnum({ event: "event", talk: "talk" })
    event_talk_type?: string;
    @ApiModelProperty({enum:["online", "presential" ]})
    @IsEnum({ online: "online", presential: "presential" })
    event_talk_chanel?: string;
    @ApiModelProperty({enum:[ "free",  "plan",  "payment",  "personal" ]})
    @IsEnum({ free: "free", plan: "plan", payment: "payment", personal: "personal" })
    event_talk_target?: string;

    @ApiModelProperty()
    @IsString()
    event_talk_broadcast_url?: string | null;

    @ApiModelProperty()
    @IsString()
    event_talk_broadcast_key?: string | null;

    @ApiModelProperty()
    @IsString()
    event_talk_share_url?: string | null;

    @ApiModelProperty()
    @IsString()
    latitude?: string | null;

    @ApiModelProperty()
    @IsString()
    longitude?: string | null;

    @ApiModelProperty()
    @IsString()
    address_1?: string | null;

    @ApiModelProperty()
    @IsString()
    adddres_2?: string | null;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?: string;

}

export class UpdateEventsTalksDto extends CreateEventsTalksDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;

}