import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { EventsTalksService } from '../../services/events-talks/events-talks.service';
import { CreateEventsTalksDto, UpdateEventsTalksDto } from '../../dto/events-talks.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('events-talks')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Events talks')
export class EventsTalksController {
    constructor(private readonly nameService: EventsTalksService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateEventsTalksDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateEventsTalksDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}