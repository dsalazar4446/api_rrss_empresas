import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksController } from './events-talks.controller';

describe('EventsTalks Controller', () => {
  let controller: EventsTalksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventsTalksController],
    }).compile();

    controller = module.get<EventsTalksController>(EventsTalksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
