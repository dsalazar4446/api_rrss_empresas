import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { EventsTalksOrganicersService } from '../../services/events-talks-organicers/events-talks-organicers.service';
import { CreateEventTalkOrganizersDto, UpdateEventTalkOrganizersDto } from '../../dto/event-talk-organizers.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('events-talks-organicers')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Events talks')
export class EventsTalksOrganicersController {
    constructor(private readonly nameService: EventsTalksOrganicersService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateEventTalkOrganizersDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateEventTalkOrganizersDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}