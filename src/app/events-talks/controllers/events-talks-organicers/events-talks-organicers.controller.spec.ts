import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksOrganicersController } from './events-talks-organicers.controller';

describe('EventsTalksOrganicers Controller', () => {
  let controller: EventsTalksOrganicersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventsTalksOrganicersController],
    }).compile();

    controller = module.get<EventsTalksOrganicersController>(EventsTalksOrganicersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
