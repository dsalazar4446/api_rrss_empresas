import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { EventsTalksAttendantsStatusesService } from '../../services/events-talks-attendants-statuses/events-talks-attendants-statuses.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateEventTalkAttendantSatatuses, UpdateEventTalkAttendantSatatuses } from '../../dto/event-talk-attendant-satatuses.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('events-talks-attendants-statuses')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Events talks')
export class EventsTalksAttendantsStatusesController {
    constructor(private readonly nameService: EventsTalksAttendantsStatusesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateEventTalkAttendantSatatuses){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateEventTalkAttendantSatatuses>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}