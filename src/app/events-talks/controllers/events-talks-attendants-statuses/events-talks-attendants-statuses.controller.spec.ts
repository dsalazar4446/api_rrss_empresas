import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksAttendantsStatusesController } from './events-talks-attendants-statuses.controller';

describe('EventsTalksAttendantsStatuses Controller', () => {
  let controller: EventsTalksAttendantsStatusesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventsTalksAttendantsStatusesController],
    }).compile();

    controller = module.get<EventsTalksAttendantsStatusesController>(EventsTalksAttendantsStatusesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
