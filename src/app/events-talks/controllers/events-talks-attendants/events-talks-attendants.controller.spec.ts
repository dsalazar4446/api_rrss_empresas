import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksAttendantsController } from './events-talks-attendants.controller';

describe('EventsTalksAttendants Controller', () => {
  let controller: EventsTalksAttendantsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventsTalksAttendantsController],
    }).compile();

    controller = module.get<EventsTalksAttendantsController>(EventsTalksAttendantsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
