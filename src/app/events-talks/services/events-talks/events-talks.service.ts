import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { events_talks } from '../../../../db/entitys/events_talks';
import { CreateEventsTalksDto, UpdateEventsTalksDto } from '../../dto/events-talks.dto';
@Injectable()
export class EventsTalksService {
    constructor(@InjectRepository(events_talks) private readonly repository: Repository<events_talks>) {}
    async create(body: CreateEventsTalksDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations:[
                'eventTalkAttendantss',
                'eventTalkOrganizerss',
                'categoriess',
                'tagss',
            ],
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations:[
                'eventTalkAttendantss',
                'eventTalkOrganizerss',
                'categoriess',
                'tagss',
            ],
        });
    }

    async update(id: string, body: Partial<UpdateEventsTalksDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}