import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksService } from './events-talks.service';

describe('EventsTalksService', () => {
  let service: EventsTalksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventsTalksService],
    }).compile();

    service = module.get<EventsTalksService>(EventsTalksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
