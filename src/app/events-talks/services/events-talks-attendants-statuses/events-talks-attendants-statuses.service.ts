import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { event_talk_attendant_satatuses } from '../../../../db/entitys/event_talk_attendant_satatuses';
import { CreateEventTalkAttendantSatatuses, UpdateEventTalkAttendantSatatuses } from '../../dto/event-talk-attendant-satatuses.dto';
@Injectable()
export class EventsTalksAttendantsStatusesService {
    constructor(@InjectRepository(event_talk_attendant_satatuses) private readonly repository: Repository<event_talk_attendant_satatuses>) {}
    async create(body: CreateEventTalkAttendantSatatuses) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: ['eventTalkAttendants']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['eventTalkAttendants']
        });
    }

    async update(id: string, body: Partial<UpdateEventTalkAttendantSatatuses>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}