import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksAttendantsStatusesService } from './events-talks-attendants-statuses.service';

describe('EventsTalksAttendantsStatusesService', () => {
  let service: EventsTalksAttendantsStatusesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventsTalksAttendantsStatusesService],
    }).compile();

    service = module.get<EventsTalksAttendantsStatusesService>(EventsTalksAttendantsStatusesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
