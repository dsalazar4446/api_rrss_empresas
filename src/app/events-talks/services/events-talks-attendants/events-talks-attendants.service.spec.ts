import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksAttendantsService } from './events-talks-attendants.service';

describe('EventsTalksAttendantsService', () => {
  let service: EventsTalksAttendantsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventsTalksAttendantsService],
    }).compile();

    service = module.get<EventsTalksAttendantsService>(EventsTalksAttendantsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
