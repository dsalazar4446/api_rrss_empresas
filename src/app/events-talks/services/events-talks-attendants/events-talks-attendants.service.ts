import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { event_talk_attendants } from '../../../../db/entitys/event_talk_attendants';
import { CreateEventTalkAttendantsDto, UpdateEventTalkAttendantsDto } from '../../dto/event-talk-attendants.dto';
@Injectable()
export class EventsTalksAttendantsService {
    constructor(@InjectRepository(event_talk_attendants) private readonly repository: Repository<event_talk_attendants>) {}
    async create(body: CreateEventTalkAttendantsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'users',
                'eventTalk',
                'eventTalkAttendantSatatusess',
                'billingTransactionss',
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'users',
                'eventTalk',
                'eventTalkAttendantSatatusess',
                'billingTransactionss',
            ]
        });
    }

    async update(id: string, body: Partial<UpdateEventTalkAttendantsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}