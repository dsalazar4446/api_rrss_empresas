import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { event_talk_organizers } from '../../../../db/entitys/event_talk_organizers';
import { CreateEventTalkOrganizersDto, UpdateEventTalkOrganizersDto } from '../../dto/event-talk-organizers.dto';
@Injectable()
export class EventsTalksOrganicersService {
    constructor(@InjectRepository(event_talk_organizers) private readonly repository: Repository<event_talk_organizers>) {}
    async create(body: CreateEventTalkOrganizersDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'users',
                'eventTalk'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'users',
                'eventTalk'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateEventTalkOrganizersDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}