import { Test, TestingModule } from '@nestjs/testing';
import { EventsTalksOrganicersService } from './events-talks-organicers.service';

describe('EventsTalksOrganicersService', () => {
  let service: EventsTalksOrganicersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventsTalksOrganicersService],
    }).compile();

    service = module.get<EventsTalksOrganicersService>(EventsTalksOrganicersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
