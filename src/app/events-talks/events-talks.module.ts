import { Module } from '@nestjs/common';
import { EventsTalksController } from './controllers/events-talks/events-talks.controller';
import { EventsTalksOrganicersController } from './controllers/events-talks-organicers/events-talks-organicers.controller';
import { EventsTalksAttendantsController } from './controllers/events-talks-attendants/events-talks-attendants.controller';
import { EventsTalksAttendantsStatusesController } from './controllers/events-talks-attendants-statuses/events-talks-attendants-statuses.controller';
import { EventsTalksService } from './services/events-talks/events-talks.service';
import { EventsTalksOrganicersService } from './services/events-talks-organicers/events-talks-organicers.service';
import { EventsTalksAttendantsService } from './services/events-talks-attendants/events-talks-attendants.service';
import { EventsTalksAttendantsStatusesService } from './services/events-talks-attendants-statuses/events-talks-attendants-statuses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { events_talks } from '../../db/entitys/events_talks';
import { event_talk_organizers } from '../../db/entitys/event_talk_organizers';
import { event_talk_attendants } from '../../db/entitys/event_talk_attendants';
import { event_talk_attendant_satatuses } from '../../db/entitys/event_talk_attendant_satatuses';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      events_talks,
      event_talk_organizers,
      event_talk_attendants,
      event_talk_attendant_satatuses
    ])
  ],
  controllers: [EventsTalksController, EventsTalksOrganicersController, EventsTalksAttendantsController, EventsTalksAttendantsStatusesController],
  providers: [EventsTalksService, EventsTalksOrganicersService, EventsTalksAttendantsService, EventsTalksAttendantsStatusesService]
})
export class EventsTalksModule {}
