import { IsUUID, IsString, IsOptional } from "class-validator";
import { sectors } from "../../../db/entitys/sectors";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";
export class CreateSectorDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    sector?: sectors | null;
    @ApiModelProperty()
    @IsString()
    sector_detail?: string;
    @ApiModelPropertyOptional()
    @IsOptional()
    language_type?: string;
}
export class UpdateSectorDetailsDto extends CreateSectorDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;

}