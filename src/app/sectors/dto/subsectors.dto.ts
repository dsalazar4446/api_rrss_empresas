import { IsUUID, IsOptional, IsString } from "class-validator";
import { sectors } from "../../../db/entitys/sectors";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateSubsectorsDto {
    @ApiModelProperty()
    @IsUUID()
    sector?: sectors | null;

    @ApiModelPropertyOptional()
    @IsOptional()
    @IsString()
    subsector_code?: string | null;

}
export class UpdateSubsectorsDto extends CreateSubsectorsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}