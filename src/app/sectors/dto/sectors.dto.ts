
import { IsUUID, IsString, IsOptional } from "class-validator";
import { sectors } from "../../../db/entitys/sectors";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";
export class CreateSectorsDto {
    @ApiModelProperty()
    @IsUUID()
    sectors?: sectors | null;
    @ApiModelPropertyOptional()
    @IsOptional()
    @IsString()
    sector_code?:string;
}
export class UpdateSectorsDto extends CreateSectorsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;

}