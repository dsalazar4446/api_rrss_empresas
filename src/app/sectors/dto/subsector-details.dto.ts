import { IsUUID, IsString, IsOptional } from "class-validator";
import { subsectors } from "../../../db/entitys/subsectors";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateSubsectorDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    subsector?: subsectors | null;
    @ApiModelProperty()
    @IsString()
    subsector_detail?: string;
    @ApiModelPropertyOptional()
    @IsOptional()
    language_type?: string;
}
export class UpdateSubsectorDetailsDto extends CreateSubsectorDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}