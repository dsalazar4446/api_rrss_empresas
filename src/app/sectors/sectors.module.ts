import { Module } from '@nestjs/common';
import { SectorsController } from './controllers/sectors/sectors.controller';
import { SectorsDetailsController } from './controllers/sectors-details/sectors-details.controller';
import { SubsectorsController } from './controllers/subsectors/subsectors.controller';
import { SubsectorsDetailsController } from './controllers/subsectors-details/subsectors-details.controller';
import { SectorsService } from './services/sectors/sectors.service';
import { SectorsDetailsService } from './services/sectors-details/sectors-details.service';
import { SubsectorsDetailsService } from './services/subsectors-details/subsectors-details.service';
import { SubsectorsService } from './services/subsectors/subsectors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { sectors } from '../../db/entitys/sectors';
import { sector_details } from '../../db/entitys/sector_details';
import { subsectors } from '../../db/entitys/subsectors';
import { subsector_details } from '../../db/entitys/subsector_details';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      sectors,
      sector_details,
      subsectors,
      subsector_details
    ])
  ],
  controllers: [SectorsController, SectorsDetailsController, SubsectorsController, SubsectorsDetailsController],
  providers: [SectorsService, SectorsDetailsService, SubsectorsDetailsService, SubsectorsService]
})
export class SectorsModule {}
