import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { sectors } from '../../../../db/entitys/sectors';
import { CreateSectorsDto, UpdateSectorsDto } from '../../dto/sectors.dto';
import * as ShortUniqueId from 'short-unique-id';


@Injectable()
export class SectorsService {
    constructor(@InjectRepository(sectors) private readonly repository: Repository<sectors>) {}
    async create(body: CreateSectorsDto) {
        const uid = new ShortUniqueId();
        body.sector_code = uid.randomUUID(12);
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'sectorDetailss',
                'subsectorss'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'sectorDetailss',
                'subsectorss'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateSectorsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}