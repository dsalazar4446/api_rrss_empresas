import { Test, TestingModule } from '@nestjs/testing';
import { SectorsDetailsService } from './sectors-details.service';

describe('SectorsDetailsService', () => {
  let service: SectorsDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SectorsDetailsService],
    }).compile();

    service = module.get<SectorsDetailsService>(SectorsDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
