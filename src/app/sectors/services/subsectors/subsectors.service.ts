import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { subsectors } from '../../../../db/entitys/subsectors';
import { CreateSubsectorsDto, UpdateSubsectorsDto } from '../../dto/subsectors.dto';
import * as ShortUniqueId from 'short-unique-id';

@Injectable()
export class SubsectorsService {
    constructor(@InjectRepository(subsectors) private readonly repository: Repository<subsectors>) {}
    async create(body: CreateSubsectorsDto) {
        const uid = new ShortUniqueId();
        body.subsector_code = uid.randomUUID(12);
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:['sector']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:['sector']
        });
    }

    async update(id: string, body: Partial<UpdateSubsectorsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}