import { Test, TestingModule } from '@nestjs/testing';
import { SubsectorsService } from './subsectors.service';

describe('SubsectorsService', () => {
  let service: SubsectorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubsectorsService],
    }).compile();

    service = module.get<SubsectorsService>(SubsectorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
