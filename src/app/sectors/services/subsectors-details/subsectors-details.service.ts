import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { subsector_details } from '../../../../db/entitys/subsector_details';
import { CreateSubsectorDetailsDto, UpdateSubsectorDetailsDto } from '../../dto/subsector-details.dto';
@Injectable()
export class SubsectorsDetailsService {
    constructor(@InjectRepository(subsector_details) private readonly repository: Repository<subsector_details>) {}
    async create(body: CreateSubsectorDetailsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations:['subsector']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations:['subsector']
        });
    }

    async update(id: string, body: Partial<UpdateSubsectorDetailsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}