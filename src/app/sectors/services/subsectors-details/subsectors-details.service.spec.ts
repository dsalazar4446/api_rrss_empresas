import { Test, TestingModule } from '@nestjs/testing';
import { SubsectorsDetailsService } from './subsectors-details.service';

describe('SubsectorsDetailsService', () => {
  let service: SubsectorsDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubsectorsDetailsService],
    }).compile();

    service = module.get<SubsectorsDetailsService>(SubsectorsDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
