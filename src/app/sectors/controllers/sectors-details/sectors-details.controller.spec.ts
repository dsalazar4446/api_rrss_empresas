import { Test, TestingModule } from '@nestjs/testing';
import { SectorsDetailsController } from './sectors-details.controller';

describe('SectorsDetails Controller', () => {
  let controller: SectorsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SectorsDetailsController],
    }).compile();

    controller = module.get<SectorsDetailsController>(SectorsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
