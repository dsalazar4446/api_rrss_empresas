import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { SubsectorsDetailsService } from '../../services/subsectors-details/subsectors-details.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateSubsectorDetailsDto, UpdateSubsectorDetailsDto } from '../../dto/subsector-details.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('subsectors-details')
@ApiUseTags('Sectors')
@UseInterceptors(ResponseInterceptor)
export class SubsectorsDetailsController {
    constructor(private readonly nameService: SubsectorsDetailsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateSubsectorDetailsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body:  Partial<UpdateSubsectorDetailsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}