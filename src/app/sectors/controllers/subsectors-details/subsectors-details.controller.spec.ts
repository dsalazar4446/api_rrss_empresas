import { Test, TestingModule } from '@nestjs/testing';
import { SubsectorsDetailsController } from './subsectors-details.controller';

describe('SubsectorsDetails Controller', () => {
  let controller: SubsectorsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubsectorsDetailsController],
    }).compile();

    controller = module.get<SubsectorsDetailsController>(SubsectorsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
