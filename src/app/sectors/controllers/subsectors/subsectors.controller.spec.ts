import { Test, TestingModule } from '@nestjs/testing';
import { SubsectorsController } from './subsectors.controller';

describe('Subsectors Controller', () => {
  let controller: SubsectorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubsectorsController],
    }).compile();

    controller = module.get<SubsectorsController>(SubsectorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
