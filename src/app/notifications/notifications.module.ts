import { Module } from '@nestjs/common';
import { NotificationsController } from './controllers/notifications/notifications.controller';
import { NotificationsMsgsController } from './controllers/notifications-msgs/notifications-msgs.controller';
import { UserNotificationsController } from './controllers/user-notifications/user-notifications.controller';
import { UserNotificationsStatusesController } from './controllers/user-notifications-statuses/user-notifications-statuses.controller';
import { NotificationsService } from './services/notifications/notifications.service';
import { NotificationsMsgsService } from './services/notifications-msgs/notifications-msgs.service';
import { UserNotificationsService } from './services/user-notifications/user-notifications.service';
import { UserNotificationsStatusesService } from './services/user-notifications-statuses/user-notifications-statuses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { notifications } from '../../db/entitys/notifications';
import { notification_msgs } from '../../db/entitys/notification_msgs';
import { user_notifications } from '../../db/entitys/user_notifications';
import { user_notification_statuses } from '../../db/entitys/user_notification_statuses';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      notifications,
      notification_msgs,
      user_notifications,
      user_notification_statuses
    ])
  ],
  controllers: [NotificationsController, NotificationsMsgsController, UserNotificationsController, UserNotificationsStatusesController],
  providers: [NotificationsService, NotificationsMsgsService, UserNotificationsService, UserNotificationsStatusesService]
})
export class NotificationsModule {}
