import { Test, TestingModule } from '@nestjs/testing';
import { NotificationsMsgsController } from './notifications-msgs.controller';

describe('NotificationsMsgs Controller', () => {
  let controller: NotificationsMsgsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotificationsMsgsController],
    }).compile();

    controller = module.get<NotificationsMsgsController>(NotificationsMsgsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
