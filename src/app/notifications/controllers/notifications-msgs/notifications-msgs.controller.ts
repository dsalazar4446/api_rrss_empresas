import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { NotificationsMsgsService } from '../../services/notifications-msgs/notifications-msgs.service';
import { CreateNotificationMsgsDto, UpdateNotificationMsgsDto } from '../../dto/notification-msgs.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('notifications-msgs')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Notifications')
export class NotificationsMsgsController {
    constructor(private readonly nameService: NotificationsMsgsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateNotificationMsgsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateNotificationMsgsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}
