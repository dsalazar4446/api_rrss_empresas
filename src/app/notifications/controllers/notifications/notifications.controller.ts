import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { NotificationsService } from '../../services/notifications/notifications.service';
import { CreateNotificationsDto, UpdateNotificationsDto } from '../../dto/notifications.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiConsumes, ApiImplicitFile, ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('notifications')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Notifications')
export class NotificationsController {
    constructor(private readonly nameService: NotificationsService){}

    @Post()
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'notification_icon', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    async create(@Body() body: CreateNotificationsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'notification_icon', required: false})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateNotificationsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}