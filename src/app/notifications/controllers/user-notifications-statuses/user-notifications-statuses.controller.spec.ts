import { Test, TestingModule } from '@nestjs/testing';
import { UserNotificationsStatusesController } from './user-notifications-statuses.controller';

describe('UserNotificationsStatuses Controller', () => {
  let controller: UserNotificationsStatusesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserNotificationsStatusesController],
    }).compile();

    controller = module.get<UserNotificationsStatusesController>(UserNotificationsStatusesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
