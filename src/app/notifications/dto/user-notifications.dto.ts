import { IsUUID } from "class-validator";
import { notifications } from "../../../db/entitys/notifications";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserNotificationsDto {
    @ApiModelProperty()
    @IsUUID()
    notification?: notifications | null;
    @ApiModelProperty()
    @IsUUID()
    user?: users | null;

}

export class UpdateUserNotificationsDto extends CreateUserNotificationsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
