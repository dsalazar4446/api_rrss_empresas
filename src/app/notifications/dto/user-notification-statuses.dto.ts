import { IsUUID, IsEnum } from "class-validator";
import { user_notifications } from "../../../db/entitys/user_notifications";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserNotificationStatusesDto {
    @ApiModelProperty()
    @IsUUID()
    userNotification?:user_notifications | null;
    @ApiModelProperty({enum: ["send","recieved","read"]})
    @IsEnum({
        send:"send",recieved:"recieved",read:"read"
    })
    notification_status?:string;
}

export class UpdateUserNotificationStatusesDto extends CreateUserNotificationStatusesDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}