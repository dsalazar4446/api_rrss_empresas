import { IsUUID, IsOptional, IsEnum, IsString } from "class-validator";
import { notifications } from "../../../db/entitys/notifications";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateNotificationMsgsDto {
    @ApiModelProperty()
    @IsUUID()
    notifications?: notifications | null;
    @ApiModelProperty()
    @IsString()
    notification_msg_header?: string;
    @ApiModelProperty()
    @IsString()
    notification_msg_body?: string;
    @ApiModelProperty({enum:[ "app",  "web"]})
    @IsEnum({ app: "app", web: "web" })
    notification_target?: string;
    @ApiModelPropertyOptional()
    @IsOptional()
    language_type?: string;
}

export class UpdateNotificationMsgsDto extends CreateNotificationMsgsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}