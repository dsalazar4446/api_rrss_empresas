import { IsUUID, IsEnum, IsString } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateNotificationsDto {
    @ApiModelProperty({enum:[ "register",  "contract",  "follower",  "new_post",  "new_like",  "new_rate"]})
    @IsEnum({ register: "register", contract: "contract", follower: "follower", new_post: "new_post", new_like: "new_like", new_rate: "new_rate" })
    notification_type?: string;
    @ApiModelProperty({enum: [ "admin",  "support",  "users",  "company",  "advisors"]})
    @IsEnum({ admin: "admin", support: "support", users: "users", company: "company", advisors: "advisors" })
    notification_target?: string;
    @ApiModelProperty()
    @IsString()
    notification_icon?: string | null;
    @ApiModelProperty()
    @IsString()
    notification_mime_sound_file?: string | null;

}
export class UpdateNotificationsDto extends CreateNotificationsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}