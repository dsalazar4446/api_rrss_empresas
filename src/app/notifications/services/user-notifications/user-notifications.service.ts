
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { user_notifications } from '../../../../db/entitys/user_notifications';
import { CreateUserNotificationsDto, UpdateUserNotificationsDto } from '../../dto/user-notifications.dto';
@Injectable()
export class UserNotificationsService {
    constructor(@InjectRepository(user_notifications) private readonly repository: Repository<user_notifications>) {}
    async create(body: CreateUserNotificationsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'notification',
                'user',
                'userNotificationStatusess'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'notification',
                'user',
                'userNotificationStatusess'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateUserNotificationsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}