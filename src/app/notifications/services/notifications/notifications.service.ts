import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { notifications } from '../../../../db/entitys/notifications';
import { CreateNotificationsDto, UpdateNotificationsDto } from '../../dto/notifications.dto';
@Injectable()
export class NotificationsService {
    constructor(@InjectRepository(notifications) private readonly repository: Repository<notifications>) {}
    async create(body: CreateNotificationsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:[
                'notificationMsgss',
                'userNotificationss'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations:[
                'notificationMsgss',
                'userNotificationss'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNotificationsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}