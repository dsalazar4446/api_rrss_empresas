import { Test, TestingModule } from '@nestjs/testing';
import { UserNotificationsStatusesService } from './user-notifications-statuses.service';

describe('UserNotificationsStatusesService', () => {
  let service: UserNotificationsStatusesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserNotificationsStatusesService],
    }).compile();

    service = module.get<UserNotificationsStatusesService>(UserNotificationsStatusesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
