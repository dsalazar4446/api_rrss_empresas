import { Test, TestingModule } from '@nestjs/testing';
import { NotificationsMsgsService } from './notifications-msgs.service';

describe('NotificationsMsgsService', () => {
  let service: NotificationsMsgsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NotificationsMsgsService],
    }).compile();

    service = module.get<NotificationsMsgsService>(NotificationsMsgsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
