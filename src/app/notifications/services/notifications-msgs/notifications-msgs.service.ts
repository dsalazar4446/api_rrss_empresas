import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { notification_msgs } from '../../../../db/entitys/notification_msgs';
import { CreateNotificationMsgsDto, UpdateNotificationMsgsDto } from '../../dto/notification-msgs.dto';
@Injectable()
export class NotificationsMsgsService {
    constructor(@InjectRepository(notification_msgs) private readonly repository: Repository<notification_msgs>) {}
    async create(body: CreateNotificationMsgsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations: [
                'notifications'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations: [
                'notifications'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNotificationMsgsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}