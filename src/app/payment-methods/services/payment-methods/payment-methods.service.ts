import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { payment_methods } from '../../../../db/entitys/payment_methods';
import { CreatePaymentMethodsDto, UpdatePaymentMethodsDto } from '../../dto/payment-methods.dto';
@Injectable()
export class PaymentMethodsService {
    constructor(@InjectRepository(payment_methods) private readonly repository: Repository<payment_methods>) {}
    async create(body: CreatePaymentMethodsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }

    async update(id: string, body: Partial<UpdatePaymentMethodsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}