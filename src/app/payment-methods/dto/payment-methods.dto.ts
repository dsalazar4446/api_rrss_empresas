import { IsUUID, IsString, IsOptional } from "class-validator";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreatePaymentMethodsDto {
    @ApiModelProperty()
    @IsString()
    payment_method_name?: string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    payment_method_icon?: string;

}

export class UpdatePaymentMethodsDto extends CreatePaymentMethodsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;

}