import { Module } from '@nestjs/common';
import { PaymentMethodsController } from './controllers/payment-methods/payment-methods.controller';
import { PaymentMethodsService } from './services/payment-methods/payment-methods.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { payment_methods } from '../../db/entitys/payment_methods';

@Module({
  imports: [
    TypeOrmModule.forFeature([payment_methods])
  ],
  controllers: [PaymentMethodsController],
  providers: [PaymentMethodsService]
})
export class PaymentMethodsModule {}
