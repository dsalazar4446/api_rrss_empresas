import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { AchievementsService } from '../../services/achievements/achievements.service';
import { CreateAchievementsDto, UpdateAchievementsDto } from '../../dto/achievements.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('achievements')
@ApiUseTags('Achievements')
@UseInterceptors(ResponseInterceptor)
export class AchievementsController {
    constructor(private readonly achievementsService: AchievementsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAchievementsDto){
        return await this.achievementsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.achievementsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.achievementsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAchievementsDto>){
        return await this.achievementsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.achievementsService.delete(id);
    }

}