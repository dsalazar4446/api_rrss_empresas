import { Test, TestingModule } from '@nestjs/testing';
import { AchievementsDetailsController } from './achievements-details.controller';

describe('AchievementsDetails Controller', () => {
  let controller: AchievementsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AchievementsDetailsController],
    }).compile();

    controller = module.get<AchievementsDetailsController>(AchievementsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
