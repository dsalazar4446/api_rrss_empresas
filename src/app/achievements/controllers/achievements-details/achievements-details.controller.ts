import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { AchievementsDetailsService } from '../../services/achievements-details/achievements-details.service';
import { CreateAchievementsDetailsDto, UpdateAchievementsDetailsDto } from '../../dto/achievements-details.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('achievements-details')
@ApiUseTags('Achievements')
@UseInterceptors(ResponseInterceptor)
export class AchievementsDetailsController {
    constructor(private readonly achievementsDetailsService: AchievementsDetailsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAchievementsDetailsDto){
        return await this.achievementsDetailsService.create(body);
    }

    @Get()  
    async list(@Headers('language') language: string){
        return await this.achievementsDetailsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.achievementsDetailsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAchievementsDetailsDto>){
        return await this.achievementsDetailsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.achievementsDetailsService.delete(id);
    }

}