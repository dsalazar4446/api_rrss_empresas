import { IsUUID, IsOptional, IsNumber, IsString } from 'class-validator';

export class CreateAchievementsDto {
    @IsString()
    @IsOptional()
    achievement_icon?: string;
    @IsNumber()
    achievement_transaction_count?: number;
    @IsNumber()
    achievement_five_stars_count?: number;
    @IsNumber()
    achievement_awards_count?: number;
    @IsNumber()
    achievement_one_month?: number;
    @IsNumber()
    achievement_six_months?: number;
    @IsNumber()
    achievement_one_year?: number;
    @IsNumber()
    achievement_five_years?: number;
    @IsNumber()
    achievement_ten_years?: number;
}

export class UpdateAchievementsDto extends CreateAchievementsDto {
    @IsUUID()
    id?: string;
}