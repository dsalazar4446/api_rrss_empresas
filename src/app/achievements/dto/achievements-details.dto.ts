import { IsUUID, IsString } from "class-validator";
import { achievements } from "../../../db/entitys/achievements";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateAchievementsDetailsDto{
    @ApiModelProperty()
    @IsUUID()
    achievement?:achievements | null;
    @ApiModelProperty()
    @IsString()
    achivementTitle?: string;
    @ApiModelProperty()
    @IsString()
    achivementDescription?: string
}

export class UpdateAchievementsDetailsDto extends CreateAchievementsDetailsDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}