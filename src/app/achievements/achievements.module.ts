import { Module } from '@nestjs/common';
import { AchievementsController } from './controllers/achievements/achievements.controller';
import { AchievementsDetailsController } from './controllers/achievements-details/achievements-details.controller';
import { AchievementsService } from './services/achievements/achievements.service';
import { AchievementsDetailsService } from './services/achievements-details/achievements-details.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { achievements } from '../../db/entitys/achievements';
import { achivement_details } from '../../db/entitys/achivement_details';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      achievements,
      achivement_details
    ])
  ],
  controllers: [AchievementsController, AchievementsDetailsController],
  providers: [AchievementsService, AchievementsDetailsService]
})
export class AchievementsModule {}
