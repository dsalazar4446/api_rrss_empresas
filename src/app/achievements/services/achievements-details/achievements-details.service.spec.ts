import { Test, TestingModule } from '@nestjs/testing';
import { AchievementsDetailsService } from './achievements-details.service';

describe('AchievementsDetailsService', () => {
  let service: AchievementsDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AchievementsDetailsService],
    }).compile();

    service = module.get<AchievementsDetailsService>(AchievementsDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
