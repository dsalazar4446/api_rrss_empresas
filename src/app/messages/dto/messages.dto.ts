
import { IsDateString, IsString, IsUUID } from 'class-validator';
import { chats } from '../../../db/entitys/chats';
import { users } from '../../../db/entitys/users';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateMessagesDto {
    @ApiModelProperty()
    @IsUUID()
    chat?: chats;
    @ApiModelProperty()
    @IsUUID()
    sender?: users;
    @ApiModelProperty()
    @IsString()
    message?: string;
    @ApiModelProperty()
    @IsDateString()
    message_sended?: string;
    @ApiModelProperty()
    @IsDateString()
    message_recieved?: string;
    @ApiModelProperty()
    @IsDateString()
    message_viewed?: string;
}
export class UpdateMessagesDto extends CreateMessagesDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}