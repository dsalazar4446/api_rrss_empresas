import { ChatType, OPTIONS } from '../../shared/utils/enums';
import { IsUUID, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateChatsDto {
    @ApiModelProperty()
    @IsString()
    chat_type?: ChatType;
}

export class UpdateChatsDto extends CreateChatsDto{
    @ApiModelProperty()
    @IsUUID()
    id?: number;
}
