import { PostAttachementType } from '../../shared/utils/enums';
import { IsUUID, IsString, IsEnum, IsOptional } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class  CreateMessageAttachmentsDto {
    @IsUUID()
    @ApiModelProperty()
    message_id?: string;
   
    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    message_attachment?: string;

    @IsEnum({
        VIDEO: 'video',
        IMAGE: 'image',
        AUDIO: 'audio'
    })
    @ApiModelProperty({enum:[
         'video',
         'image',
         'audio'
    ]})
    message_attachment_type?:PostAttachementType;
}

export class  UpdateMessageAttachmentsDto  extends CreateMessageAttachmentsDto{
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
