import { Module } from '@nestjs/common';
import { MessagesController } from './controllers/messages/messages.controller';
import { ChatController } from './controllers/chat/chat.controller';
import { MessagesAtachmentsController } from './controllers/messages-atachments/messages-atachments.controller';
import { MessagesService } from './services/messages/messages.service';
import { ChatsService } from './services/chats/chats.service';
import { MessagesAttachmentsService } from './services/messages-attachments/messages-attachments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { messages } from '../../db/entitys/messages';
import { message_attachments } from '../../db/entitys/message_attachments';
import { chats } from '../../db/entitys/chats';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      messages,
      message_attachments,
      chats
    ])
  ],
  controllers: [MessagesController, ChatController, MessagesAtachmentsController],
  providers: [MessagesService, ChatsService, MessagesAttachmentsService]
})
export class MessagesModule {}
