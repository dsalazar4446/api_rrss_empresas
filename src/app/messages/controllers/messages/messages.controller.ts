import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { MessagesService } from '../../services/messages/messages.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateMessagesDto, UpdateMessagesDto } from '../../dto/messages.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('messages')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Messages')
export class MessagesController {
    constructor(private readonly chatsService: MessagesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateMessagesDto){
        return await this.chatsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.chatsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.chatsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateMessagesDto>){
        return await this.chatsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.chatsService.delete(id);
    }

}