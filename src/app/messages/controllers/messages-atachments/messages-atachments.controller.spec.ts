import { Test, TestingModule } from '@nestjs/testing';
import { MessagesAtachmentsController } from './messages-atachments.controller';

describe('MessagesAtachments Controller', () => {
  let controller: MessagesAtachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessagesAtachmentsController],
    }).compile();

    controller = module.get<MessagesAtachmentsController>(MessagesAtachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
