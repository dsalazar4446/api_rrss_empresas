import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { MessagesAttachmentsService } from '../../services/messages-attachments/messages-attachments.service';
import { CreateMessageAttachmentsDto, UpdateMessageAttachmentsDto } from '../../dto/message-attachment.dto';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('messages-atachments')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Messages')
export class MessagesAtachmentsController   {
    constructor(private readonly messagesAttachmentsService: MessagesAttachmentsService){}

    @Post()
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'message_attachment', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateMessageAttachmentsDto){
        return await this.messagesAttachmentsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.messagesAttachmentsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.messagesAttachmentsService.detail(id, language);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'message_attachment', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateMessageAttachmentsDto>){
        return await this.messagesAttachmentsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.messagesAttachmentsService.delete(id);
    }

}