import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { ChatsService } from '../../services/chats/chats.service';
import { CreateChatsDto, UpdateChatsDto } from '../../dto/chat.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('chat')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Messages')
export class ChatController {
    constructor(private readonly chatsService:  ChatsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateChatsDto){
        return await this.chatsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.chatsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.chatsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateChatsDto>){
        return await this.chatsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.chatsService.delete(id);
    }

}