import { Test, TestingModule } from '@nestjs/testing';
import { MessagesAttachmentsService } from './messages-attachments.service';

describe('MessagesAttachmentsService', () => {
  let service: MessagesAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MessagesAttachmentsService],
    }).compile();

    service = module.get<MessagesAttachmentsService>(MessagesAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
