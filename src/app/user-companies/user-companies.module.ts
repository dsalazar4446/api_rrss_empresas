import { Module } from '@nestjs/common';
import { UserCompaniesController } from './controllers/user-companies/user-companies.controller';
import { UserCompaniesAboutsController } from './controllers/user-companies-abouts/user-companies-abouts.controller';
import { UserCompaniesViewsController } from './controllers/user-companies-views/user-companies-views.controller';
import { UserCompaniesService } from './services/user-companies/user-companies.service';
import { UserCompaniesAboutService } from './services/user-companies-about/user-companies-about.service';
import { UserCompaniesViewsService } from './services/user-companies-views/user-companies-views.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { user_companies } from '../../db/entitys/user_companies';
import { user_company_abouts } from '../../db/entitys/user_company_abouts';
import { user_company_views } from '../../db/entitys/user_company_views';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      user_companies,
      user_company_abouts,
      user_company_views
    ])
  ],
  controllers: [UserCompaniesController, UserCompaniesAboutsController, UserCompaniesViewsController],
  providers: [UserCompaniesService, UserCompaniesAboutService, UserCompaniesViewsService]
})
export class UserCompaniesModule {}
