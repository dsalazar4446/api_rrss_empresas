import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { user_companies } from '../../../../db/entitys/user_companies';
import { CreateUserCompaniesDto, UpdateUserCompaniesDto } from '../../dto/user-companies.dto';
@Injectable()
export class UserCompaniesService {
    constructor(@InjectRepository(user_companies) private readonly repository: Repository<user_companies>) {}
    async create(body: CreateUserCompaniesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
           relations:[
            'user',
            'city',
            'subsector',
            'userCompanyAboutss',
            'userCompanyViewss',
           ]
        });
    }

    async listByUser(userId: string){
        return await this.repository.findOne({
            where: {
                user: userId
            }
        })
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:[
                'user',
                'city',
                'subsector',
                'userCompanyAboutss',
                'userCompanyViewss',
               ]
        });
    }

    async update(id: string, body: Partial<UpdateUserCompaniesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}