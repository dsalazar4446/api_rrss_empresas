import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesService } from './user-companies.service';

describe('UserCompaniesService', () => {
  let service: UserCompaniesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserCompaniesService],
    }).compile();

    service = module.get<UserCompaniesService>(UserCompaniesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
