import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesAboutService } from './user-companies-about.service';

describe('UserCompaniesAboutService', () => {
  let service: UserCompaniesAboutService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserCompaniesAboutService],
    }).compile();

    service = module.get<UserCompaniesAboutService>(UserCompaniesAboutService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
