import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesViewsService } from './user-companies-views.service';

describe('UserCompaniesViewsService', () => {
  let service: UserCompaniesViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserCompaniesViewsService],
    }).compile();

    service = module.get<UserCompaniesViewsService>(UserCompaniesViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
