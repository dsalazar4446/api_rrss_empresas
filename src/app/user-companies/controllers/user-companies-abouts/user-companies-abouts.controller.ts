import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { UserCompaniesAboutService } from '../../services/user-companies-about/user-companies-about.service';
import { CreateUserCompanyAboutsDto, UpdateUserCompanyAboutsDto } from '../../dto/user-company-abouts.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('user-companies-abouts')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('User companies')
export class UserCompaniesAboutsController {
    constructor(private readonly nameService: UserCompaniesAboutService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserCompanyAboutsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserCompanyAboutsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}