import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesAboutsController } from './user-companies-abouts.controller';

describe('UserCompaniesAbouts Controller', () => {
  let controller: UserCompaniesAboutsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCompaniesAboutsController],
    }).compile();

    controller = module.get<UserCompaniesAboutsController>(UserCompaniesAboutsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
