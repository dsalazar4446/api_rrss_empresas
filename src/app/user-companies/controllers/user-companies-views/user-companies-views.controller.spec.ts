import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesViewsController } from './user-companies-views.controller';

describe('UserCompaniesViews Controller', () => {
  let controller: UserCompaniesViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCompaniesViewsController],
    }).compile();

    controller = module.get<UserCompaniesViewsController>(UserCompaniesViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
