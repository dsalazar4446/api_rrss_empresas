import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { UserCompaniesService } from '../../services/user-companies/user-companies.service';
import { CreateUserCompaniesDto, UpdateUserCompaniesDto } from '../../dto/user-companies.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiConsumes, ApiImplicitFile, ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('user-companies')
@ApiUseTags('User companies')
@UseInterceptors(ResponseInterceptor)
export class UserCompaniesController {
    constructor(private readonly nameService: UserCompaniesService){}

    @Post()
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'user_company_logo', required: false})
    @ApiImplicitFile({ name: 'user_company_image', required: false})
    @ApiImplicitFile({ name: 'user_company_document_attachment', required: true})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateUserCompaniesDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Get('/user/:userId')
    async listByUser(@Param('userId') userId: string){
        return await this.nameService.listByUser(userId);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'user_company_logo', required: false})
    @ApiImplicitFile({ name: 'user_company_image', required: false})
    @ApiImplicitFile({ name: 'user_company_document_attachment', required: false})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateUserCompaniesDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}