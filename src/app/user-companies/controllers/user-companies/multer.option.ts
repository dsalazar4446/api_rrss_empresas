
import { diskStorage } from 'multer';
import { extname } from "path";
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';

export const arrayFiles = [
    { name: 'user_company_logo', maxCount: 1 },
    { name: 'user_company_image', maxCount: 1 },
    { name: 'user_company_document_attachment', maxCount: 1 },
];

export const localOptions: MulterOptions = {
    storage: diskStorage({
        destination: './uploads/use-companies'
        , filename: (req, file, cb) => {
            let extValidas = [
                '.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG', '.svg', '.SVG', 
                '.pdf', '.PDF', '.odt', '.ODT', '.doc','.DOC', '.docx', '.DOCX', 
            ];

            if (extValidas.indexOf(extname(file.originalname)) < 0) {
                cb('valid extensions: ' + extValidas.join(', '));
                return;
            }
            const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
            if (file.fieldname === 'user_company_logo') {
                req.body.user_company_logo = `${randomName}${extname(file.originalname)}`;
            }
            if (file.fieldname === 'user_company_image') {
                req.body.user_company_image = `${randomName}${extname(file.originalname)}`;
            }
            if (file.fieldname === 'user_company_document_attachment') {
                req.body.user_company_document_attachment = `${randomName}${extname(file.originalname)}`;
            }
            cb(null, `${randomName}${extname(file.originalname)}`)
        }
    })
}