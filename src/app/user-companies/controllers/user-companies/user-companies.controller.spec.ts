import { Test, TestingModule } from '@nestjs/testing';
import { UserCompaniesController } from './user-companies.controller';

describe('UserCompanies Controller', () => {
  let controller: UserCompaniesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCompaniesController],
    }).compile();

    controller = module.get<UserCompaniesController>(UserCompaniesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
