import { IsUUID, IsString, IsNumber, IsOptional, IsEnum } from "class-validator";
import { user_companies } from "../../../db/entitys/user_companies";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateUserCompanyViewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
    @ApiModelProperty()
    @IsUUID()
    userCompany?:user_companies | null;
    @ApiModelProperty()
    @IsUUID()
    user?:users | null;
    @ApiModelProperty()
    @IsNumber()
    user_company_share?:number;
    @ApiModelProperty()
    @IsNumber()
    user_company_like?:number;
    @ApiModelProperty()
    @IsNumber()
    user_company_rate?:number | null;
    @ApiModelProperty()
    @IsString()
    user_company_review?:string | null;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr"
    })
    @IsOptional()
    language_type?:string | null;  
}

export class UpdateUserCompanyViewsDto extends CreateUserCompanyViewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}