import { IsString, IsUUID, IsEnum, IsOptional } from "class-validator";
import { user_companies } from "../../../db/entitys/user_companies";
import { Column } from "typeorm";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateUserCompanyAboutsDto {
    @ApiModelProperty()
    @IsUUID()
    userCompany?:user_companies | null;
    @ApiModelProperty()
    @IsString()
    user_company_about?:string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr"
    })
    @IsOptional()
    language_type?:string;
        
}

export class UpdateUserCompanyAboutsDto extends CreateUserCompanyAboutsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}