import { IsUUID, IsString } from "class-validator";
import { users } from "../../../db/entitys/users";
import { cities } from "../../../db/entitys/cities";
import { subsectors } from "../../../db/entitys/subsectors";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserCompaniesDto {

    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsUUID()
    city?: cities;
    @ApiModelProperty()
    @IsUUID()
    subsector?: subsectors;
    @ApiModelProperty()
    @IsString()
    user_company_identification?: string;
    @ApiModelProperty()
    @IsString()
    user_company_name?: string;
    @ApiModelProperty()
    @IsString()
    user_company_logo?: string;
    @ApiModelProperty()
    @IsString()
    user_company_image?: string;
    @ApiModelProperty()
    @IsString()
    user_company_document_attachment?: string;
    @ApiModelProperty()
    @IsString()
    user_company_politics_attachment?: string;
    @ApiModelProperty()
    @IsString()
    user_company_color?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_latitude?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_longitude?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_address_1?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_adddres_2?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_website?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_email?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_phone?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_cellphone?: string | null;
    @ApiModelProperty()
    @IsString()
    user_company_whatsapp?: string | null;
}
export class UpdateUserCompaniesDto extends CreateUserCompaniesDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}