import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, createQueryBuilder } from 'typeorm';
import { news } from '../../../../db/entitys/news';
import { CreateNewsDto, UpdateNewsDto } from '../../dto/news';
import { categories } from '../../../../db/entitys/categories';
import { tags } from '../../../../db/entitys/tags';
@Injectable()
export class NewsService {
    constructor(
        @InjectRepository(news) private readonly repository: Repository<news>,
        @InjectRepository(categories) private readonly categoriesRpo: Repository<categories>,
        @InjectRepository(tags) private readonly tagsRpo: Repository<tags>
    ) {}
    async create(body: CreateNewsDto, categoryId: string, tagId: string) {
        const category = await this.categoriesRpo.findOne(categoryId)
        if(!category){
            return new HttpException('category not found', HttpStatus.NOT_FOUND)
        }
        const tag = await this.tagsRpo.findOne(tagId)
        if(!tag){
            return new HttpException('tag not found', HttpStatus.NOT_FOUND)
        }

        const result = await this.repository.save(body)
        await createQueryBuilder()
            .relation(news, 'categoriess')
            .of(result)
            .add(category)

        await createQueryBuilder()
            .relation(news, 'tagss')
            .of(result)
            .add(tag)
 
        return this.repository.save(result);
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:[
                'newsAttachmentss',
                'newsDetailss',
                'newsUserViewss',
                'tagss',
                'categoriess',
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:[
                'newsAttachmentss',
                'newsDetailss',
                'newsUserViewss',
                'categoriess',
                'tagss'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNewsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}