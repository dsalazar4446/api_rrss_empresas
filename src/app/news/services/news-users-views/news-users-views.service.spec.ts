import { Test, TestingModule } from '@nestjs/testing';
import { NewsUsersViewsService } from './news-users-views.service';

describe('NewsUsersViewsService', () => {
  let service: NewsUsersViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsUsersViewsService],
    }).compile();

    service = module.get<NewsUsersViewsService>(NewsUsersViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
