import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { news_user_views } from '../../../../db/entitys/news_user_views';
import { CreateNewsUserViewsDto, UpdateNewsUserViewsDto } from '../../dto/news-user-views.dto';
@Injectable()
export class NewsUsersViewsService {
    constructor(@InjectRepository(news_user_views) private readonly repository: Repository<news_user_views>) {}
    async create(body: CreateNewsUserViewsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'news',
                'user'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'news',
                'user'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNewsUserViewsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}