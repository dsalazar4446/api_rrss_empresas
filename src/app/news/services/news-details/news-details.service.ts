import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { news_details } from '../../../../db/entitys/news_details';
import { CreateNewsDetailsDto, UpdateNewsDetailsDto } from '../../dto/news_details';
@Injectable()
export class NewsDetailsService {
    constructor(@InjectRepository(news_details) private readonly repository: Repository<news_details>) {}
    async create(body: CreateNewsDetailsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations: [
                'news'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations: [
                'news'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNewsDetailsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}