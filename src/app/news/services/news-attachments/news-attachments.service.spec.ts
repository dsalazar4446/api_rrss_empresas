import { Test, TestingModule } from '@nestjs/testing';
import { NewsAttachmentsService } from './news-attachments.service';

describe('NewsAttachmentsService', () => {
  let service: NewsAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsAttachmentsService],
    }).compile();

    service = module.get<NewsAttachmentsService>(NewsAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
