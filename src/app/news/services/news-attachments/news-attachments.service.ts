
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { news_attachments } from '../../../../db/entitys/news_attachments';
import { CreateNewsAttachmentsDto, UpdateNewsAttachmentsDto } from '../../dto/news-attachments.dto';
@Injectable()
export class NewsAttachmentsService {
    constructor(@InjectRepository(news_attachments) private readonly repository: Repository<news_attachments>) {}
    async create(body: CreateNewsAttachmentsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'news'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'news'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateNewsAttachmentsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}