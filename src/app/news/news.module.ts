import { Module } from '@nestjs/common';
import { NewsController } from './controllers/news/news.controller';
import { NewsAttachmentsController } from './controllers/news-attachments/news-attachments.controller';
import { NewsDetailsController } from './controllers/news-details/news-details.controller';
import { NewsUserViewsController } from './controllers/news-user-views/news-user-views.controller';
import { NewsService } from './services/news/news.service';
import { NewsAttachmentsService } from './services/news-attachments/news-attachments.service';
import { NewsDetailsService } from './services/news-details/news-details.service';
import { NewsUsersViewsService } from './services/news-users-views/news-users-views.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { news } from '../../db/entitys/news';
import { news_attachments } from '../../db/entitys/news_attachments';
import { news_details } from '../../db/entitys/news_details';
import { news_user_views } from '../../db/entitys/news_user_views';
import { categories } from '../../db/entitys/categories';
import { tags } from '../../db/entitys/tags';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      news,
      news_attachments,
      news_details,
      news_user_views,
      categories,
      tags,
    ])
  ],
  controllers: [NewsController, NewsAttachmentsController, NewsDetailsController, NewsUserViewsController],
  providers: [NewsService, NewsAttachmentsService, NewsDetailsService, NewsUsersViewsService]
})
export class NewsModule {}
