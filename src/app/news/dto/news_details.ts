import { IsEnum, IsString, IsUUID, IsOptional } from "class-validator";
import { news } from "../../../db/entitys/news";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateNewsDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    news?: news;
    @ApiModelProperty()
    @IsString()
    news_title?: string;
    @ApiModelProperty()
    @IsString()
    news_description?: string;
    @ApiModelPropertyOptional({enum:[ "es",  "en",  "it",  "pr"]})
    @IsEnum({
        es: "es", en: "en", it: "it", pr: "pr"
    })
    @IsOptional()
    language_type?: string;
}

export class UpdateNewsDetailsDto extends CreateNewsDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}