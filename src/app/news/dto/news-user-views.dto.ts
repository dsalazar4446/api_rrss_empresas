import { IsNumber, IsUUID } from "class-validator";
import { news } from "../../../db/entitys/news";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateNewsUserViewsDto {
    @ApiModelProperty()
    @IsUUID()
    news?: news;
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsNumber()
    news_user_share?: number;
    @ApiModelProperty()
    @IsNumber()
    news_user_like?: number;
}

export class UpdateNewsUserViewsDto extends CreateNewsUserViewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}