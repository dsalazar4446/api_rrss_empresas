import { IsUUID, IsEnum } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateNewsDto {
    @ApiModelProperty({enum:[ "all",  "company",  "user"]})
    @IsEnum({
        all: "all", company: "company", user: "user"
    })
    news_target?: string;
    @ApiModelProperty({enum:[ "news",  "tips"]})
    @IsEnum({
        news: "news", tips: "tips"
    })
    news_type?: string;
   
}

export class UpdateNewsDto extends CreateNewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}