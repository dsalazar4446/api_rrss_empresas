import { IsUUID, IsEnum, IsString, IsOptional } from "class-validator";
import { news } from "../../../db/entitys/news";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateNewsAttachmentsDto {
    @ApiModelProperty()
    @IsUUID()
    news?: news;
    @ApiModelProperty()
    @IsString()
    news_attachment?: string;
    @ApiModelProperty({enum:["1", "2", "3"]})
    @IsEnum({
        1: "1", 2: "2", 3: "3"
    })
    @ApiModelPropertyOptional()
    @IsOptional()
    news_attachment_type?: string;

}

export class UpdateNewsAttachmentsDto extends CreateNewsAttachmentsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}