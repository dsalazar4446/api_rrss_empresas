import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { NewsUsersViewsService } from '../../services/news-users-views/news-users-views.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateNewsUserViewsDto, UpdateNewsUserViewsDto } from '../../dto/news-user-views.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('news-user-views')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('News')
export class NewsUserViewsController {
    constructor(private readonly nameService: NewsUsersViewsService){}

    @Post() 
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateNewsUserViewsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateNewsUserViewsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}