import { Test, TestingModule } from '@nestjs/testing';
import { NewsUserViewsController } from './news-user-views.controller';

describe('NewsUserViews Controller', () => {
  let controller: NewsUserViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsUserViewsController],
    }).compile();

    controller = module.get<NewsUserViewsController>(NewsUserViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
