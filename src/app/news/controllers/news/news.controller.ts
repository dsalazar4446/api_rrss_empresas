import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { NewsService } from '../../services/news/news.service';
import { CreateNewsDto, UpdateNewsDto } from '../../dto/news';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('news')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('News')
export class NewsController {
    constructor(private readonly nameService: NewsService){}

    @Post(':categoryId/:tagId')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateNewsDto, @Param('categoryId') categoryId: string, @Param('tagId') tagId:string){
        return await this.nameService.create(body, categoryId, tagId);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateNewsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}