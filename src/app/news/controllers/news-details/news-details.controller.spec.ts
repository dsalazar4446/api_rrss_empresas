import { Test, TestingModule } from '@nestjs/testing';
import { NewsDetailsController } from './news-details.controller';

describe('NewsDetails Controller', () => {
  let controller: NewsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsDetailsController],
    }).compile();

    controller = module.get<NewsDetailsController>(NewsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
