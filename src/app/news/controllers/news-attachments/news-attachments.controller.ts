import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { NewsAttachmentsService } from '../../services/news-attachments/news-attachments.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateNewsAttachmentsDto, UpdateNewsAttachmentsDto } from '../../dto/news-attachments.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiImplicitFile, ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('news-attachments')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('News')
export class NewsAttachmentsController {
    constructor(private readonly nameService: NewsAttachmentsService) { }

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiImplicitFile({ name: 'news_attachment', required: false })
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    async create(@Body() body: CreateNewsAttachmentsDto) {
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string) {
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string) {
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @ApiImplicitFile({ name: 'news_attachment', required: false})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateNewsAttachmentsDto>) {
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return await this.nameService.delete(id);
    }

}