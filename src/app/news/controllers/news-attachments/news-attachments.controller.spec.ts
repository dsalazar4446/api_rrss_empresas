import { Test, TestingModule } from '@nestjs/testing';
import { NewsAttachmentsController } from './news-attachments.controller';

describe('NewsAttachments Controller', () => {
  let controller: NewsAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsAttachmentsController],
    }).compile();

    controller = module.get<NewsAttachmentsController>(NewsAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
