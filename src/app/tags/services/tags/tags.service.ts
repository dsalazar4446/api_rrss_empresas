import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { tags } from '../../../../db/entitys/tags';
import { CreateTagsDto, UpdateTagsDto } from '../../dto/tags.dto';
@Injectable()
export class TagsService {
    constructor(@InjectRepository(tags) private readonly repository: Repository<tags>) {}
    async create(body: CreateTagsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations: [
                'eventsTalkss',
                'newss',
                'userPostss',
                'userss',
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations: [
                'eventsTalkss',
                'newss',
                'userPostss',
                'userss',
            ]
        });
    }

    async update(id: string, body: Partial<UpdateTagsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}