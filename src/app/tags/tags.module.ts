import { Module } from '@nestjs/common';
import { TagsController } from './controllers/tags/tags.controller';
import { TagsService } from './services/tags/tags.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { tags } from '../../db/entitys/tags';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      tags
    ])
  ],
  controllers: [TagsController],
  providers: [TagsService]
})
export class TagsModule {}
