import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateTagsDto {
    @ApiModelProperty()
    @IsString()
    tag_name?:string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr"
    })
    @IsOptional()
    language_type?:string;
}

export class UpdateTagsDto extends CreateTagsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}