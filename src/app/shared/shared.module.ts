import { Module, HttpModule } from '@nestjs/common';
import { SendgridService } from './services/sendgrid/sendgrid.service';
import { PayuService } from './services/payu/payu.service';

@Module({
  imports: [HttpModule],
  providers: [SendgridService, PayuService],
  exports: [PayuService,SendgridService]
})
export class SharedModule {}
