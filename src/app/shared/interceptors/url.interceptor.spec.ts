import { UrlInterceptor } from './url.interceptor';

describe('UrlInterceptor', () => {
  it('should be defined', () => {
    expect(new UrlInterceptor()).toBeDefined();
  });
});
