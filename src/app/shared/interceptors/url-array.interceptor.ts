import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UrlArrayInterceptor implements NestInterceptor {
  constructor(private param: string, private directory: string) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(
        element => {
          element.forEach(elem => {
            if (elem.hasOwnProperty(this.param)) {
              elem[this.param] = `${process.env.URL}${this.directory}/${elem[this.param]}`
            }
          });
          return element
        }
      )
    );
  }
}
