import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class UrlInterceptor implements NestInterceptor {
  constructor(private param: string, private directory: string) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

    console.log(this.param)
    return next.handle().pipe(
      map(
        element => {
          if (element.hasOwnProperty(this.param)) {
            element[this.param] = `${process.env.URL}${this.directory}/${element[this.param]}`
          }
          return element
        }
      )
    );
  }
}
