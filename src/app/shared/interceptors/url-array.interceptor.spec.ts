import { UrlArrayInterceptor } from './url-array.interceptor';

describe('UrlArrayInterceptor', () => {
  it('should be defined', () => {
    expect(new UrlArrayInterceptor()).toBeDefined();
  });
});
