import { CallHandler, ExecutionContext, Injectable, NestInterceptor, HttpException, HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class LangInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();
    if(request.headers.language){
      request.body.language_type = request.headers.language
    }else{
      throw new HttpException('language type not specificed',HttpStatus.NOT_FOUND)
    }

    return next.handle();
  }
}
