import { RelationOptions } from "typeorm";

export enum Gender {
    MALE = 'male', 
    FEMALE = 'female'
}

export enum UserStatus {
    PENDING = 'pending', 
    ACTIVE = 'active', 
    UNACTIVE = 'unactive'
}

export enum LanguageType {
    ES = 'es',
    EN = 'en',
    IT = 'it', 
    PR = 'pr'
}

export enum PostAttachementType {
    VIDEO = 'video',
    IMAGE = 'image',
    AUDIO = 'audio'
}

export enum PaymentRecievePay {
    RECIEVE = 'recieve',
    PAY = 'pay'
} 

export enum OS {
    CHROME = 'chrome',
    FIREFOX = 'firefox',
    OPERA = 'opera',
    EXPLORER = 'explorer',
    ANDROID = 'android',
    IOS = 'ios',
    WIN = 'win'
}
export enum ProductServiceTypes {
    PRODUCT ='product',
    SERVICE ='service'
}

export enum ChatType {
    helpy = 'helpy',
    support = 'support'
}

export const OPTIONS:RelationOptions = { onDelete:'CASCADE', onUpdate: 'CASCADE' }