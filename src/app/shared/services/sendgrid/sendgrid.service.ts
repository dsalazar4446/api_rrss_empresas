import { Injectable } from '@nestjs/common';
import * as sendgrid from '@sendgrid/mail';
// import { nodemailer } from '../../../config/nodemailer.config';
// import * as Email from 'email-templates';
// const email = new Email();

@Injectable()
export class SendgridService {
    private nodemailer: any;
    constructor(){
        sendgrid.setApiKey(process.env.SENDGRID_API_KEY);
    }
    async sendMail(from: string, to: string[], subject: string, text: string, html: string) {
        const msg = {to, from, subject, text, html};
        sendgrid.send(msg);
    }
}