import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class PayuService {
    private url: string = ' https://api.payulatam.com/';
    private endpoint = {
        payments: 'payments-api/4.0/service.cgi',
        reports: 'reports-api/4.0/service.cgi'
    };
    private apiKey: string
    private apiLogin: string
    constructor(private http:HttpService){}

    ping(){
        this.http.post(
            `${this.url}${this.endpoint.reports}`,
            {
                test: false,
                language:'es',
                merchant: {
                    apiLogin: this.apiLogin,
                    apiKey: this.apiKey
                }
            }
        ).toPromise()
    }
    setApiKey(apiKey: string){
        this.apiKey = apiKey;
    }
    setApiLogin(apiLogin: string){
        this.apiLogin = apiLogin
    } 
}
