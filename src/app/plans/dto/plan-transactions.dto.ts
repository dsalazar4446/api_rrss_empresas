import { IsUUID, IsDate, IsDateString } from "class-validator";
import { plans } from "../../../db/entitys/plans";
import { billing_transactions } from "../../../db/entitys/billing_transactions";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreatePlanTransactionsDto {
    @ApiModelProperty()
    @IsUUID()
    plan?:plans;
    @ApiModelProperty()
    @IsUUID()
    billingTransaction?:billing_transactions;
    @ApiModelProperty()
    @IsDateString()
    plan_start?:Date;
    @ApiModelProperty()
    @IsDateString()
    plan_end?:Date;   
}

export class UpdatePlanTransactionsDto extends CreatePlanTransactionsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}