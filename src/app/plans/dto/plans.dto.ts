import { IsUUID, IsString, IsNumber } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreatePlansDto {
    @ApiModelProperty()
    @IsString()
    plan_icon?:string;
    @ApiModelProperty()
    @IsString()
    plan_image?:string;
    @ApiModelProperty()
    @IsNumber()
    plan_talks?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_events?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_tips?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_advisory?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_advisory_cant?:number;
       
}
export class UpdatePlansDto extends CreatePlansDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}