import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { plans } from "../../../db/entitys/plans";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreatePlanDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    plan?:plans;
    @ApiModelProperty()
    @IsString()
    plan_title?:string;
    @ApiModelProperty()
    @IsString()
    plan_description?:string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr"
    })
    @IsOptional()
    language_type?:string;    
}

export class UpdatePlanDetailsDto extends CreatePlanDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}
