import { IsUUID, IsNumber } from "class-validator";
import { plans } from "../../../db/entitys/plans";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreatePlanPricingsDto {
    @ApiModelProperty()
    @IsUUID()
    plan?:plans;
    @ApiModelProperty()
    @IsNumber()
    plan_taxes?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_retefte?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_ammount?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_total?:number;
    @ApiModelProperty()
    @IsNumber()
    plan_price_status?:number;
}

export class UpdatePlanPricingsDto extends CreatePlanPricingsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}