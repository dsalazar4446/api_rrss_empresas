import { Module } from '@nestjs/common';
import { PlansController } from './controllers/plans/plans.controller';
import { PlansTransactionsController } from './controllers/plans-transactions/plans-transactions.controller';
import { PlansPricingsController } from './controllers/plans-pricings/plans-pricings.controller';
import { PlansDetailsController } from './controllers/plans-details/plans-details.controller';
import { PlansService } from './services/plans/plans.service';
import { PlansDetailsService } from './services/plans-details/plans-details.service';
import { PlansPricingsService } from './services/plans-pricings/plans-pricings.service';
import { PlansTransactionsService } from './services/plans-transactions/plans-transactions.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { plans } from '../../db/entitys/plans';
import { plan_details } from '../../db/entitys/plan_details';
import { plan_pricings } from '../../db/entitys/plan_pricings';
import { plan_transactions } from '../../db/entitys/plan_transactions';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      plans,
      plan_details,
      plan_pricings,
      plan_transactions
    ])
  ],
  controllers: [PlansController, PlansTransactionsController, PlansPricingsController, PlansDetailsController],
  providers: [PlansService, PlansDetailsService, PlansPricingsService, PlansTransactionsService]
})
export class PlansModule {}
