import { Test, TestingModule } from '@nestjs/testing';
import { PlansPricingsController } from './plans-pricings.controller';

describe('PlansPricings Controller', () => {
  let controller: PlansPricingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlansPricingsController],
    }).compile();

    controller = module.get<PlansPricingsController>(PlansPricingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
