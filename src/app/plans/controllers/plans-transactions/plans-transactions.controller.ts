import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { PlansTransactionsService } from '../../services/plans-transactions/plans-transactions.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreatePlanTransactionsDto, UpdatePlanTransactionsDto } from '../../dto/plan-transactions.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('plans-transactions')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Plans')
export class PlansTransactionsController {
    constructor(private readonly nameService: PlansTransactionsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreatePlanTransactionsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdatePlanTransactionsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}