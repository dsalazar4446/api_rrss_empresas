import { Test, TestingModule } from '@nestjs/testing';
import { PlansTransactionsController } from './plans-transactions.controller';

describe('PlansTransactions Controller', () => {
  let controller: PlansTransactionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlansTransactionsController],
    }).compile();

    controller = module.get<PlansTransactionsController>(PlansTransactionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
