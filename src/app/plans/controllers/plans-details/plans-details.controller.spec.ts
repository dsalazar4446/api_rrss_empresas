import { Test, TestingModule } from '@nestjs/testing';
import { PlansDetailsController } from './plans-details.controller';

describe('PlansDetails Controller', () => {
  let controller: PlansDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlansDetailsController],
    }).compile();

    controller = module.get<PlansDetailsController>(PlansDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
