import { Test, TestingModule } from '@nestjs/testing';
import { PlansTransactionsService } from './plans-transactions.service';

describe('PlansTransactionsService', () => {
  let service: PlansTransactionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlansTransactionsService],
    }).compile();

    service = module.get<PlansTransactionsService>(PlansTransactionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
