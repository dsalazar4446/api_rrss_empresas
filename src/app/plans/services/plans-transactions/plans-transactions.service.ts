import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plan_transactions } from '../../../../db/entitys/plan_transactions';
import { CreatePlanTransactionsDto, UpdatePlanTransactionsDto } from '../../dto/plan-transactions.dto';
@Injectable()
export class PlansTransactionsService {
    constructor(@InjectRepository(plan_transactions) private readonly repository: Repository<plan_transactions>) {}
    async create(body: CreatePlanTransactionsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
           relations: ['plan', 'billingTransaction']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['plan', 'billingTransaction']
        });
    }

    async update(id: string, body: Partial<UpdatePlanTransactionsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}