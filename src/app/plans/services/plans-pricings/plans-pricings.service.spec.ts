import { Test, TestingModule } from '@nestjs/testing';
import { PlansPricingsService } from './plans-pricings.service';

describe('PlansPricingsService', () => {
  let service: PlansPricingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlansPricingsService],
    }).compile();

    service = module.get<PlansPricingsService>(PlansPricingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
