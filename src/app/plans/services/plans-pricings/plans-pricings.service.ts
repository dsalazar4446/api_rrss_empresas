import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plan_pricings } from '../../../../db/entitys/plan_pricings';
import { CreatePlanPricingsDto, UpdatePlanPricingsDto } from '../../dto/plan-pricings.dto';
@Injectable()
export class PlansPricingsService {
    constructor(@InjectRepository(plan_pricings) private readonly repository: Repository<plan_pricings>) {}
    async create(body: CreatePlanPricingsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: ['plan']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['plan']
        });
    }

    async update(id: string, body: Partial<UpdatePlanPricingsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}