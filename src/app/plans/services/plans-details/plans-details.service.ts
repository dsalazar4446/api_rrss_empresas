import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plan_details } from '../../../../db/entitys/plan_details';
import { CreatePlanDetailsDto, UpdatePlanDetailsDto } from '../../dto/plan-details.dto';
@Injectable()
export class PlansDetailsService {
    constructor(@InjectRepository(plan_details) private readonly repository: Repository<plan_details>) {}
    async create(body: CreatePlanDetailsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations: ['plan']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations: ['plan']
        });
    }

    async update(id: string, body: Partial<UpdatePlanDetailsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}