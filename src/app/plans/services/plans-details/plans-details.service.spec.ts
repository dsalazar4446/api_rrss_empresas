import { Test, TestingModule } from '@nestjs/testing';
import { PlansDetailsService } from './plans-details.service';

describe('PlansDetailsService', () => {
  let service: PlansDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlansDetailsService],
    }).compile();

    service = module.get<PlansDetailsService>(PlansDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
