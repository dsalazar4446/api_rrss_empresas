import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { products_services } from "../../../db/entitys/products_services";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateProductServiceDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty({enum: [ "product",  "service"]})
    @IsEnum({
        product: "product", service: "service"
    })
    productService?: products_services;
    @ApiModelProperty()
    @IsString()
    product_service_name?: string;
    @ApiModelProperty()
    @IsString()
    product_service_description?: string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es: "es", en: "en", it: "it", pr: "pr"
    })
    @IsOptional()
    language_type?: string;
}


export class UpdateProductServiceDetailsDto extends CreateProductServiceDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}