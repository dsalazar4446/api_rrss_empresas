import { IsUUID, IsNumber, IsString, IsEnum, IsOptional } from "class-validator";
import { products_services } from "../../../db/entitys/products_services";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateProductServiceViewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
    @ApiModelProperty()
    @IsUUID()
    productService?: products_services;
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsNumber()
    product_share?: number;
    @ApiModelProperty()
    @IsNumber()
    product_like?: number;
    @ApiModelProperty()
    @IsString()
    product_review?: string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es: "es", en: "en", it: "it", pr: "pr"
    })
    @IsOptional()
    language_type?: string;
}

export class UpdateProductServiceViewsDto extends CreateProductServiceViewsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}