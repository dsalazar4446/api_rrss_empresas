import { IsUUID, IsString, IsNumber } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateProductsServicesDto {
    @ApiModelProperty()
    @IsUUID()
    user?: users;
    @ApiModelProperty()
    @IsString()
    product_service_type?: string;
    @ApiModelProperty()
    @IsNumber()
    product_service_total?: number;
    @ApiModelProperty()
    @IsNumber()
    product_service_taxes?: number;
    @ApiModelProperty()
    @IsNumber()
    product_service_retefte?: number;
    @ApiModelProperty()
    @IsNumber()
    product_service_ammount?: number;
    @ApiModelProperty()
    @IsNumber()
    product_service_shipment_local?: number;
    @ApiModelProperty()
    @IsNumber()
    product_service_shipment_outside?: number;
    @ApiModelProperty()
    @IsString()
    product_service_extenal_url?: string;
    @ApiModelProperty()
    @IsNumber()
    product_service_shares?: number;
    @ApiModelProperty()
    @IsString()
    payu_id?: string;

}

export class UpdateProductsServicesDto extends CreateProductsServicesDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}