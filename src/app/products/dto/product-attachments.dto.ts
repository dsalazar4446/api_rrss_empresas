import { IsEnum, IsString, IsUUID, IsOptional } from "class-validator";
import { products_services } from "../../../db/entitys/products_services";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateProductAttachmentsDto {
    @ApiModelProperty()
    @IsUUID()
    productService?: products_services;
    @ApiModelProperty()
    @IsString()
    product_attachment?: string;
    @ApiModelProperty({enum:[ "video",  "image",  "audio"]})
    @IsEnum({
        video: "video", image: "image", audio: "audio"
    })
    @IsOptional()
    product_attachment_type?: string;

}

export class UpdateProductAttachmentsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}