import { Module } from '@nestjs/common';
import { ProductsServicesController } from './controllers/products-services/products-services.controller';
import { ProductsServicesDetailsController } from './controllers/products-services-details/products-services-details.controller';
import { ProductsAttachmentsController } from './controllers/products-attachments/products-attachments.controller';
import { ProductsServicesViewsController } from './controllers/products-services-views/products-services-views.controller';
import { ProductsServicesViewsService } from './services/products-services-views/products-services-views.service';
import { ProductsServicesService } from './services/products-services/products-services.service';
import { ProductsServicesDetailsService } from './services/products-services-details/products-services-details.service';
import { ProductsAttachmentsService } from './services/products-attachments/products-attachments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { products_services } from '../../db/entitys/products_services';
import { product_service_details } from '../../db/entitys/product_service_details';
import { product_attachments } from '../../db/entitys/product_attachments';
import { product_service_views } from '../../db/entitys/product_service_views';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      products_services,
      product_service_details,
      product_attachments,
      product_service_views
    ]),
  ],
  controllers: [ProductsServicesController, ProductsServicesDetailsController, ProductsAttachmentsController, ProductsServicesViewsController],
  providers: [ProductsServicesViewsService, ProductsServicesService, ProductsServicesDetailsService, ProductsAttachmentsService],
  exports: [ProductsServicesService]
})
export class ProductsModule {}
