import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesViewsController } from './products-services-views.controller';

describe('ProductsServicesViews Controller', () => {
  let controller: ProductsServicesViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsServicesViewsController],
    }).compile();

    controller = module.get<ProductsServicesViewsController>(ProductsServicesViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
