import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesDetailsController } from './products-services-details.controller';

describe('ProductsServicesDetails Controller', () => {
  let controller: ProductsServicesDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsServicesDetailsController],
    }).compile();

    controller = module.get<ProductsServicesDetailsController>(ProductsServicesDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
