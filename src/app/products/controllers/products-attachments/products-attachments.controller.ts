import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ProductsAttachmentsService } from '../../services/products-attachments/products-attachments.service';
import { CreateProductAttachmentsDto, UpdateProductAttachmentsDto } from '../../dto/product-attachments.dto';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiImplicitFile, ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('products-attachments')
@ApiUseTags('Products')
@UseInterceptors(ResponseInterceptor)
export class ProductsAttachmentsController {
    constructor(private readonly productsAttachmentsService: ProductsAttachmentsService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiImplicitFile({ name: 'product_attachment', required: false})
    async create(@Body() body: CreateProductAttachmentsDto){
        return await this.productsAttachmentsService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.productsAttachmentsService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.productsAttachmentsService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiImplicitFile({ name: 'product_attachment', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateProductAttachmentsDto>){
        return await this.productsAttachmentsService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.productsAttachmentsService.delete(id);
    }

}