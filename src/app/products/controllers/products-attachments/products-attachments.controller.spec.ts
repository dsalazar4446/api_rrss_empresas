import { Test, TestingModule } from '@nestjs/testing';
import { ProductsAttachmentsController } from './products-attachments.controller';

describe('ProductsAttachments Controller', () => {
  let controller: ProductsAttachmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsAttachmentsController],
    }).compile();

    controller = module.get<ProductsAttachmentsController>(ProductsAttachmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
