import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesController } from './products-services.controller';

describe('ProductsServices Controller', () => {
  let controller: ProductsServicesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsServicesController],
    }).compile();

    controller = module.get<ProductsServicesController>(ProductsServicesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
