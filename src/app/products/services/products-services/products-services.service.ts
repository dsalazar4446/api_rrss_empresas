import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { products_services } from '../../../../db/entitys/products_services';
import { UpdateProductsServicesDto, CreateProductsServicesDto } from '../../dto/products-services.dto';
@Injectable()
export class ProductsServicesService {
    constructor(@InjectRepository(products_services) private readonly repository: Repository<products_services>) {}
    async create(body: CreateProductsServicesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }
    async findById(id: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }

    async update(id: string, body: Partial<UpdateProductsServicesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}