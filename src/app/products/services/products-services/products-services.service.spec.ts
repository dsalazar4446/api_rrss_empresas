import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesService } from './products-services.service';

describe('ProductsServicesService', () => {
  let service: ProductsServicesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductsServicesService],
    }).compile();

    service = module.get<ProductsServicesService>(ProductsServicesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
