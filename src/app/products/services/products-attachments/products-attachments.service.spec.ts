import { Test, TestingModule } from '@nestjs/testing';
import { ProductsAttachmentsService } from './products-attachments.service';

describe('ProductsAttachmentsService', () => {
  let service: ProductsAttachmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductsAttachmentsService],
    }).compile();

    service = module.get<ProductsAttachmentsService>(ProductsAttachmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
