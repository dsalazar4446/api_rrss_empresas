import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesDetailsService } from './products-services-details.service';

describe('ProductsServicesDetailsService', () => {
  let service: ProductsServicesDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductsServicesDetailsService],
    }).compile();

    service = module.get<ProductsServicesDetailsService>(ProductsServicesDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
