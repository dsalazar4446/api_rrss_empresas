import { Test, TestingModule } from '@nestjs/testing';
import { ProductsServicesViewsService } from './products-services-views.service';

describe('ProductsServicesViewsService', () => {
  let service: ProductsServicesViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductsServicesViewsService],
    }).compile();

    service = module.get<ProductsServicesViewsService>(ProductsServicesViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
