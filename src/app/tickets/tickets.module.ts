import { Module } from '@nestjs/common';
import { TicketsService } from './services/tickets/tickets.service';
import { TicketsController } from './controllers/tickets/tickets.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { tickets } from '../../db/entitys/tickets';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      tickets
    ])
  ],
  providers: [TicketsService],
  controllers: [TicketsController]
})
export class TicketsModule {}
