import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";

import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateTicketsDto {
    @ApiModelProperty()
    @IsUUID()
    user?:users;
    @ApiModelProperty()
    @IsString()
    ticket_description?:string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es:"es",en:"en",it:"it",pr:"pr" 
    })
    @IsOptional()
    language_type?:string;

}

export class UpdateTicketsDto extends CreateTicketsDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}
