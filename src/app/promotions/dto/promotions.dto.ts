import { IsUUID, IsString, IsEnum, IsNumber } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreatePromotionsDto {
    @ApiModelProperty()
    @IsString()
    promotion_code?: string;
    @ApiModelProperty({enum:[ "plan",  "contract"]})
    @IsEnum({
        plan: "plan", contract: "contract"
    })
    promotion_type: string;
    @ApiModelProperty()
    @IsNumber()
    promotion_percentage_discount?: number;
    @ApiModelProperty()
    @IsNumber()
    promotion_available_cant?: number;
    @ApiModelProperty()
    @IsNumber()
    promotion_status?: number;

}

export class UpdatePromotionsDto extends CreatePromotionsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}