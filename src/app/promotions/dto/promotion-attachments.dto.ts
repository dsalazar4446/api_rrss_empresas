import { IsUUID, IsString, IsEnum, IsOptional } from "class-validator";
import { promotions } from "../../../db/entitys/promotions";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreatePromotionAttachments {
    @ApiModelProperty()
    @IsUUID()
    promotion?:promotions;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    promotion_attachment?:string;
    @ApiModelProperty({enum:["1","2","3" ]})
    @IsEnum({
        1:"1",2:"2",3:"3" 
    })
    promotion_attachment_type?:string;
        
}

export class UpdatePromotionAttachments extends CreatePromotionAttachments{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}