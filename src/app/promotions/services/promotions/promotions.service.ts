import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { promotions } from '../../../../db/entitys/promotions';
import { CreatePromotionsDto, UpdatePromotionsDto } from '../../dto/promotions.dto';
@Injectable()
export class PromotionsService {
    constructor(@InjectRepository(promotions) private readonly repository: Repository<promotions>) {}
    async create(body: CreatePromotionsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'billingTransactionss',
                'promotionAttachmentss',
                'userss',
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'billingTransactionss',
                'promotionAttachmentss',
                'userss',
            ]
        });
    }

    async update(id: string, body: Partial<UpdatePromotionsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}