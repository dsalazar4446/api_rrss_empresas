import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { promotion_attachments } from '../../../../db/entitys/promotion_attachments';
import { CreatePromotionAttachments, UpdatePromotionAttachments } from '../../dto/promotion-attachments.dto';
@Injectable()
export class PromotionsAttacmentsService {
    constructor(@InjectRepository(promotion_attachments) private readonly repository: Repository<promotion_attachments>) {}
    async create(body: CreatePromotionAttachments) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: ['promotion']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['promotion']
        });
    }

    async update(id: string, body: Partial<UpdatePromotionAttachments>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}