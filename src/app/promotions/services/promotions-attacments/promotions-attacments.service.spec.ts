import { Test, TestingModule } from '@nestjs/testing';
import { PromotionsAttacmentsService } from './promotions-attacments.service';

describe('PromotionsAttacmentsService', () => {
  let service: PromotionsAttacmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PromotionsAttacmentsService],
    }).compile();

    service = module.get<PromotionsAttacmentsService>(PromotionsAttacmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
