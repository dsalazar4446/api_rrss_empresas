import { Module } from '@nestjs/common';
import { PromotionsController } from './controllers/promotions/promotions.controller';
import { PromotionsAttacmentsController } from './controllers/promotions-attacments/promotions-attacments.controller';
import { PromotionsAttacmentsService } from './services/promotions-attacments/promotions-attacments.service';
import { PromotionsService } from './services/promotions/promotions.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { promotions } from '../../db/entitys/promotions';
import { promotion_attachments } from '../../db/entitys/promotion_attachments';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      promotions,
      promotion_attachments
    ])
  ],
  controllers: [PromotionsController, PromotionsAttacmentsController],
  providers: [PromotionsAttacmentsService, PromotionsService]
})
export class PromotionsModule {}
