import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreatePromotionsDto, UpdatePromotionsDto } from '../../dto/promotions.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('promotions')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Promotions')
export class PromotionsController {
    constructor(private readonly nameService: PromotionsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreatePromotionsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdatePromotionsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}