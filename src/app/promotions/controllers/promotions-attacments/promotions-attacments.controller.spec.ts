import { Test, TestingModule } from '@nestjs/testing';
import { PromotionsAttacmentsController } from './promotions-attacments.controller';

describe('PromotionsAttacments Controller', () => {
  let controller: PromotionsAttacmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PromotionsAttacmentsController],
    }).compile();

    controller = module.get<PromotionsAttacmentsController>(PromotionsAttacmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
