import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { PromotionsAttacmentsService } from '../../services/promotions-attacments/promotions-attacments.service';
import { CreatePromotionAttachments, UpdatePromotionAttachments } from '../../dto/promotion-attachments.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiUseTags, ApiImplicitFile, ApiConsumes, ApiImplicitHeader } from '@nestjs/swagger';


@Controller('promotions-attacments')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Promotions')
export class PromotionsAttacmentsController {
    constructor(private readonly nameService: PromotionsAttacmentsService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'promotion_attachment', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreatePromotionAttachments){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @ApiImplicitFile({ name: 'promotion_attachment', required: false})
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body()body:  Partial<UpdatePromotionAttachments>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}