import { IsUUID, IsString, IsNumber, IsEnum, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateRatingUsersDto {
    @ApiModelProperty()
    @IsUUID()
    rater?: users;
    @ApiModelProperty()
    @IsUUID()
    rated?: users;
    @ApiModelProperty()
    @IsNumber()
    rating?: number;
    @ApiModelProperty()
    @IsString()
    rating_comments?: string;
    @ApiModelPropertyOptional()
    @IsEnum({
        es: "es", en: "en", it: "it", pr: "pr"
    })
    @IsOptional()
    language_type?: string;
}

export class UpdateRatingUsersDto extends CreateRatingUsersDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}