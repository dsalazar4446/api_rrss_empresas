import { Test, TestingModule } from '@nestjs/testing';
import { UserRatingsController } from './user-ratings.controller';

describe('UserRatings Controller', () => {
  let controller: UserRatingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserRatingsController],
    }).compile();

    controller = module.get<UserRatingsController>(UserRatingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
