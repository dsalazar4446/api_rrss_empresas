import { Module } from '@nestjs/common';
import { UserRatingsController } from './controllers/user-ratings/user-ratings.controller';
import { UserRatingsService } from './services/user-ratings/user-ratings.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { rating_users } from '../../db/entitys/rating_users';

@Module({
  imports: [
    TypeOrmModule.forFeature([rating_users])
  ],
  controllers: [UserRatingsController],
  providers: [UserRatingsService]
})
export class RatingsModule {}
