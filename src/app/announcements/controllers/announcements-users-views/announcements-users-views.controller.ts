import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { AnnouncementsUsersViewsService } from '../../services/announcements-users-views/announcements-users-views.service';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateAnnouncementsUsersViewsDto, UpdateAnnouncementsUsersViewsDto } from '../../dto/announcements-users-views.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('announcements-users-views')
@ApiUseTags('Announcements')
@UseInterceptors(ResponseInterceptor)
export class AnnouncementsUsersViewsController {
    constructor(private readonly nameService: AnnouncementsUsersViewsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAnnouncementsUsersViewsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAnnouncementsUsersViewsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}