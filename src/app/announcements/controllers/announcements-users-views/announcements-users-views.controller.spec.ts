import { Test, TestingModule } from '@nestjs/testing';
import { AnnouncementsUsersViewsController } from './announcements-users-views.controller';

describe('AnnouncementsUsersViews Controller', () => {
  let controller: AnnouncementsUsersViewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnnouncementsUsersViewsController],
    }).compile();

    controller = module.get<AnnouncementsUsersViewsController>(AnnouncementsUsersViewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
