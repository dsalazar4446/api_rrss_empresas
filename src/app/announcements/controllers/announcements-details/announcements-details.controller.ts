import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { AnnouncementsDetailsService } from '../../services/announcements-details/announcements-details.service';
import { CreateAnnouncementDetailsDto, UpdateAnnouncementDetailsDto } from '../../dto/announcements-details.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('announcements-details')
@ApiUseTags('Announcements')
@UseInterceptors(ResponseInterceptor)
export class AnnouncementsDetailsController {
    constructor(private readonly nameService: AnnouncementsDetailsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateAnnouncementDetailsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAnnouncementDetailsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}