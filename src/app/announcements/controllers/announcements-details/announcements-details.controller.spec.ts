import { Test, TestingModule } from '@nestjs/testing';
import { AnnouncementsDetailsController } from './announcements-details.controller';

describe('AnnouncementsDetails Controller', () => {
  let controller: AnnouncementsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnnouncementsDetailsController],
    }).compile();

    controller = module.get<AnnouncementsDetailsController>(AnnouncementsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
