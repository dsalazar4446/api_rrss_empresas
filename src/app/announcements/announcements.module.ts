import { Module } from '@nestjs/common';
import { AnnouncementsController } from './controllers/announcements/announcements.controller';
import { AnnouncementsDetailsController } from './controllers/announcements-details/announcements-details.controller';
import { AnnouncementsUsersViewsController } from './controllers/announcements-users-views/announcements-users-views.controller';
import { AnnouncementsService } from './services/announcements/announcements.service';
import { AnnouncementsDetailsService } from './services/announcements-details/announcements-details.service';
import { AnnouncementsUsersViewsService } from './services/announcements-users-views/announcements-users-views.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { announcements } from '../../db/entitys/announcements';
import { announcement_details } from '../../db/entitys/announcement_details';
import { announcements_users_views } from '../../db/entitys/announcements_users_views';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      announcements,
      announcement_details,
      announcements_users_views
    ]),
  ],
  controllers: [AnnouncementsController, AnnouncementsDetailsController, AnnouncementsUsersViewsController],
  providers: [AnnouncementsService, AnnouncementsDetailsService, AnnouncementsUsersViewsService]
})
export class AnnouncementsModule {}
