import { IsUUID } from "class-validator";
import { announcements } from "../../../db/entitys/announcements";
import { users } from "../../../db/entitys/users";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateAnnouncementsUsersViewsDto {
    @ApiModelProperty()
    @IsUUID()
    announcement?:announcements;
    @ApiModelProperty()
    @IsUUID()
    user?:users;
}

export class UpdateAnnouncementsUsersViewsDto extends CreateAnnouncementsUsersViewsDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}