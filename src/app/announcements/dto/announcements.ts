import { IsUUID, IsEnum, IsString } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateAnnouncementsDto {
    @ApiModelProperty({enum:["all",  "company",  "user",  "advisor"]})
    @IsEnum({
        all: "all", company: "company", user: "user", advisor: "advisor"
    })
    announcement_target?: string;
    @ApiModelProperty()
    @IsString()
    announcement_image?: string;

}
export class UpdateAnnouncementsDto extends CreateAnnouncementsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}