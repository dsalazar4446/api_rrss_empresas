import { IsUUID, IsString, IsOptional } from "class-validator";
import { announcements } from "../../../db/entitys/announcements";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateAnnouncementDetailsDto {
    @ApiModelProperty()
    @IsString()
    announcement?:announcements;
    @ApiModelProperty()
    @IsString()
    announcement_title?:string;
    @ApiModelProperty()
    @IsString()
    announcement_description?:string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?:string;

}

export class UpdateAnnouncementDetailsDto extends CreateAnnouncementDetailsDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}