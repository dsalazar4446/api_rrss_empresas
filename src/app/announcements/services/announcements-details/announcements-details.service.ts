import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { announcement_details } from '../../../../db/entitys/announcement_details';
import { UpdateAnnouncementDetailsDto, CreateAnnouncementDetailsDto } from '../../dto/announcements-details.dto';
@Injectable()
export class AnnouncementsDetailsService {
    constructor(@InjectRepository(announcement_details) private readonly repository: Repository<announcement_details>) {}
    async create(body: CreateAnnouncementDetailsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            where:{
                language_type: languageType,
            },
            relations: ['announcement']
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
                language_type: languageType,
            },
            relations: ['announcement']
        });
    }

    async update(id: string, body: Partial<UpdateAnnouncementDetailsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}