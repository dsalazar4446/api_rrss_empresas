import { Test, TestingModule } from '@nestjs/testing';
import { AnnouncementsDetailsService } from './announcements-details.service';

describe('AnnouncementsDetailsService', () => {
  let service: AnnouncementsDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnnouncementsDetailsService],
    }).compile();

    service = module.get<AnnouncementsDetailsService>(AnnouncementsDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
