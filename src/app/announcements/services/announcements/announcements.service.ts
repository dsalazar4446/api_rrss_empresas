import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { announcements } from '../../../../db/entitys/announcements';
import { CreateAnnouncementsDto, UpdateAnnouncementsDto } from '../../dto/announcements';
@Injectable()
export class AnnouncementsService {
    constructor(@InjectRepository(announcements) private readonly repository: Repository<announcements>) {}
    async create(body: CreateAnnouncementsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations:[
                'announcementDetailss',
                'announcementsUsersViewss'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:[
                'announcementDetailss',
                'announcementsUsersViewss'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateAnnouncementsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}