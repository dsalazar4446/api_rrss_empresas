import { Test, TestingModule } from '@nestjs/testing';
import { AnnouncementsUsersViewsService } from './announcements-users-views.service';

describe('AnnouncementsUsersViewsService', () => {
  let service: AnnouncementsUsersViewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnnouncementsUsersViewsService],
    }).compile();

    service = module.get<AnnouncementsUsersViewsService>(AnnouncementsUsersViewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
