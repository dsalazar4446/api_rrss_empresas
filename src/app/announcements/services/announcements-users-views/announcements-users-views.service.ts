import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { announcements_users_views } from '../../../../db/entitys/announcements_users_views';
import { CreateAnnouncementsUsersViewsDto, UpdateAnnouncementsUsersViewsDto } from '../../dto/announcements-users-views.dto';
@Injectable()
export class AnnouncementsUsersViewsService {
    constructor(@InjectRepository(announcements_users_views) private readonly repository: Repository<announcements_users_views>) {}
    async create(body: CreateAnnouncementsUsersViewsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'announcement',
                'user'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'announcement',
                'user'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateAnnouncementsUsersViewsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}