import { IsUUID, IsString, IsOptional } from "class-validator";
import { awards } from "../../../db/entitys/awards";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateAwardDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    award?:awards;
    @ApiModelProperty()
    @IsString()
    award_title?:string;
    @ApiModelProperty()
    @IsString()
    award_description?:string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?:string;
        
}

export class UpdateAwardDetailsDto extends CreateAwardDetailsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}