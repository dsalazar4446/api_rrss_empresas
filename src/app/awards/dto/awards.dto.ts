import { IsString, IsOptional } from "class-validator";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateAwardsDto {
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    award_icon?:string;           
}

export class UpdateAwardsDto extends CreateAwardsDto{
    @ApiModelProperty()
    @IsString()
    id?:string;        
}