import { Test, TestingModule } from '@nestjs/testing';
import { AwardsDetailsService } from './awards-details.service';

describe('AwardsDetailsService', () => {
  let service: AwardsDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AwardsDetailsService],
    }).compile();

    service = module.get<AwardsDetailsService>(AwardsDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
