import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { awards } from '../../../../db/entitys/awards';
import { CreateAwardsDto, UpdateAwardsDto } from '../../dto/awards.dto';
@Injectable()
export class AwardsService {
    constructor(@InjectRepository(awards) private readonly repository: Repository<awards>) {}
    async create(body: CreateAwardsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }

    async update(id: string, body: Partial<UpdateAwardsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}