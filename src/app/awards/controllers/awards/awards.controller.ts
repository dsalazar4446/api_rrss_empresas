import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { AwardsService } from '../../services/awards/awards.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateAwardsDto, UpdateAwardsDto } from '../../dto/awards.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';
import { UrlArrayInterceptor } from '../../../shared/interceptors/url-array.interceptor';
import { UrlInterceptor } from '../../../shared/interceptors/url.interceptor';

@Controller('awards')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Awards')
export class AwardsController {
    constructor(private readonly nameService: AwardsService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'award_icon', required: false})
    async create(@Body() body: CreateAwardsDto){
        return await this.nameService.create(body);
    }

    @Get()
    @UseInterceptors(new UrlArrayInterceptor('category_icon','categories'))
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    @UseInterceptors(new UrlInterceptor('category_icon','categories'))
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'award_icon', required: false})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateAwardsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}