import { Test, TestingModule } from '@nestjs/testing';
import { AwardsDetailsController } from './awards-details.controller';

describe('AwardsDetails Controller', () => {
  let controller: AwardsDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AwardsDetailsController],
    }).compile();

    controller = module.get<AwardsDetailsController>(AwardsDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
