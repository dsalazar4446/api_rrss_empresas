import { Module } from '@nestjs/common';
import { AwardsController } from './controllers/awards/awards.controller';
import { AwardsDetailsController } from './controllers/awards-details/awards-details.controller';
import { AwardsDetailsService } from './services/awards-details/awards-details.service';
import { AwardsService } from './services/awards/awards.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { awards } from '../../db/entitys/awards';
import { award_details } from '../../db/entitys/award_details';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      awards,
      award_details
    ])
  ],
  controllers: [AwardsController, AwardsDetailsController],
  providers: [AwardsDetailsService, AwardsService]
})
export class AwardsModule {}
