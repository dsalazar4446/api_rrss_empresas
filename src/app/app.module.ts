import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { UsersModule } from './users/users.module';
import { LocationsModule } from './locations/locations.module';
import { ProductsModule } from './products/products.module';
import { TicketsModule } from './tickets/tickets.module';
import { SharedModule } from './shared/shared.module';
import { MessagesModule } from './messages/messages.module';
import { SectorsModule } from './sectors/sectors.module';
import { NotificationsModule } from './notifications/notifications.module';
import { PaymentMethodsModule } from './payment-methods/payment-methods.module';
import { RatingsModule } from './ratings/ratings.module';
import { AwardsModule } from './awards/awards.module';
import { TransactionsModule } from './transactions/transactions.module';
import { PlansModule } from './plans/plans.module';
import { AchievementsModule } from './achievements/achievements.module';
import { AdvisoriesModule } from './advisories/advisories.module';
import { PromotionsModule } from './promotions/promotions.module';
import { EventsTalksModule } from './events-talks/events-talks.module';
import { TagsModule } from './tags/tags.module';
import { ConfigurationsModule } from './configurations/configurations.module';
import { CategoriesModule } from './categories/categories.module';
import { AnnouncementsModule } from './announcements/announcements.module';
import { NewsModule } from './news/news.module';
import { UserCompaniesModule } from './user-companies/user-companies.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UsersModule,
    LocationsModule,
    ProductsModule,
    TicketsModule,
    SharedModule,
    MessagesModule,
    SectorsModule,
    NotificationsModule,
    PaymentMethodsModule,
    RatingsModule,
    AwardsModule,
    TransactionsModule,
    PlansModule,
    AchievementsModule,
    AdvisoriesModule,
    PromotionsModule,
    EventsTalksModule,
    TagsModule,
    ConfigurationsModule,
    CategoriesModule,
    AnnouncementsModule,
    NewsModule,
    UserCompaniesModule,
  ],
})
export class AppModule {
  constructor(private readonly connection: Connection){}
}
