import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { BilllinTransactionsPaymentsStatusesService } from '../../services/billlin-transactions-payments-statuses/billlin-transactions-payments-statuses.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateBillingTransactionPaymentStatusesDto, UpdateBillingTransactionPaymentStatusesDto } from '../../dto/billing-transaction-payment-statuses.dto';
import { ApiUseTags, ApiImplicitHeader } from '@nestjs/swagger';

@Controller('billlin-transactions-payments-statuses')
@ApiUseTags('Transactions')
@UseInterceptors(ResponseInterceptor)
export class BilllinTransactionsPaymentsStatusesController {
    constructor(private readonly nameService: BilllinTransactionsPaymentsStatusesService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateBillingTransactionPaymentStatusesDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateBillingTransactionPaymentStatusesDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }
}