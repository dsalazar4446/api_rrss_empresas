import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsPaymentsStatusesController } from './billlin-transactions-payments-statuses.controller';

describe('BilllinTransactionsPaymentsStatuses Controller', () => {
  let controller: BilllinTransactionsPaymentsStatusesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BilllinTransactionsPaymentsStatusesController],
    }).compile();

    controller = module.get<BilllinTransactionsPaymentsStatusesController>(BilllinTransactionsPaymentsStatusesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
