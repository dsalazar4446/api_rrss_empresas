import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { BilllinTransactionsPaymentsService } from '../../services/billlin-transactions-payments/billlin-transactions-payments.service';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CreateBillingTransactionPaymentsDto, UpdateBillingTransactionPaymentsDto } from '../../dto/billing-transaction-payments.dto';
import { ApiUseTags } from '@nestjs/swagger/dist/decorators/api-use-tags.decorator';
import { ApiImplicitHeader } from '@nestjs/swagger';

@Controller('billlin-transactions-payments')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Transactions')
export class BilllinTransactionsPaymentsController {
    constructor(private readonly nameService: BilllinTransactionsPaymentsService){}

    @Post()
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async create(@Body() body: CreateBillingTransactionPaymentsDto){
        return await this.nameService.create(body);
    }

    @Get()
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }

    @Get(':id')
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateBillingTransactionPaymentsDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}