import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsPaymentsController } from './billlin-transactions-payments.controller';

describe('BilllinTransactionsPayments Controller', () => {
  let controller: BilllinTransactionsPaymentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BilllinTransactionsPaymentsController],
    }).compile();

    controller = module.get<BilllinTransactionsPaymentsController>(BilllinTransactionsPaymentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
