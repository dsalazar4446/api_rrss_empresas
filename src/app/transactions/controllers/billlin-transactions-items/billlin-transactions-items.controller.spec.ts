import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsItemsController } from './billlin-transactions-items.controller';

describe('BilllinTransactionsItems Controller', () => {
  let controller: BilllinTransactionsItemsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BilllinTransactionsItemsController],
    }).compile();

    controller = module.get<BilllinTransactionsItemsController>(BilllinTransactionsItemsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
