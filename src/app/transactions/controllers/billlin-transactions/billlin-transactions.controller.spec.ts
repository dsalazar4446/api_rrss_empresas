import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsController } from './billlin-transactions.controller';

describe('BilllinTransactions Controller', () => {
  let controller: BilllinTransactionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BilllinTransactionsController],
    }).compile();

    controller = module.get<BilllinTransactionsController>(BilllinTransactionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
