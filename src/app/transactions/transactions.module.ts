import { Module } from '@nestjs/common';
import { BilllinTransactionsController } from './controllers/billlin-transactions/billlin-transactions.controller';
import { BilllinTransactionsItemsController } from './controllers/billlin-transactions-items/billlin-transactions-items.controller';
import { BilllinTransactionsPaymentsController } from './controllers/billlin-transactions-payments/billlin-transactions-payments.controller';
import { BilllinTransactionsPaymentsStatusesController } from './controllers/billlin-transactions-payments-statuses/billlin-transactions-payments-statuses.controller';
import { BilllinTransactionsService } from './services/billlin-transactions/billlin-transactions.service';
import { BilllinTransactionsItemsService } from './services/billlin-transactions-items/billlin-transactions-items.service';
import { BilllinTransactionsPaymentsService } from './services/billlin-transactions-payments/billlin-transactions-payments.service';
import { BilllinTransactionsPaymentsStatusesService } from './services/billlin-transactions-payments-statuses/billlin-transactions-payments-statuses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { billing_transactions } from '../../db/entitys/billing_transactions';
import { billing_transaction_items } from '../../db/entitys/billing_transaction_items';
import { billing_transaction_payments } from '../../db/entitys/billing_transaction_payments';
import { billing_transaction_payment_statuses } from '../../db/entitys/billing_transaction_payment_statuses';
import { SharedModule } from '../shared/shared.module';
import { ProductsModule } from '../products/products.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      billing_transactions,
      billing_transaction_items,
      billing_transaction_payments,
      billing_transaction_payment_statuses
    ]),
    SharedModule,
    ProductsModule
  ],
  controllers: [BilllinTransactionsController, BilllinTransactionsItemsController, BilllinTransactionsPaymentsController, BilllinTransactionsPaymentsStatusesController],
  providers: [BilllinTransactionsService, BilllinTransactionsItemsService, BilllinTransactionsPaymentsService, BilllinTransactionsPaymentsStatusesService]
})
export class TransactionsModule {}
