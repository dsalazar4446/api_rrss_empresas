import { IsUUID, IsEnum } from "class-validator";
import { billing_transaction_payments } from "../../../db/entitys/billing_transaction_payments";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateBillingTransactionPaymentStatusesDto {
    @ApiModelProperty()
    @IsUUID()
    billingTransactionPayment?: billing_transaction_payments;
    @ApiModelProperty({enum: ["pending", "delayed", "success", "rejected", "underpaid", "overpaid", "override"]})
    @IsEnum({
        pending: "pending", delayed: "delayed", success: "success", rejected: "rejected", underpaid: "underpaid", overpaid: "overpaid", override: "override"
    })
    billing_transaction_payment_status?: string;
}

export class UpdateBillingTransactionPaymentStatusesDto extends CreateBillingTransactionPaymentStatusesDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}