import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import { IsUUID, IsNumber } from "class-validator";
import { billing_transactions } from "../../../db/entitys/billing_transactions";
import { products_services } from "../../../db/entitys/products_services";
import { ApiModelProperty } from "@nestjs/swagger";


export class CreateBillingTransactionItemsDto {
    @ApiModelProperty()
    @IsUUID()
    billingTransaction?:billing_transactions;
    @ApiModelProperty()
    @IsUUID()
    productService?:products_services;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_item_cant?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_item_subtotal?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_item_taxes?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_item_retefte?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_item_total?:number;
}

export class UpdateBillingTransactionItemsDto extends CreateBillingTransactionItemsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}