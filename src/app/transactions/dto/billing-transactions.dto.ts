import { IsUUID, IsString, IsEnum, IsNumber, IsOptional } from "class-validator";
import { users } from "../../../db/entitys/users";
import { promotions } from "../../../db/entitys/promotions";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateBillingTransactionsDto {
    
    @ApiModelProperty()
    @IsUUID()
    user?:users;
    @ApiModelProperty()
    @IsUUID()
    promotion?:promotions;
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    billing_transaction_code?:string;
    @ApiModelProperty({enum:["plan","event","purchase","withdraw"]})
    @IsEnum({plan:"plan",event:"event",purchase:"purchase",withdraw:"withdraw"})
    billing_transaction_type?:string;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_subtotal?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_disccount?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_taxes?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_retefte?:number;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_total?:number;
    @ApiModelProperty({enum: ["pending","success","rejected"]})
    @IsEnum({pending:"pending",success:"success",rejected:"rejected"})
    billing_transaction_status?:string;
    @ApiModelProperty()
    @IsString()
    billing_transaction_observations?:string;
        
}

export class UpdateBillingTransactionsDto extends CreateBillingTransactionsDto {
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}