import { IsUUID, IsString, IsNumber } from "class-validator";
import { billing_transactions } from "../../../db/entitys/billing_transactions";
import { payment_methods } from "../../../db/entitys/payment_methods";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateBillingTransactionPaymentsDto {
    @ApiModelProperty()
    @IsUUID()
    billingTransaction?: billing_transactions;
    @ApiModelProperty()
    @IsUUID()
    paymentMethod?: payment_methods;
    @ApiModelProperty()
    @IsString()
    billing_transaction_payment_token?: string;
    @ApiModelProperty()
    @IsNumber()
    billing_transaction_payment_ammount?: number;

}

export class UpdateBillingTransactionPaymentsDto extends CreateBillingTransactionPaymentsDto {
    @ApiModelProperty()
    @IsUUID()
    id?: string;
}
