import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsService } from './billlin-transactions.service';

describe('BilllinTransactionsService', () => {
  let service: BilllinTransactionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BilllinTransactionsService],
    }).compile();

    service = module.get<BilllinTransactionsService>(BilllinTransactionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
