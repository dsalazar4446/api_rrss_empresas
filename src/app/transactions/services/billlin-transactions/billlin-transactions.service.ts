import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { billing_transactions } from '../../../../db/entitys/billing_transactions';
import { CreateBillingTransactionsDto, UpdateBillingTransactionsDto } from '../../dto/billing-transactions.dto';
import * as payU from 'payu-latam'
import { PayuService } from '../../../shared/services/payu/payu.service';
@Injectable()
export class BilllinTransactionsService {
    payApi:any
    constructor(
        @InjectRepository(billing_transactions) private readonly repository: Repository<billing_transactions>,
        private readonly payu: PayuService
    ) {
        this.payApi =new payU({
            apiKey: 'LGTM6iT1B2Q8FIt786yX546OdZ',
            apiLogin: 'xYdxWGW5qawpm6T'
        })       
    }
    async create(body: CreateBillingTransactionsDto) {
        return await this.repository.save(body)
    }
    async createIfNotPending(){
        let transactions: CreateBillingTransactionsDto
        const bill = await this.repository.findOne({where:{billing_transaction_type:'pending'}})
    }
    async list(languageType: string){
        return await this.repository.find({relations: ['billingTransactionItemss', 'billingTransactionPaymentss']});
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: ['billingTransactionItemss', 'billingTransactionPaymentss']
        });
    }
    async ping(){
        this.payu.setApiKey('LGTM6iT1B2Q8FIt786yX546OdZ')
        this.payu.setApiLogin('xYdxWGW5qawpm6T')
        return this.payu.ping()
    }



    async update(id: string, body: Partial<UpdateBillingTransactionsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }
    

}