import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsPaymentsService } from './billlin-transactions-payments.service';

describe('BilllinTransactionsPaymentsService', () => {
  let service: BilllinTransactionsPaymentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BilllinTransactionsPaymentsService],
    }).compile();

    service = module.get<BilllinTransactionsPaymentsService>(BilllinTransactionsPaymentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
