import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { billing_transaction_payments } from '../../../../db/entitys/billing_transaction_payments';
import { CreateBillingTransactionPaymentsDto, UpdateBillingTransactionPaymentsDto } from '../../dto/billing-transaction-payments.dto';
@Injectable()
export class BilllinTransactionsPaymentsService {
    constructor(@InjectRepository(billing_transaction_payments) private readonly repository: Repository<billing_transaction_payments>) {}
    async create(body: CreateBillingTransactionPaymentsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({
            relations: [
                'billingTransaction',
                'paymentMethod'
            ]
        });
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations: [
                'billingTransaction',
                'paymentMethod'
            ]
        });
    }

    async update(id: string, body: Partial<UpdateBillingTransactionPaymentsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}