import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { billing_transaction_payment_statuses } from '../../../../db/entitys/billing_transaction_payment_statuses';
import { CreateBillingTransactionPaymentStatusesDto, UpdateBillingTransactionPaymentStatusesDto } from '../../dto/billing-transaction-payment-statuses.dto';
@Injectable()
export class BilllinTransactionsPaymentsStatusesService {
    constructor(@InjectRepository(billing_transaction_payment_statuses) private readonly repository: Repository<billing_transaction_payment_statuses>) {}
    async create(body: CreateBillingTransactionPaymentStatusesDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find();
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            }
        });
    }

    async update(id: string, body: Partial<UpdateBillingTransactionPaymentStatusesDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}