import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsPaymentsStatusesService } from './billlin-transactions-payments-statuses.service';

describe('BilllinTransactionsPaymentsStatusesService', () => {
  let service: BilllinTransactionsPaymentsStatusesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BilllinTransactionsPaymentsStatusesService],
    }).compile();

    service = module.get<BilllinTransactionsPaymentsStatusesService>(BilllinTransactionsPaymentsStatusesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
