import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { billing_transaction_items } from '../../../../db/entitys/billing_transaction_items';
import { CreateBillingTransactionItemsDto, UpdateBillingTransactionItemsDto } from '../../dto/billing-transaction-items.dto';
import { ProductsServicesService } from '../../../products/services/products-services/products-services.service';
import { billing_transactions } from '../../../../db/entitys/billing_transactions';
@Injectable()
export class BilllinTransactionsItemsService {
    constructor(
        @InjectRepository(billing_transaction_items) private readonly repository: Repository<billing_transaction_items>,
        @InjectRepository(billing_transactions) private readonly billingTransactions: Repository<billing_transactions>,
        private readonly products: ProductsServicesService
    ) {}
    async create(body: CreateBillingTransactionItemsDto) {
        return await this.repository.save(body)
    }

    async list(languageType: string){
        return await this.repository.find({relations:['billingTransaction', 'productService']});
    }

    async detail(id: string, languageType: string){
        return await this.repository.findOne({
            where:{
                id,
            },
            relations:['billingTransaction', 'productService']
        });
    }

    async update(id: string, body: Partial<UpdateBillingTransactionItemsDto>){
        const result = await this.repository.findOne(id);
        await this.repository.merge(result,body);
        return await this.repository.save(result);
    }

    async delete(id: string){
        const result = await this.repository.findOne({where:{id}})
        if(!result){
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if(remove){
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }
    async addProduct(idProduct: string, IdBill: string, cant: number){
       const producto =  await this.products.findById(idProduct);
       if(!producto){
           throw new HttpException('product not found',HttpStatus.NOT_FOUND)
       }

       const bill = await this.billingTransactions.findOne({where:{id:IdBill}})
       if(!bill){
            throw new HttpException('bill not found',HttpStatus.NOT_FOUND) 
       }

       let item: CreateBillingTransactionItemsDto = {
        billingTransaction: bill,
        productService: producto,
        billing_transaction_item_cant: cant,
        billing_transaction_item_subtotal: producto.product_service_total * cant,
        billing_transaction_item_retefte: 0,
        billing_transaction_item_taxes: producto.product_service_taxes,
        billing_transaction_item_total: producto.product_service_total * cant + (producto.product_service_total * cant * producto.product_service_taxes)
   }

       if(producto.product_service_type == 'product'){
            item = {
                billingTransaction: bill,
                productService: producto,
                billing_transaction_item_cant: cant,
                billing_transaction_item_subtotal: producto.product_service_total * cant,
                billing_transaction_item_retefte: 0,
                billing_transaction_item_taxes: producto.product_service_taxes,
                billing_transaction_item_total: producto.product_service_total * cant + (producto.product_service_total * cant * producto.product_service_taxes)
            }
       }
       if(producto.product_service_type == 'service'){
           item = {                
            billingTransaction: bill,
            productService: producto,
            billing_transaction_item_cant: cant,
            billing_transaction_item_subtotal: producto.product_service_total,
            billing_transaction_item_retefte: 0,
            billing_transaction_item_taxes: producto.product_service_taxes,
            billing_transaction_item_total: producto.product_service_total + (producto.product_service_total * producto.product_service_taxes)
   
           }
       }
       const result = this.repository.save(item)
       return result
       
    }
}