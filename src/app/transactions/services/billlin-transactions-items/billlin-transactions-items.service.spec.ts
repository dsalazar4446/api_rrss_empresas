import { Test, TestingModule } from '@nestjs/testing';
import { BilllinTransactionsItemsService } from './billlin-transactions-items.service';

describe('BilllinTransactionsItemsService', () => {
  let service: BilllinTransactionsItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BilllinTransactionsItemsService],
    }).compile();

    service = module.get<BilllinTransactionsItemsService>(BilllinTransactionsItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
