import { IsString, IsUUID, IsOptional } from "class-validator";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateCategoriesDto {
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    category_icon?:string;
}

export class UpdateCategoriesDto extends CreateCategoriesDto{
    @ApiModelProperty()
    @IsUUID()
    id?:string;
}