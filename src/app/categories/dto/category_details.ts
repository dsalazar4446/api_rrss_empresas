import { IsUUID, IsString, IsOptional } from "class-validator";
import { categories } from "../../../db/entitys/categories";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export class CreateCategoryDetails {
    @ApiModelProperty()
    @IsUUID()
    category?:categories | null;
    @ApiModelProperty()
    @IsString()
    category_detail?:string;
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    language_type?:string;
        
        
}

export class UpdatecategoryDetails extends CreateCategoryDetails{
    @ApiModelProperty()
    @IsUUID()
    id?:string;    
        
}