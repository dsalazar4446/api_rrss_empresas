import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, createQueryBuilder } from 'typeorm';
import { categories } from '../../../../db/entitys/categories';
import { CreateCategoriesDto, UpdateCategoriesDto } from '../../dto/categories';
@Injectable()
export class CategoriesService {
    constructor(@InjectRepository(categories) private readonly repository: Repository<categories>) { }
    async create(body: CreateCategoriesDto) {
        console.log(body)
        const category: CreateCategoriesDto = {
            category_icon: body.category_icon
        }
        return this.repository.save(category).then(resp => { return resp }).catch(err => { console.log('err', err) })
        // return await this.repository.save(body)
    }

    async list(languageType: string) {

        return createQueryBuilder('categories')
            .leftJoinAndSelect('categories.categoryDetailss', 'category_details')
            .leftJoinAndSelect('categories.eventsTalkss', 'events_talks')
            .leftJoinAndSelect('categories.newss', 'news')
            .leftJoinAndSelect('categories.userPostss', 'user_posts')
            .where('category_details.language_type = :language_type' ,{language_type: languageType}).getMany()
    }

    async detail(id: string, languageType: string) {
        return createQueryBuilder('categories')
            .leftJoinAndSelect('categories.categoryDetailss', 'category_details')
            .leftJoinAndSelect('categories.eventsTalkss', 'events_talks')
            .leftJoinAndSelect('categories.newss', 'news')
            .leftJoinAndSelect('categories.userPostss', 'user_posts')
            .where('category_details.language_type = :language_type' ,{language_type: languageType})
            .andWhere('categories.id = : id', {id})
            .getMany()

    }

    async update(id: string, body: Partial<UpdateCategoriesDto>) {
        const result = await this.repository.findOne(id);
        await this.repository.merge(result, body);
        return await this.repository.save(result);
    }

    async delete(id: string) {
        const result = await this.repository.findOne({ where: { id } })
        if (!result) {
            throw new HttpException('Recurso a remover no existe', HttpStatus.NOT_FOUND)
        }
        const remove = await this.repository.remove(result)

        if (remove) {
            return {
                status: 1,
                message: 'El Recurso ha sido removido con exito'
            }
        }
    }

}