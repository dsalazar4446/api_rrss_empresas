import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesDetailsService } from './categories-details.service';

describe('CategoriesDetailsService', () => {
  let service: CategoriesDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CategoriesDetailsService],
    }).compile();

    service = module.get<CategoriesDetailsService>(CategoriesDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
