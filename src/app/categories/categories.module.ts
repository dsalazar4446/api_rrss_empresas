import { Module } from '@nestjs/common';
import { CategoriesDetailsController } from './controllers/categories-details/categories-details.controller';
import { CategoriesDetailsService } from './services/categories-details/categories-details.service';
import { CategoriesService } from './services/categories/categories.service';
import { CategoriesController } from './controllers/categories/categories.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { categories } from '../../db/entitys/categories';
import { category_details } from '../../db/entitys/category_details';



@Module({
  
  imports: [
    TypeOrmModule.forFeature([
      categories,
      category_details
    ])
  ],

  controllers: [CategoriesController, CategoriesDetailsController],

  providers: [CategoriesService, CategoriesDetailsService]
})
export class CategoriesModule {}
