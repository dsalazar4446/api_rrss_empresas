import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesDetailsController } from './categories-details.controller';

describe('CategoriesDetails Controller', () => {
  let controller: CategoriesDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoriesDetailsController],
    }).compile();

    controller = module.get<CategoriesDetailsController>(CategoriesDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
