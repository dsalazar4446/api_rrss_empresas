import { Controller, Post, UseInterceptors, Headers, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ResponseInterceptor } from '../../../shared/interceptors/response.interceptor';
import { LangInterceptor } from '../../../shared/interceptors/lang.interceptor';
import { CategoriesService } from '../../services/categories/categories.service';
import { CreateCategoriesDto, UpdateCategoriesDto } from '../../dto/categories';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { arrayFiles, localOptions } from './multer.option';
import { ApiUseTags, ApiConsumes, ApiImplicitFile, ApiImplicitHeader } from '@nestjs/swagger';
import { UrlInterceptor } from '../../../shared/interceptors/url.interceptor';
import { UrlArrayInterceptor } from '../../../shared/interceptors/url-array.interceptor';

@Controller('categories')
@UseInterceptors(ResponseInterceptor)
@ApiUseTags('Categories')
export class CategoriesController {
    constructor(private readonly nameService: CategoriesService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'category_icon', required: false})
    async create(@Body() body: CreateCategoriesDto){
        return await this.nameService.create(body);
    }

    @Get()
    @UseInterceptors(new UrlArrayInterceptor('award_icon','awards'))
    async list(@Headers('language') language: string){
        return await this.nameService.list(language);
    }
    awards
    @Get(':id')
    @UseInterceptors(new UrlInterceptor('award_icon','awards'))
    async detail(@Param('id') id: string, @Headers('language') language: string){
        return await this.nameService.detail(id, language);
    }

    @Put(':id')
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'category_icon', required: false})
    @UseInterceptors(FileFieldsInterceptor(arrayFiles, localOptions))
    @UseInterceptors(LangInterceptor)
    @ApiImplicitHeader({name: 'language', required: true})
    async update(@Param('id') id: string, @Body() body: Partial<UpdateCategoriesDto>){
        return await this.nameService.update(id, body);
    }

    @Delete(':id')
    async delete(@Param('id') id: string){
        return await this.nameService.delete(id);
    }

}