import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {sectors} from "./sectors";


@Entity("sector_details",{schema:"ecomybusiness" } )
@Index("fk_sector_details_sectors1_idx",["sector",])
export class sector_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>sectors, sectors=>sectors.sectorDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sector_id'})
    sector:sectors | null;


    @Column("varchar",{ 
        nullable:false,
        name:"sector_detail"
        })
    sector_detail:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
