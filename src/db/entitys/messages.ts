import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {chats} from "./chats";
import {message_attachments} from "./message_attachments";
import { users } from "./users";


@Entity("messages",{schema:"ecomybusiness" } )
@Index("fk_Messages_users1_idx",["sender",])
@Index("fk_messages_chats1_idx",["chat",])
export class messages {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>chats, chats=>chats.messagess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'chat_id'})
    chat:chats | null;

    @ManyToOne(type=>users, users=>users.messagess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sender_id'})
    sender:users | null;


    @Column("varchar",{ 
        nullable:false,
        name:"message"
        })
    message:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"message_sended"
        })
    message_sended:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"message_recieved"
        })
    message_recieved:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"message_viewed"
        })
    message_viewed:Date;
        
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
   
    @OneToMany(type=>message_attachments, message_attachments=>message_attachments.message,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    messageAttachmentss:message_attachments[];
    
}
