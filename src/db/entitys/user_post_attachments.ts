import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {user_posts} from "./user_posts";


@Entity("user_post_attachments",{schema:"ecomybusiness" } )
@Index("fk_user_post_attachments_user_posts1_idx",["userPost",])
export class user_post_attachments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>user_posts, user_posts=>user_posts.userPostAttachmentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_post_id'})
    userPost:user_posts | null;


    @Column("varchar",{ 
        nullable:false,
        name:"post_attachement"
        })
    post_attachement:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["video","image","audio"],
        name:"post_attachement_type"
        })
    post_attachement_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
