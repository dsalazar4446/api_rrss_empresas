import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {events_talks} from "./events_talks";
import {event_talk_attendant_satatuses} from "./event_talk_attendant_satatuses";
import { users } from "./users";
import { billing_transactions } from "./billing_transactions";



@Entity("event_talk_attendants",{schema:"ecomybusiness" } )
@Index("fk_vip_event_attendants_users1_idx",["users",])
@Index("fk_event_talk_attendants_events_talks1_idx",["eventTalk",])
export class event_talk_attendants {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.eventTalkAttendantss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'users_id'})
    users:users | null;


   
    @ManyToOne(type=>events_talks, events_talks=>events_talks.eventTalkAttendantss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'event_talk_id'})
    eventTalk:events_talks | null;


    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>event_talk_attendant_satatuses, event_talk_attendant_satatuses=>event_talk_attendant_satatuses.eventTalkAttendants,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    eventTalkAttendantSatatusess:event_talk_attendant_satatuses[];
    

   
    @ManyToMany(type=>billing_transactions, billing_transactions=>billing_transactions.eventTalkAttendantss,{  nullable:false, })
    @JoinTable({ name:'event_talk_attendants_has_billing_transactions'})
    billingTransactionss:billing_transactions[];
    
}
