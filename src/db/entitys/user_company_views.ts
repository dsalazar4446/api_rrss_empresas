import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {user_companies} from "./user_companies";
import { users } from "./users";



@Entity("user_company_views",{schema:"ecomybusiness" } )
@Index("fk_user_company_views_user_companies1_idx",["userCompany",])
@Index("fk_user_company_views_users1_idx",["user",])
export class user_company_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>user_companies, user_companies=>user_companies.userCompanyViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_company_id'})
    userCompany:user_companies | null;


   
    @ManyToOne(type=>users, users=>users.userCompanyViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"user_company_share"
        })
    user_company_share:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"user_company_like"
        })
    user_company_like:number;
        

    @Column("int",{ 
        nullable:true,
        name:"user_company_rate"
        })
    user_company_rate:number | null;
        

    @Column("longtext",{ 
        nullable:true,
        name:"user_company_review"
        })
    user_company_review:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
