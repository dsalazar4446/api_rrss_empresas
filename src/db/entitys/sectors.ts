import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {sector_details} from "./sector_details";
import {subsectors} from "./subsectors";


@Entity("sectors",{schema:"ecomybusiness" } )
export class sectors {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:12,
        name:"sector_code"
        })
    sector_code:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>sector_details, sector_details=>sector_details.sector,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    sectorDetailss:sector_details[];
    

   
    @OneToMany(type=>subsectors, subsectors=>subsectors.sector,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    subsectorss:subsectors[];
    
}
