import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";


@Entity("user_languages",{schema:"ecomybusiness" } )
@Index("fk_user_languages_users1_idx",["users",])
export class user_languages {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>users, users=>users.userLanguagess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'users_id'})
    users:users | null;

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
