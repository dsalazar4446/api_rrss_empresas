import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {achivement_details} from "./achivement_details";
import { users } from "./users";


@Entity("achievements",{schema:"ecomybusiness" } )
export class achievements {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"achievement_icon"
        })
    achievement_icon:string;
        
    @Column("int",{ 
        nullable:false,
        name:"achievement_transaction_count"
        })
    achievement_transaction_count:number;
        

    @Column("int",{ 
        nullable:false,
        name:"achievement_five_stars_count"
        })
    achievement_five_stars_count:number;
        

    @Column("int",{ 
        nullable:false,
        name:"achievement_awards_count"
        })
    achievement_awards_count:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"achievement_one_month"
        })
    achievement_one_month:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"achievement_six_months"
        })
    achievement_six_months:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"achievement_one_year"
        })
    achievement_one_year:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"achievement_five_years"
        })
    achievement_five_years:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"achievement_ten_years"
        })
    achievement_ten_years:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>achivement_details, achivement_details=>achivement_details.achievement,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    achivementDetailss:achivement_details[];
    

   
    @ManyToMany(type=>users, users=>users.achievementss)
    userss:users[];
    
}
