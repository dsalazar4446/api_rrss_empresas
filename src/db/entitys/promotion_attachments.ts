import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {promotions} from "./promotions";


@Entity("promotion_attachments",{schema:"ecomybusiness" } )
@Index("fk_promotion_attachments_promotions1_idx",["promotion",])
export class promotion_attachments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>promotions, promotions=>promotions.promotionAttachmentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'promotion_id'})
    promotion:promotions | null;


    @Column("varchar",{ 
        nullable:false,
        name:"promotion_attachment"
        })
    promotion_attachment:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["1","2","3"],
        name:"promotion_attachment_type"
        })
    promotion_attachment_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
