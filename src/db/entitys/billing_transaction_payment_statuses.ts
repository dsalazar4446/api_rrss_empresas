import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {billing_transaction_payments} from "./billing_transaction_payments";


@Entity("billing_transaction_payment_statuses",{schema:"ecomybusiness" } )
@Index("fk_billing_transaction_payment_histories_billing_transactio_idx",["billingTransactionPayment",])
export class billing_transaction_payment_statuses {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>billing_transaction_payments, billing_transaction_payments=>billing_transaction_payments.billingTransactionPaymentStatusess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'billing_transaction_payment_id'})
    billingTransactionPayment:billing_transaction_payments | null;


    @Column("enum",{ 
        nullable:false,
        enum:["pending","delayed","success","rejected","underpaid","overpaid","override"],
        name:"billing_transaction_payment_status"
        })
    billing_transaction_payment_status:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
