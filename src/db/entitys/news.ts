import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {news_attachments} from "./news_attachments";
import {news_details} from "./news_details";
import {news_user_views} from "./news_user_views";
import { categories } from "./categories";
import { tags } from "./tags";

@Entity("news",{schema:"ecomybusiness" } )
export class news {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("enum",{ 
        nullable:false,
        enum:["all","company","user"],
        name:"news_target"
        })
    news_target:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["news","tips"],
        name:"news_type"
        })
    news_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>news_attachments, news_attachments=>news_attachments.news,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    newsAttachmentss:news_attachments[];
    

   
    @OneToMany(type=>news_details, news_details=>news_details.news,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    newsDetailss:news_details[];
    

   
    @OneToMany(type=>news_user_views, news_user_views=>news_user_views.news,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    newsUserViewss:news_user_views[];
    

     
    @ManyToMany(type=>categories, categories=>categories.newss,{  nullable:false, onDelete: 'CASCADE' ,onUpdate: 'CASCADE'})
    @JoinTable({ name:'news_has_categories'})   
    categoriess:categories[];
    

   
    @ManyToMany(type=>tags, tags=>tags.newss,{  nullable:false, onDelete: 'CASCADE' ,onUpdate: 'CASCADE'})
    @JoinTable({ name:'news_has_tags'})
    tagss:tags[];
    
}
