import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {promotion_attachments} from "./promotion_attachments";
import { billing_transactions } from "./billing_transactions";
import { users } from "./users";

@Entity("promotions",{schema:"ecomybusiness" } )
export class promotions {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:9,
        name:"promotion_code"
        })
    promotion_code:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["plan","contract"],
        name:"promotion_type"
        })
    promotion_type:string;
        

    @Column("float",{ 
        nullable:false,
        name:"promotion_percentage_discount"
        })
    promotion_percentage_discount:number;
        

    @Column("int",{ 
        nullable:false,
        name:"promotion_available_cant"
        })
    promotion_available_cant:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"promotion_status"
        })
    promotion_status:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>billing_transactions, billing_transactions=>billing_transactions.promotion,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionss:billing_transactions[];
    

   
    @OneToMany(type=>promotion_attachments, promotion_attachments=>promotion_attachments.promotion,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    promotionAttachmentss:promotion_attachments[];
    

   
    @ManyToMany(type=>users, users=>users.promotionss,{  nullable:false, })
    @JoinTable({ name:'promotions_viewed_by_users'})
    userss:users[];
    
}
