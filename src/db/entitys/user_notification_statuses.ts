import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {user_notifications} from "./user_notifications";


@Entity("user_notification_statuses",{schema:"ecomybusiness" } )
@Index("fk_user_notification_history_user_notifications1_idx",["userNotification",])
export class user_notification_statuses {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>user_notifications, user_notifications=>user_notifications.userNotificationStatusess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_notification_id'})
    userNotification:user_notifications | null;

    @Column("enum",{ 
        nullable:false,
        default:"send",
        enum:["send","recieved","read"],
        name:"notification_status"
        })
    notification_status:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
