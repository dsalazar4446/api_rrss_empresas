import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";


@Entity("devices",{schema:"ecomybusiness" } )
@Index("fk_devices_users_idx",["user",])
export class devices {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.devicess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["chrome","firefox","opera","explorer","android","ios","win"],
        name:"os"
        })
    os:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"ip_address"
        })
    ip_address:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:36,
        name:"uuid_device"
        })
    uuid_device:string;
        

    @Column("char",{ 
        nullable:false,
        length:128,
        name:"longitude"
        })
    longitude:string;
        

    @Column("char",{ 
        nullable:false,
        length:128,
        name:"latitude"
        })
    latitude:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"player_id"
        })
    player_id:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"device_status"
        })
    device_status:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"mobile_notification"
        })
    mobile_notification:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"mobile_vibration"
        })
    mobile_vibration:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
