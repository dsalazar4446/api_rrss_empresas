import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, UpdateDateColumn, CreateDateColumn} from "typeorm";
import {announcements} from "./announcements";
import { users } from "./users";


@Entity("announcements_users_views",{schema:"ecomybusiness" } )
@Index("fk_announcements_users_views_announcements1_idx",["announcement",])
@Index("fk_announcements_users_views_users1_idx",["user",])
export class announcements_users_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>announcements, announcements=>announcements.announcementsUsersViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'announcement_id'})
    announcement:announcements | null;


    @ManyToOne(type=>users, users=>users.announcementsUsersViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
