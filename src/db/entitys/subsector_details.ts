import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {subsectors} from "./subsectors";


@Entity("subsector_details",{schema:"ecomybusiness" } )
@Index("fk_subsector_details_subsectors1_idx",["subsector",])
export class subsector_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
   
    @ManyToOne(type=>subsectors, subsectors=>subsectors.subsectorDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'subsector_id'})
    subsector:subsectors | null;

    @Column("varchar",{ 
        nullable:false,
        name:"subsector_detail"
        })
    subsector_detail:string;
        
    @Column("enum",{ 
        nullable:false, 
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
