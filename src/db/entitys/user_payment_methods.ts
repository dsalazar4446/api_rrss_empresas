import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";
import { payment_methods } from "./payment_methods";


@Entity("user_payment_methods",{schema:"ecomybusiness" } )
@Index("fk_user_payment_methods_payment_methods1_idx",["paymentMethod",])
@Index("fk_user_payment_methods_users2_idx",["user",])
export class user_payment_methods {

    @PrimaryGeneratedColumn('uuid')
    id:string;

    @ManyToOne(type=>payment_methods, payment_methods=>payment_methods.userPaymentMethodss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'payment_method_id'})
    paymentMethod:payment_methods | null;


   
    @ManyToOne(type=>users, users=>users.userPaymentMethodss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("varchar",{ 
        nullable:false,
        length:4,
        name:"last_numbers"
        })
    last_numbers:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"payment_token"
        })
    payment_token:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["recieve","pay"],
        name:"payment_recieve_pay"
        })
    payment_recieve_pay:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"payment_main"
        })
    payment_main:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
