import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import { billing_transaction_payments } from "./billing_transaction_payments";
import { user_payment_methods } from "./user_payment_methods";




@Entity("payment_methods",{schema:"ecomybusiness" } )
export class payment_methods {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"payment_method_name"
        })
    payment_method_name:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"payment_method_icon"
        })
    payment_method_icon:string;

    @Column("enum",{ 
        nullable:false,
        enum:['ahorros','corriente'],
        default:'ahorros',
        name:"account_type"
        })
        account_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>billing_transaction_payments, billing_transaction_payments=>billing_transaction_payments.paymentMethod,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionPaymentss:billing_transaction_payments[];
    

   
    @OneToMany(type=>user_payment_methods, user_payment_methods=>user_payment_methods.paymentMethod,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPaymentMethodss:user_payment_methods[];
    
}
