import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {achievements} from "./achievements";


@Entity("achivement_details",{schema:"ecomybusiness" } )
@Index("fk_achivement_details_achievements1_idx",["achievement",])
export class achivement_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
   
    @ManyToOne(type=>achievements, achievements=>achievements.achivementDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'achievement_id'})
    achievement:achievements | null;


    @Column("varchar",{ 
        nullable:false,
        name:"achivement_title"
        })
    achivement_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"achivement_description"
        })
    achivement_description:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
