import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {product_attachments} from "./product_attachments";
import {product_service_details} from "./product_service_details";
import {product_service_views} from "./product_service_views";
import { users } from "./users";
import { billing_transaction_items } from "./billing_transaction_items";


@Entity("products_services",{schema:"ecomybusiness" } )
@Index("fk_products_services_users1_idx",["user",])
export class products_services {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.productsServicess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("enum",{ 
        nullable:false,
        enum:["product","service"],
        name:"product_service_type"
        })
    product_service_type:string;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_total"
        })
    product_service_total:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_taxes"
        })
    product_service_taxes:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_retefte"
        })
    product_service_retefte:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_ammount"
        })
    product_service_ammount:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_shipment_local"
        })
    product_service_shipment_local:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_shipment_outside"
        })
    product_service_shipment_outside:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"product_service_extenal_url"
        })
    product_service_extenal_url:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"product_service_shares"
        })
    product_service_shares:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:128,
        name:"payu_id"
        })
    payu_id:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>billing_transaction_items, billing_transaction_items=>billing_transaction_items.productService,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionItemss:billing_transaction_items[];
    

   
    @OneToMany(type=>product_attachments, product_attachments=>product_attachments.productService,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    productAttachmentss:product_attachments[];
    

   
    @OneToMany(type=>product_service_details, product_service_details=>product_service_details.productService,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    productServiceDetailss:product_service_details[];
    

   
    @OneToMany(type=>product_service_views, product_service_views=>product_service_views.productService,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    productServiceViewss:product_service_views[];
    
}
