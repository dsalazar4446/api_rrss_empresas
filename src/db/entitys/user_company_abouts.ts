import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {user_companies} from "./user_companies";


@Entity("user_company_abouts",{schema:"ecomybusiness" } )
@Index("fk_user_company_abouts_user_companies1_idx",["userCompany",])
export class user_company_abouts {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>user_companies, user_companies=>user_companies.userCompanyAboutss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_company_id'})
    userCompany:user_companies | null;


    @Column("longtext",{ 
        nullable:false,
        name:"user_company_about"
        })
    user_company_about:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
