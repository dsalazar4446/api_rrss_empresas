import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {category_details} from "./category_details";
import { events_talks } from "./events_talks";
import { news } from "./news";
import { user_posts } from "./user_posts";



@Entity("categories",{schema:"ecomybusiness" } )
export class categories {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"category_icon"
        })
    category_icon:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>category_details, category_details=>category_details.category,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    categoryDetailss:category_details[];

    @ManyToMany(type=>events_talks, events_talks=>events_talks.categoriess)
    eventsTalkss:events_talks[];
       
    @ManyToMany(type=>news, news=>news.categoriess)
    newss:news[];
    
    @ManyToMany(type=>user_posts, user_posts=>user_posts.categoriess)
    userPostss:user_posts[];
    
}
