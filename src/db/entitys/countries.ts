import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {cities} from "./cities";


@Entity("countries",{schema:"ecomybusiness" } )
export class countries {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"country_name"
        })
    country_name:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"country_flag"
        })
    country_flag:string;
        
    @Column("varchar",{ 
        nullable:false,
        length:6,
        name:"country_code"
        })
    country_code:string;
        
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>cities, cities=>cities.country,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    citiess:cities[];
    
}
