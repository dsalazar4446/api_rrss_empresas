import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {news} from "./news";
import { users } from "./users";


@Entity("news_user_views",{schema:"ecomybusiness" } )
@Index("fk_news_user_views_news1_idx",["news",])
@Index("fk_news_user_views_users1_idx",["user",])
export class news_user_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>news, news=>news.newsUserViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'news_id'})
    news:news | null;


   
    @ManyToOne(type=>users, users=>users.newsUserViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"news_user_share"
        })
    news_user_share:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"news_user_like"
        })
    news_user_like:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
