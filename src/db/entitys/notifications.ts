import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {notification_msgs} from "./notification_msgs";
import {user_notifications} from "./user_notifications";


@Entity("notifications",{schema:"ecomybusiness" } )
export class notifications {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["register","contract","follower","new_post","new_like","new_rate"],
        name:"notification_type"
        })
    notification_type:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["admin","support","users","company","advisors"],
        name:"notification_target"
        })
    notification_target:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"notification_icon"
        })
    notification_icon:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"notification_mime_sound_file"
        })
    notification_mime_sound_file:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>notification_msgs, notification_msgs=>notification_msgs.notifications,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    notificationMsgss:notification_msgs[];
    

   
    @OneToMany(type=>user_notifications, user_notifications=>user_notifications.notification,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userNotificationss:user_notifications[];
    
}
