import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {plans} from "./plans";


@Entity("plan_details",{schema:"ecomybusiness" } )
@Index("fk_plan_details_plans1_idx",["plan",])
export class plan_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>plans, plans=>plans.planDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plan_id'})
    plan:plans | null;


    @Column("varchar",{ 
        nullable:false,
        length:75,
        name:"plan_title"
        })
    plan_title:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"plan_description"
        })
    plan_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
