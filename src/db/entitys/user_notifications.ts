import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {notifications} from "./notifications";
import {user_notification_statuses} from "./user_notification_statuses";
import { users } from "./users";


@Entity("user_notifications",{schema:"ecomybusiness" } )
@Index("fk_user_notifications_notifications1_idx",["notification",])
@Index("fk_user_notifications_users1_idx",["user",])
export class user_notifications {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>notifications, notifications=>notifications.userNotificationss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'notification_id'})
    notification:notifications | null;
   
    @ManyToOne(type=>users, users=>users.userNotificationss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
   
    @OneToMany(type=>user_notification_statuses, user_notification_statuses=>user_notification_statuses.userNotification,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userNotificationStatusess:user_notification_statuses[];
    
}
