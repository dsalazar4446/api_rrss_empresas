import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {announcements} from "./announcements";


@Entity("announcement_details",{schema:"ecomybusiness" } )
@Index("fk_announcement_details_announcements1_idx",["announcement",])
export class announcement_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>announcements, announcements=>announcements.announcementDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'announcement_id'})
    announcement:announcements | null;


    @Column("varchar",{ 
        nullable:false,
        name:"announcement_title"
        })
    announcement_title:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"announcement_description"
        })
    announcement_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
