import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {advisories} from "./advisories";


@Entity("advisory_statuses",{schema:"ecomybusiness" } )
@Index("fk_advisory_statuses_advisories1_idx",["advisories",])
export class advisory_statuses {

    @PrimaryGeneratedColumn('uuid')
    id:string;

    @ManyToOne(type=>advisories, advisories=>advisories.advisoryStatusess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'advisories_id'})
    advisories:advisories | null;


    @Column("enum",{ 
        nullable:false,
        enum:["requested","confirmed","rejected","canceled"],
        name:"advisory_status"
        })
    advisory_status:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
