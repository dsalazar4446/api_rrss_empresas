import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {user_company_abouts} from "./user_company_abouts";
import {user_company_views} from "./user_company_views";
import { users } from "./users";
import { cities } from "./cities";
import { subsectors } from "./subsectors";



@Entity("user_companies",{schema:"ecomybusiness" } )
@Index("fk_user_companies_users1_idx",["user",])
@Index("fk_user_companies_cities1_idx",["city",])
@Index("fk_user_companies_subsectors1_idx",["subsector",])
export class user_companies {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.userCompaniess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


   
    @ManyToOne(type=>cities, cities=>cities.userCompaniess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'city_id'})
    city:cities | null;


   
    @ManyToOne(type=>subsectors, subsectors=>subsectors.userCompaniess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'subsector_id'})
    subsector:subsectors | null;


    @Column("varchar",{ 
        nullable:false,
        length:36,
        name:"user_company_identification"
        })
    user_company_identification:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"user_company_name"
        })
    user_company_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"user_company_logo"
        })
    user_company_logo:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"user_company_image"
        })
    user_company_image:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"user_company_document_attachment"
        })
    user_company_document_attachment:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"user_company_politics_attachment"
        })
    user_company_politics_attachment:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:6,
        name:"user_company_color"
        })
    user_company_color:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"user_company_latitude"
        })
    user_company_latitude:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"user_company_longitude"
        })
    user_company_longitude:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"user_company_address_1"
        })
    user_company_address_1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"user_company_adddres_2"
        })
    user_company_adddres_2:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"user_company_website"
        })
    user_company_website:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"user_company_email"
        })
    user_company_email:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:12,
        name:"user_company_phone"
        })
    user_company_phone:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:12,
        name:"user_company_cellphone"
        })
    user_company_cellphone:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:12,
        name:"user_company_whatsapp"
        })
    user_company_whatsapp:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>user_company_abouts, user_company_abouts=>user_company_abouts.userCompany,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompanyAboutss:user_company_abouts[];
    

   
    @OneToMany(type=>user_company_views, user_company_views=>user_company_views.userCompany,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompanyViewss:user_company_views[];
    
}
