import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";


@Entity("taxes",{schema:"ecomybusiness" } )
export class taxes {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"tax_date"
        })
    tax_date:Date;
        

    @Column("float",{ 
        nullable:false,
        name:"tax_percentage"
        })
    tax_percentage:number;
        

    @Column("float",{ 
        nullable:false,
        name:"retefete_percentage"
        })
    retefete_percentage:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
