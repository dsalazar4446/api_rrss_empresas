import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {messages} from "./messages";
import { users } from "./users";


@Entity("chats",{schema:"ecomybusiness" } )
export class chats {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("enum",{ 
        nullable:false,
        enum:["helpy","support"],
        name:"chat_type"
        })
    chat_type:string;
        
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
    @OneToMany(type=>messages, messages=>messages.chat,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    messagess:messages[];
    
    @ManyToMany(type=>users, users=>users.chatss,{  nullable:false, })
    @JoinTable({ name:'chats_has_users'})
    userss:users[];
    
}
