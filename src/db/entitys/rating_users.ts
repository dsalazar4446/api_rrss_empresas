import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import { users } from "./users";
import { awards } from "./awards";

@Entity("rating_users",{schema:"ecomybusiness" } )
@Index("fk_rating_users_users1_idx",["rater",])
@Index("fk_rating_users_users2_idx",["rated",])
export class rating_users {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.ratingUserss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rater_id'})
    rater:users | null;


   
    @ManyToOne(type=>users, users=>users.ratingUserss2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rated_id'})
    rated:users | null;


    @Column("int",{ 
        nullable:false,
        name:"rating"
        })
    rating:number;
        

    @Column("longtext",{ 
        nullable:true,
        name:"rating_comments"
        })
    rating_comments:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @ManyToMany(type=>awards, awards=>awards.ratingUserss,{  nullable:false, })
    @JoinTable({ name:'ratings_has_awards'})
    awardss:awards[];
    
}
