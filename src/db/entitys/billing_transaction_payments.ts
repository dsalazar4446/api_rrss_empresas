import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {billing_transactions} from "./billing_transactions";
import {billing_transaction_payment_statuses} from "./billing_transaction_payment_statuses";
import { payment_methods } from "./payment_methods";


@Entity("billing_transaction_payments",{schema:"ecomybusiness" } )
@Index("fk_billing_transaction_payments_billing_transactions1_idx",["billingTransaction",])
@Index("fk_billing_transaction_payments_payment_methods1_idx",["paymentMethod",])
export class billing_transaction_payments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>billing_transactions, billing_transactions=>billing_transactions.billingTransactionPaymentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'billing_transaction_id'})
    billingTransaction:billing_transactions | null;

    @ManyToOne(type=>payment_methods, payment_methods=>payment_methods.billingTransactionPaymentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'payment_method_id'})
    paymentMethod:payment_methods | null;


    @Column("varchar",{ 
        nullable:false,
        name:"billing_transaction_payment_token"
        })
    billing_transaction_payment_token:string;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"billing_transaction_payment_ammount"
        })
    billing_transaction_payment_ammount:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>billing_transaction_payment_statuses, billing_transaction_payment_statuses=>billing_transaction_payment_statuses.billingTransactionPayment,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionPaymentStatusess:billing_transaction_payment_statuses[];
    
}
