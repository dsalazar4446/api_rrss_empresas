import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {news} from "./news";


@Entity("news_details",{schema:"ecomybusiness" } )
@Index("fk_news_details_news1_idx",["news",])
export class news_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>news, news=>news.newsDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'news_id'})
    news:news | null;


    @Column("varchar",{ 
        nullable:false,
        name:"news_title"
        })
    news_title:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"news_description"
        })
    news_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
