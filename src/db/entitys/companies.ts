import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {company_details} from "./company_details";


@Entity("companies",{schema:"ecomybusiness" } )
export class companies {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"company_name"
        })
    company_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:75,
        name:"company_identification"
        })
    company_identification:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"company_email"
        })
    company_email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:15,
        name:"company_phone"
        })
    company_phone:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"company_logo"
        })
    company_logo:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"company_address"
        })
    company_address:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"company_latitude"
        })
    company_latitude:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"company_longitude"
        })
    company_longitude:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:75,
        name:"company_postal_code"
        })
    company_postal_code:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"company_website"
        })
    company_website:string;
     
    @Column("enum",{ 
        nullable:false,
        enum:{
            light:'light',
            gray:'gray',
            dark:'dark',
        },
        default: 'light',
        name:"user_company_color_txt"
        })
    user_company_color_txt: string 
    
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>company_details, company_details=>company_details.company,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    companyDetailss:company_details[];
    
}
