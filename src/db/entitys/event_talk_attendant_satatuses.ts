import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {event_talk_attendants} from "./event_talk_attendants";


@Entity("event_talk_attendant_satatuses",{schema:"ecomybusiness" } )
@Index("fk_event_talk_attendant_satatuses_event_talk_attendants1_idx",["eventTalkAttendants",])
export class event_talk_attendant_satatuses {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>event_talk_attendants, event_talk_attendants=>event_talk_attendants.eventTalkAttendantSatatusess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'event_talk_attendants_id'})
    eventTalkAttendants:event_talk_attendants | null;


    @Column("enum",{ 
        nullable:false,
        enum:["reserved","confirmed","canceled","missed","attended"],
        name:"event_talk_attendant_satatus"
        })
    event_talk_attendant_satatus:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
}
