import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {events_talks} from "./events_talks";
import { users } from "./users";


@Entity("event_talk_organizers",{schema:"ecomybusiness" } )
@Index("fk_vip_event_organizers_users1_idx",["users",])
@Index("fk_event_talk_organizers_events_talks1_idx",["eventTalk",])
export class event_talk_organizers {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.eventTalkOrganizerss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'users_id'})
    users:users | null;


   
    @ManyToOne(type=>events_talks, events_talks=>events_talks.eventTalkOrganizerss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'event_talk_id'})
    eventTalk:events_talks | null;


    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
