import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {news} from "./news";


@Entity("news_attachments",{schema:"ecomybusiness" } )
@Index("fk_news_attachments_news1_idx",["news",])
export class news_attachments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>news, news=>news.newsAttachmentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'news_id'})
    news:news | null;


    @Column("varchar",{ 
        nullable:false,
        name:"news_attachment"
        })
    news_attachment:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["1","2","3"],
        name:"news_attachment_type"
        })
    news_attachment_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
