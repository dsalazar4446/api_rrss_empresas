import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {plan_details} from "./plan_details";
import {plan_pricings} from "./plan_pricings";
import {plan_transactions} from "./plan_transactions";


@Entity("plans",{schema:"ecomybusiness" } )
export class plans {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"plan_icon"
        })
    plan_icon:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"plan_image"
        })
    plan_image:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plan_talks"
        })
    plan_talks:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plan_events"
        })
    plan_events:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plan_tips"
        })
    plan_tips:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"plan_advisory"
        })
    plan_advisory:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"plan_advisory_cant"
        })
    plan_advisory_cant:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>plan_details, plan_details=>plan_details.plan,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    planDetailss:plan_details[];
    

   
    @OneToMany(type=>plan_pricings, plan_pricings=>plan_pricings.plan,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    planPricingss:plan_pricings[];
    

   
    @OneToMany(type=>plan_transactions, plan_transactions=>plan_transactions.plan,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    planTransactionss:plan_transactions[];
    
}
