import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {products_services} from "./products_services";


@Entity("product_attachments",{schema:"ecomybusiness" } )
@Index("fk_product_attachments_products_services1_idx",["productService",])
export class product_attachments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>products_services, products_services=>products_services.productAttachmentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'product_service_id'})
    productService:products_services | null;


    @Column("varchar",{ 
        nullable:false,
        name:"product_attachment"
        })
    product_attachment:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["video","image","audio"],
        name:"product_attachment_type"
        })
    product_attachment_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
