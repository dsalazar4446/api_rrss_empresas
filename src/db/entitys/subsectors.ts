import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {sectors} from "./sectors";
import {subsector_details} from "./subsector_details";
import { user_companies } from "./user_companies";

@Entity("subsectors",{schema:"ecomybusiness" } )
@Index("fk_subsectors_sectors1_idx",["sector",])
export class subsectors {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>sectors, sectors=>sectors.subsectorss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'sector_id'})
    sector:sectors | null;


    @Column("varchar",{ 
        nullable:true,
        length:12,
        name:"subsector_code"
        })
    subsector_code:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>subsector_details, subsector_details=>subsector_details.subsector,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    subsectorDetailss:subsector_details[];
    

   
    @OneToMany(type=>user_companies, user_companies=>user_companies.subsector,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompaniess:user_companies[];
    
}
