import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import { cities } from "./cities";
import { advisories } from "./advisories";
import { announcements_users_views } from "./announcements_users_views";
import { auths } from "./auths";
import { billing_transactions } from "./billing_transactions";
import { bios } from "./bios";
import { devices } from "./devices";
import { event_talk_attendants } from "./event_talk_attendants";
import { event_talk_organizers } from "./event_talk_organizers";
import { followers } from "./followers";
import { messages } from "./messages";
import { news_user_views } from "./news_user_views";
import { product_service_views } from "./product_service_views";
import { products_services } from "./products_services";
import { rating_users } from "./rating_users";
import { tickets } from "./tickets";
import { user_companies } from "./user_companies";
import { user_company_views } from "./user_company_views";
import { user_languages } from "./user_languages";
import { user_notifications } from "./user_notifications";
import { user_payment_methods } from "./user_payment_methods";
import { user_post_views } from "./user_post_views";
import { user_posts } from "./user_posts";
import { user_profile_views } from "./user_profile_views";
import { chats } from "./chats";
import { promotions } from "./promotions";
import { achievements } from "./achievements";
import { tags } from "./tags";

@Entity("users",{schema:"ecomybusiness" } )
@Index("email_UNIQUE",["email",],{unique:true})
@Index("phone_UNIQUE",["cellphone",],{unique:true})
export class users {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>cities, cities=>cities.userss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'city_id'})
    city:cities | null;


    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        name:"cellphone"
        })
    cellphone:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"avatar"
        })
    avatar:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:75,
        name:"first_name"
        })
    first_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:75,
        name:"last_name"
        })
    last_name:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["male","female"],
        name:"gender"
        })
    gender:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"birth_date"
        })
    birth_date:Date | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"latitude"
        })
    latitude:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"longitude"
        })
    longitude:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"address_1"
        })
    address_1:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        name:"adddres_2"
        })
    adddres_2:string | null;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'1'",
        name:"assistant_step"
        })
    assistant_step:number;
        

    @Column("char",{ 
        nullable:false,
        length:128,
        default: () => "'0000'",
        name:"verification_code"
        })
    verification_code:string;
        

    @Column("enum",{ 
        nullable:false,
        default:"pending",
        enum:["pending","active","unactive"],
        name:"user_status"
        })
    user_status:string;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: 0,
        name:"role_admin"
        })
    role_admin:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: 1,
        name:"role_user"
        })
    role_user:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: 0,
        name:"role_advisor"
        })
    role_advisor:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: 0,
        name:"role_company"
        })
    role_company:number;
    
    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>advisories, advisories=>advisories.advisorUser,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    advisoriess:advisories[];
    

   
    @OneToMany(type=>advisories, advisories=>advisories.requestedByUser,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    advisoriess2:advisories[];
    

   
    @OneToMany(type=>announcements_users_views, announcements_users_views=>announcements_users_views.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    announcementsUsersViewss:announcements_users_views[];
    

   
    @OneToMany(type=>auths, auths=>auths.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    authss:auths[];
    

   
    @OneToMany(type=>billing_transactions, billing_transactions=>billing_transactions.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionss:billing_transactions[];
    

   
    @OneToMany(type=>bios, bios=>bios.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    bioss:bios[];
    

   
    @OneToMany(type=>devices, devices=>devices.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    devicess:devices[];
    

   
    @OneToMany(type=>event_talk_attendants, event_talk_attendants=>event_talk_attendants.users,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    eventTalkAttendantss:event_talk_attendants[];
    

   
    @OneToMany(type=>event_talk_organizers, event_talk_organizers=>event_talk_organizers.users,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    eventTalkOrganizerss:event_talk_organizers[];
    

   
    @OneToMany(type=>followers, followers=>followers.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    followerss:followers[];
    

   
    @OneToMany(type=>followers, followers=>followers.followed,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    followerss2:followers[];
    

   
    @OneToMany(type=>messages, messages=>messages.sender,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    messagess:messages[];
    

   
    @OneToMany(type=>news_user_views, news_user_views=>news_user_views.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    newsUserViewss:news_user_views[];
    

   
    @OneToMany(type=>product_service_views, product_service_views=>product_service_views.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    productServiceViewss:product_service_views[];
    

   
    @OneToMany(type=>products_services, products_services=>products_services.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    productsServicess:products_services[];
    

   
    @OneToMany(type=>rating_users, rating_users=>rating_users.rater,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    ratingUserss:rating_users[];
    

   
    @OneToMany(type=>rating_users, rating_users=>rating_users.rated,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    ratingUserss2:rating_users[];
    

   
    @OneToMany(type=>tickets, tickets=>tickets.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    ticketss:tickets[];
    

   
    @OneToMany(type=>user_companies, user_companies=>user_companies.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompaniess:user_companies[];
    

   
    @OneToMany(type=>user_company_views, user_company_views=>user_company_views.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompanyViewss:user_company_views[];
    

   
    @OneToMany(type=>user_languages, user_languages=>user_languages.users,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userLanguagess:user_languages[];
    

   
    @OneToMany(type=>user_notifications, user_notifications=>user_notifications.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userNotificationss:user_notifications[];
    

   
    @OneToMany(type=>user_payment_methods, user_payment_methods=>user_payment_methods.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPaymentMethodss:user_payment_methods[];
    

   
    @OneToMany(type=>user_post_views, user_post_views=>user_post_views.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPostViewss:user_post_views[];
    

   
    @OneToMany(type=>user_posts, user_posts=>user_posts.user,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPostss:user_posts[];
    

   
    @OneToMany(type=>user_profile_views, user_profile_views=>user_profile_views.viewedByUser,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userProfileViewss:user_profile_views[];
    

   
    @OneToMany(type=>user_profile_views, user_profile_views=>user_profile_views.profileUser,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userProfileViewss2:user_profile_views[];
    

   
    @ManyToMany(type=>chats, chats=>chats.userss)
    chatss:chats[];
    

   
    @ManyToMany(type=>promotions, promotions=>promotions.userss)
    promotionss:promotions[];
    

   
    @ManyToMany(type=>achievements, achievements=>achievements.userss,{  nullable:false, })
    @JoinTable({ name:'users_has_achievements'})
    achievementss:achievements[];
    

   
    @ManyToMany(type=>tags, tags=>tags.userss,{  nullable:false, })
    @JoinTable({ name:'users_has_tags'})
    tagss:tags[];
    
}
