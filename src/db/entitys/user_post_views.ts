import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";
import {user_posts} from "./user_posts";


@Entity("user_post_views",{schema:"ecomybusiness" } )
@Index("fk_user_post_views_user_posts1_idx",["userPost",])
@Index("fk_user_post_views_users1_idx",["user",])
export class user_post_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>users, users=>users.userPostViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;
   
    @ManyToOne(type=>user_posts, user_posts=>user_posts.userPostViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_post_id'})
    userPost:user_posts | null;

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"user_post_share"
        })
    user_post_share:boolean;
        
    @Column("tinyint",{ 
        nullable:false,
        width:1,
        name:"user_post_like"
        })
    user_post_like:boolean;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
