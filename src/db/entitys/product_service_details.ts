import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {products_services} from "./products_services";


@Entity("product_service_details",{schema:"ecomybusiness" } )
@Index("fk_product_service_details_products_services1_idx",["productService",])
export class product_service_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>products_services, products_services=>products_services.productServiceDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'product_service_id'})
    productService:products_services | null;


    @Column("varchar",{ 
        nullable:false,
        name:"product_service_name"
        })
    product_service_name:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"product_service_description"
        })
    product_service_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
