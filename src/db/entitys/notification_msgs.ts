import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {notifications} from "./notifications";


@Entity("notification_msgs",{schema:"ecomybusiness" } )
@Index("fk_notification_msgs_notifications1_idx",["notifications",])
export class notification_msgs {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>notifications, notifications=>notifications.notificationMsgss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'notifications_id'})
    notifications:notifications | null;


    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"notification_msg_header"
        })
    notification_msg_header:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"notification_msg_body"
        })
    notification_msg_body:string;
        

    @Column("enum",{ 
        nullable:false,
        default:'app',
        enum:["app","web"],
        name:"notification_target"
        })
    notification_target:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
