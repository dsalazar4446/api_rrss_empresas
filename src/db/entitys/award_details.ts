import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {awards} from "./awards";


@Entity("award_details",{schema:"ecomybusiness" } )
@Index("fk_award_details_awards1_idx",["award",])
export class award_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>awards, awards=>awards.awardDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'award_id'})
    award:awards | null;


    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"award_title"
        })
    award_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"award_description"
        })
    award_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
