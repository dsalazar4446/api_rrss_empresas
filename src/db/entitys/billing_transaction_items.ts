import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {billing_transactions} from "./billing_transactions";
import { products_services } from "./products_services";


@Entity("billing_transaction_items",{schema:"ecomybusiness" } )
@Index("fk_billing_transaction_items_products_services1_idx",["productService",])
@Index("fk_billing_transaction_items_billing_transactions1_idx",["billingTransaction",])
export class billing_transaction_items {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>billing_transactions, billing_transactions=>billing_transactions.billingTransactionItemss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'billing_transaction_id'})
    billingTransaction:billing_transactions | null;


   
    @ManyToOne(type=>products_services, products_services=>products_services.billingTransactionItemss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'product_service_id'})
    productService:products_services | null;


    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_item_cant"
        })
    billing_transaction_item_cant:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_item_subtotal"
        })
    billing_transaction_item_subtotal:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_item_taxes"
        })
    billing_transaction_item_taxes:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_item_retefte"
        })
    billing_transaction_item_retefte:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_item_total"
        })
    billing_transaction_item_total:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
