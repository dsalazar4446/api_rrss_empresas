import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, UpdateDateColumn, CreateDateColumn} from "typeorm";
import {award_details} from "./award_details";
import { rating_users } from "./rating_users";


@Entity("awards",{schema:"ecomybusiness" } )
export class awards {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"award_icon"
        })
    award_icon:string;
        
    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
    @OneToMany(type=>award_details, award_details=>award_details.award,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    awardDetailss:award_details[];
    
    @ManyToMany(type=>rating_users, rating_users=>rating_users.awardss)
    ratingUserss:rating_users[];
    
}
