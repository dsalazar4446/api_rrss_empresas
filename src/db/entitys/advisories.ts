import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {advisory_statuses} from "./advisory_statuses";
import { users } from "./users";


@Entity("advisories",{schema:"ecomybusiness" } )
@Index("fk_advisories_users3_idx",["advisorUser",])
@Index("fk_advisories_users4_idx",["requestedByUser",])
export class advisories {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>users, users=>users.advisoriess2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'requested_by_user_id'})
    requestedByUser:users | null;


   
    @ManyToOne(type=>users, users=>users.advisoriess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'advisor_user_id'})
    advisorUser:users | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"advisory_sheduled"
        })
    advisory_sheduled:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"advisory_start"
        })
    advisory_start:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"advisory_end"
        })
    advisory_end:Date;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"advisory_rate"
        })
    advisory_rate:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:45,
        name:"advisory_observation"
        })
    advisory_observation:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>advisory_statuses, advisory_statuses=>advisory_statuses.advisories,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    advisoryStatusess:advisory_statuses[];
    
}
