import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";
import {user_post_attachments} from "./user_post_attachments";
import {user_post_views} from "./user_post_views";
import { categories } from "./categories";
import { tags } from "./tags";



@Entity("user_posts",{schema:"ecomybusiness" } )
@Index("fk_user_works_users1_idx",["user",])
export class user_posts {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>users, users=>users.userPostss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;

    @Column("varchar",{ 
        nullable:false,
        name:"user_post_picture"
        })
    user_post_picture:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"user_post_title"
        })
    user_post_title:string;
        
    @Column("longtext",{ 
        nullable:false,
        name:"user_post_description"
        })
    user_post_description:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>user_post_attachments, user_post_attachments=>user_post_attachments.userPost,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPostAttachmentss:user_post_attachments[];
    
    @OneToMany(type=>user_post_views, user_post_views=>user_post_views.userPost,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userPostViewss:user_post_views[];
    
    @ManyToMany(type=>categories, categories=>categories.userPostss,{  nullable:false, })
    @JoinTable({ name:'user_posts_has_categories'})
    categoriess:categories[];
    
    @ManyToMany(type=>tags, tags=>tags.userPostss,{  nullable:false, })
    @JoinTable({ name:'user_posts_has_tags'})
    tagss:tags[];
    
}
