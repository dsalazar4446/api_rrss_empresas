import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {companies} from "./companies";


@Entity("company_details",{schema:"ecomybusiness" } )
@Index("fk_company_details_companies1_idx",["company",])
export class company_details {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>companies, companies=>companies.companyDetailss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'company_id'})
    company:companies | null;


    @Column("longtext",{ 
        nullable:false,
        name:"company_detail"
        })
    company_detail:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"company_terms"
        })
    company_terms:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"company_privacy"
        })
    company_privacy:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
