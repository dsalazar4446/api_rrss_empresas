import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {event_talk_attendants} from "./event_talk_attendants";
import {event_talk_organizers} from "./event_talk_organizers";
import { categories } from "./categories";
import { tags } from "./tags";




@Entity("events_talks",{schema:"ecomybusiness" } )
export class events_talks {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @Column("varchar",{ 
        nullable:false,
        name:"event_talk_title"
        })
    event_talk_title:string;
        
    @Column("longtext",{ 
        nullable:false,
        name:"event_talk_description"
        })
    event_talk_description:string;
        
    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"event_talk_schedule"
        })
    event_talk_schedule:Date;
        
    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"event_talk_start"
        })
    event_talk_start:Date;
        
    @Column("varchar",{ 
        nullable:false,
        length:45,
        default: () => "'current_timestamp'",
        name:"event_talk_end"
        })
    event_talk_end:string;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"event_talk_taxes"
        })
    event_talk_taxes:number;
        
    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"event_talk_retefte"
        })
    event_talk_retefte:number;
        
    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"event_talk_ammount"
        })
    event_talk_ammount:number;
        
    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"event_talk_total"
        })
    event_talk_total:number;

    @Column("enum",{ 
        nullable:false,
        default:"event",
        enum:["event","talk"],
        name:"event_talk_type"
        })
    event_talk_type:string;
        
    @Column({
        type:'enum', 
        nullable:false,
        default: "online",
        enum:["online","presential"],
        name:"event_talk_chanel"
        })
    event_talk_chanel:string;
        
    @Column("enum",{ 
        nullable:false,
        default:"free",
        enum:["free","plan","payment","personal"],
        name:"event_talk_target"
        })
    event_talk_target:string;
        
    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"event_talk_broadcast_url"
        })
    event_talk_broadcast_url:string | null;
        
    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"event_talk_broadcast_key"
        })
    event_talk_broadcast_key:string | null;
        
    @Column("varchar",{ 
        nullable:true,
        length:500,
        name:"event_talk_share_url"
        })
    event_talk_share_url:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:128,
        name:"latitude"
        })
    latitude:string | null;
        
    @Column("char",{ 
        nullable:true,
        length:128,
        name:"longitude"
        })
    longitude:string | null;
        
    @Column("varchar",{ 
        nullable:true,
        name:"address_1"
        })
    address_1:string | null;
        
    @Column("varchar",{ 
        nullable:true,
        name:"adddres_2"
        })
    adddres_2:string | null;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        default:"es",
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>event_talk_attendants, event_talk_attendants=>event_talk_attendants.eventTalk,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    eventTalkAttendantss:event_talk_attendants[];
    

   
    @OneToMany(type=>event_talk_organizers, event_talk_organizers=>event_talk_organizers.eventTalk,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    eventTalkOrganizerss:event_talk_organizers[];
    

   
    @ManyToMany(type=>categories, categories=>categories.eventsTalkss,{  nullable:false, onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    @JoinTable({ name:'events_talks_has_categories'})
    categoriess:categories[];
    

   
    @ManyToMany(type=>tags, tags=>tags.eventsTalkss,{  nullable:false, onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    @JoinTable({ name:'events_talks_has_tags'})
    tagss:tags[];
    
}
