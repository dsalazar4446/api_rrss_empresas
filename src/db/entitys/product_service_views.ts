import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {products_services} from "./products_services";
import { users } from "./users";


@Entity("product_service_views",{schema:"ecomybusiness" } )
@Index("fk_product_service_views_products_services1_idx",["productService",])
@Index("fk_product_service_views_users1_idx",["user",])
export class product_service_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>products_services, products_services=>products_services.productServiceViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'product_service_id'})
    productService:products_services | null;


   
    @ManyToOne(type=>users, users=>users.productServiceViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;


    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"product_share"
        })
    product_share:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'0'",
        name:"product_like"
        })
    product_like:number;
        

    @Column("longtext",{ 
        nullable:true,
        name:"product_review"
        })
    product_review:string | null;
        

    @Column("enum",{ 
        nullable:true,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
