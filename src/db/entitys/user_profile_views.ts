import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";


@Entity("user_profile_views",{schema:"ecomybusiness" } )
@Index("fk_user_profile_views_users1_idx",["viewedByUser",])
@Index("fk_user_profile_views_users2_idx",["profileUser",])
export class user_profile_views {

    @PrimaryGeneratedColumn('uuid')
    id:string;

    @ManyToOne(type=>users, users=>users.userProfileViewss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'viewed_by_user_id'})
    viewedByUser:users | null;
   
    @ManyToOne(type=>users, users=>users.userProfileViewss2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'profile_user_id'})
    profileUser:users | null;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date
        
}
