import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {announcement_details} from "./announcement_details";
import {announcements_users_views} from "./announcements_users_views";


@Entity("announcements",{schema:"ecomybusiness" } )
export class announcements {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["all","company","user","advisor"],
        name:"announcement_target"
        })
    announcement_target:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"announcement_image"
        })
    announcement_image:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>announcement_details, announcement_details=>announcement_details.announcement,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    announcementDetailss:announcement_details[];
    

   
    @OneToMany(type=>announcements_users_views, announcements_users_views=>announcements_users_views.announcement,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    announcementsUsersViewss:announcements_users_views[];
    
}
