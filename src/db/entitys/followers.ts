import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {users} from "./users";


@Entity("followers",{schema:"ecomybusiness" } )
@Index("fk_followers_users1_idx",["user",])
@Index("fk_followers_users2_idx",["followed",])
export class followers {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>users, users=>users.followerss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;
   
    @ManyToOne(type=>users, users=>users.followerss2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'followed_id'})
    followed:users | null;

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        
    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
