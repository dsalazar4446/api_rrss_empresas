import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {messages} from "./messages";


@Entity("message_attachments",{schema:"ecomybusiness" } )
@Index("fk_message_attachements_messages1_idx",["message",])
export class message_attachments {

    @PrimaryGeneratedColumn('uuid')
    id:string;
    
    @ManyToOne(type=>messages, messages=>messages.messageAttachmentss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'message_id'})
    message:messages | null;

    @Column("varchar",{ 
        nullable:false,
        name:"message_attachment"
        })
    message_attachment:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["video","image","audio"],
        name:"message_attachment_type"
        })
    message_attachment_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
