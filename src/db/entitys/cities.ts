import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {countries} from "./countries";
import { user_companies } from "./user_companies";
import { users } from "./users";

@Entity("cities",{schema:"ecomybusiness" } )
@Index("fk_cities_countries1_idx",["country",])
export class cities {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>countries, countries=>countries.citiess,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'country_id'})
    country:countries | null;


    @Column("varchar",{ 
        nullable:false,
        name:"city_name"
        })
    city_name:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>user_companies, user_companies=>user_companies.city,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userCompaniess:user_companies[];
    

   
    @OneToMany(type=>users, users=>users.city,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    userss:users[];
    
}
