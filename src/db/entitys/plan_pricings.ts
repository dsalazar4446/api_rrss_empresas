import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {plans} from "./plans";


@Entity("plan_pricings",{schema:"ecomybusiness" } )
@Index("fk_plan_pricings_plans1_idx",["plan",])
export class plan_pricings {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>plans, plans=>plans.planPricingss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plan_id'})
    plan:plans | null;


    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"plan_taxes"
        })
    plan_taxes:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"plan_retefte"
        })
    plan_retefte:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"plan_ammount"
        })
    plan_ammount:number;
        

    @Column("float",{ 
        nullable:false,
        default: () => "'0'",
        name:"plan_total"
        })
    plan_total:number;
        

    @Column("tinyint",{ 
        nullable:false,
        width:1,
        default: () => "'1'",
        name:"plan_price_status"
        })
    plan_price_status:number;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
