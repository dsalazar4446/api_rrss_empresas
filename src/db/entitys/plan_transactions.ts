import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {plans} from "./plans";
import { billing_transactions } from "./billing_transactions";


@Entity("plan_transactions",{schema:"ecomybusiness" } )
@Index("fk_plan_transactions_plans1_idx",["plan",])
@Index("fk_plan_transactions_billing_transactions1_idx",["billingTransaction",])
export class plan_transactions {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

   
    @ManyToOne(type=>plans, plans=>plans.planTransactionss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'plan_id'})
    plan:plans | null;


   
    @ManyToOne(type=>billing_transactions, billing_transactions=>billing_transactions.planTransactionss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'billing_transaction_id'})
    billingTransaction:billing_transactions | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"plan_start"
        })
    plan_start:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"plan_end"
        })
    plan_end:Date;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        
}
