import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import { events_talks } from "./events_talks";
import { news } from "./news";
import { user_posts } from "./user_posts";
import { users } from "./users";


@Entity("tags",{schema:"ecomybusiness" } )
export class tags {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"tag_name"
        })
    tag_name:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["es","en","it","pr"],
        name:"language_type"
        })
    language_type:string;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @ManyToMany(type=>events_talks, events_talks=>events_talks.tagss)
    eventsTalkss:events_talks[];
    

   
    @ManyToMany(type=>news, news=>news.tagss)
    newss:news[];
    

   
    @ManyToMany(type=>user_posts, user_posts=>user_posts.tagss)
    userPostss:user_posts[];
    

   
    @ManyToMany(type=>users, users=>users.tagss)
    userss:users[];
    
}
