import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {billing_transaction_items} from "./billing_transaction_items";
import {billing_transaction_payments} from "./billing_transaction_payments";
import { users } from "./users";
import { promotions } from "./promotions";
import { plan_transactions } from "./plan_transactions";
import { event_talk_attendants } from "./event_talk_attendants";


@Entity("billing_transactions",{schema:"ecomybusiness" } )
@Index("fk_transactions_users1_idx",["user",])
@Index("fk_billing_transactions_promotions1_idx",["promotion",])
export class billing_transactions {

    @PrimaryGeneratedColumn('uuid')
    id:string;
        
    @ManyToOne(type=>users, users=>users.billingTransactionss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'user_id'})
    user:users | null;

    @ManyToOne(type=>promotions, promotions=>promotions.billingTransactionss,{ onDelete: 'NO ACTION',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'promotion_id'})
    promotion:promotions | null;

    @Column("varchar",{ 
        nullable:false,
        length:9,
        name:"billing_transaction_code"
        })
    billing_transaction_code:string;
        

    @Column("enum",{ 
        nullable:false,
        enum:["plan","event","purchase","withdraw"],
        name:"billing_transaction_type"
        })
    billing_transaction_type:string;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_subtotal"
        })
    billing_transaction_subtotal:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_disccount"
        })
    billing_transaction_disccount:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_taxes"
        })
    billing_transaction_taxes:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_retefte"
        })
    billing_transaction_retefte:number;
        

    @Column("float",{ 
        nullable:false,
        name:"billing_transaction_total"
        })
    billing_transaction_total:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["pending","success","rejected"],
        name:"billing_transaction_status"
        })
    billing_transaction_status:string;
        

    @Column("longtext",{ 
        nullable:true,
        name:"billing_transaction_observations"
        })
    billing_transaction_observations:string | null;
        

    @CreateDateColumn({name: 'created_at'})
    created_at:Date;
        

    @UpdateDateColumn({name: 'updated_at'})
    updated_at:Date;
        

   
    @OneToMany(type=>billing_transaction_items, billing_transaction_items=>billing_transaction_items.billingTransaction,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionItemss:billing_transaction_items[];
    

   
    @OneToMany(type=>billing_transaction_payments, billing_transaction_payments=>billing_transaction_payments.billingTransaction,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    billingTransactionPaymentss:billing_transaction_payments[];
    

   
    @OneToMany(type=>plan_transactions, plan_transactions=>plan_transactions.billingTransaction,{ onDelete: 'CASCADE' ,onUpdate: 'CASCADE' })
    planTransactionss:plan_transactions[];
    

   
    @ManyToMany(type=>event_talk_attendants, event_talk_attendants=>event_talk_attendants.billingTransactionss)
    eventTalkAttendantss:event_talk_attendants[];
    
}
