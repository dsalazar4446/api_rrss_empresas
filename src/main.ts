import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import "reflect-metadata";
import { ValidationPipe } from '@nestjs/common';
import express = require('express');
import { json } from 'body-parser';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const PORT = process.env.PORT || 3000

  await app.enableCors();
  await app.setGlobalPrefix('api/v1');
  app.use(express.static('uploads'));
  app.use(json({ limit: '50mb' }));
  app.useGlobalPipes(new ValidationPipe());
  /******************* SWAGGER  ************************/
  const options = new DocumentBuilder()
  .setTitle('Ecomybusiness')
  .setDescription('API ecomybusiness')
  .setVersion('1.0')
  .setBasePath('api/v1/')
  .build()
   
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('doc', app, document);

  /***********************************************************/
  await app.listen(PORT, () => {
    console.log(`Ecomybusiness is running in port ${PORT}`)
  });
}
bootstrap();
